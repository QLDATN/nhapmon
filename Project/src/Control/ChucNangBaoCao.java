package Control;

import Model.LoadData;
import View.FrameBaoCao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class ChucNangBaoCao {
    LoadData ld = new LoadData();
    
    public ChucNangBaoCao(){
        
    }

    public void DSCB(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb1, JComboBox jcb2) {

        String sql = "";
        Object[] arr = {"STT", "Họ Và tên", "Số Điện Thoại", "Email",
            "Số Tài Khoản", "Mã Số Thuế", "Số CMND", "Bộ môn"};
        if (jcb2.getSelectedItem().toString().equals("DS Cán Bộ")) {
            sql = "Select HotenGV, Dienthoai, Email, Sotaikhoan, Masothue, SoCMND, Bomon"
                    + " From GiangVien"
                    + " Where Bomon = ?";
            dtm.setRowCount(0);
            ld.loadData(jt, dtm, jsp, jcb1, null, null, null, sql, arr);
        } else {
            sql = "Select HotenGV, Dienthoai, Email, Sotaikhoan, Masothue, SoCMND, Bomon"
                    + " From GiangVien Where Bomon = ? AND Ngayvaodang != ''";
            dtm.setRowCount(0);
            ld.loadData(jt, dtm, jsp, jcb1, null, null, null, sql, arr);
        }

    }
    
    public void DSDoAn(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb){
        String sql = "Select Madoan, Tendetai, TenSV, HotenGV, TenGVPB, Kyhoc, Namhoc"
                    + " From Doan DA, GiangVien GV"
                    + " Where DA.MaGVHD = GV.MaGV AND Bomon = ?";
        Object[] arr = {"STT", "Mã Đồ Án", "Tên Đề Tài", "Họ Tên SV", "GVHD", "GVPB", "Kỳ Học", "Năm Học"};
        dtm.setRowCount(0);
        ld.loadData(jt, dtm, jsp, jcb, null, null, null, sql, arr);
    }
    
    public void DSMuonDoAn(JTable jt, DefaultTableModel dtm, JScrollPane jsp,JTextField jtf1, JTextField jtf2){
//        dtm.addColumn("STT");

        String sql = "Select MDA.[Muon-doan], Tendetai, TenSV, TenGVHD, TenGVPB, Tennguoimuon, Ngaymuon"
                + " From MuonDoAn MDA, DoAn DA"
                + " Where MDA.MaDoAn = DA.Madoan AND Ngaymuon BETWEEN ? AND ?";
        Object[] arr = {"STT", "Mã Đồ Án", "Tên Đề Tài", "Tên SV", "GVHD", "GVPB", "Tên Người Mượn", "Ngày Mượn"};
        //dtmDSMuonDoAn.addColumn(dtmDSMuonDoAn.getRowCount(), arr);
        dtm.setRowCount(0);
        ld.loadData(jt, dtm, jsp, null, null, jtf1, jtf2, sql, arr);

    }
    
    public void DAThanhLy(JTable jt, DefaultTableModel dtm, JScrollPane jsp,JTextField jtf1, JTextField jtf2){
    
        String sql = "Select DA.Madoan, Tendetai, TenSV, HotenGV, TenGVPB, Kyhoc, Namhoc"
                + " From GiangVien GV, DoAn DA"
                + " Where DA.MaGVHD = GV.MaGV AND DATEDIFF(Day, Ngaythanhly, GetDate()) >= 0"
                    + " AND Ngaythanhly BETWEEN ? AND ?";
        Object[] arr2 = {"STT", "Mã Đồ Án", "Tên Đề Tài", "Tên SV", "GVHD", "GVPB", "Kỳ học", "Năm Học"};
        dtm.setRowCount(0);
        ld.loadData(jt, dtm, jsp, null, null, jtf1, jtf2, sql, arr2);

    }
    
    public void DAMuonQuaHan(JTable jt, DefaultTableModel dtm, JScrollPane jsp,JTextField jtf1, JTextField jtf2){
    
        String sql = "Select MaGV, HotenGV, Tendetai, TenSV, Ngaymuon, Ngaytra, Baolanh"
                + " From MuonDoAn MDA, GiangVien GV, DoAn DA"
                + " Where MDA.MaDoAn = DA.Madoan AND GV.MaGV = DA.MaGVHD"
                + " AND DATEDIFF ( day , Ngaytradukien , Ngaytra ) > 0 AND Ngaymuon BETWEEN ? AND ?";
        Object[] arr1 = {"STT", "Mã Giáo Viên", "Họ Tên GV", "Tên Sách", "Tên Tác Giả",
            "Ngày Mượn", "Ngày Trả", "Phạt"};
        dtm.setRowCount(0);
        ld.loadData(jt, dtm, jsp, null, null, jtf1, jtf2, sql, arr1);

    }
    
    public void DAMuonDunghan(JTable jt, DefaultTableModel dtm, JScrollPane jsp,JTextField jtf1, JTextField jtf2){
    
        String sql = "Select MaGV, HotenGV, Tendetai, TenSV, Ngaymuon, Ngaytra, MDA.Tinhtrang"
                + " From MuonDoAn MDA, GiangVien GV, DoAn DA"
                + " Where MDA.MaDoAn = DA.Madoan AND GV.MaGV = DA.MaGVHD"
                + " AND DATEDIFF ( day , Ngaytradukien , Ngaytra ) <= 0 AND Ngaymuon BETWEEN ? AND ?";
        Object[] arr3 = {"STT", "Mã Giáo Viên", "Họ Tên GV", "Tên Sách", "Tên Tác Giả",
            "Ngày Mượn", "Ngày Trả", "Tình Trạng"};
        dtm.setRowCount(0);
        ld.loadData(jt, dtm, jsp, null, null, jtf1, jtf2, sql, arr3);

    }

}
