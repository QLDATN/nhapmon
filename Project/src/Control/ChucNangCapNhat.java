
package Control;

import Model.CapNhat;
import Model.Info_DA;
import Model.Info_GV;
import Model.Info_MDA;
import Model.Info_NguoiDung;
import java.sql.Date;
import java.util.ArrayList;

public class ChucNangCapNhat{
    
    CapNhat cn = new CapNhat();

    public ChucNangCapNhat() {
    }

    public int themGV(String maGV, String hoTenGV, String gioiTinhGV, String ngaySinhGV, String ngayVeTruong,
            String ngayVaoDang, String chucDanh, String chucVu, String dienThoaiGV, 
            String emailGV, String diaChi, String boMon, String chiBo, String soTK, 
            String msThue, String soCMND, String ghiChu){
        
        Info_GV gv = new Info_GV();
        
        gv.setMaGV(maGV);
        gv.setHoTenGV(hoTenGV);
        gv.setGioiTinhGV(gioiTinhGV);
        gv.setNgaySinhGV(ngaySinhGV);
        gv.setNgayVeTruong(ngayVeTruong);
        gv.setNgayVaoDang(ngayVaoDang);
        gv.setChucDanh(chucDanh);
        gv.setChucVu(chucVu);
        gv.setDienThoaiGV(dienThoaiGV);
        gv.setEmailGV(emailGV);
        gv.setDiaChi(diaChi);
        gv.setBoMon(boMon);
        gv.setChiBo(chiBo);
        gv.setSoTK(soTK);
        gv.setMsThue(msThue);
        gv.setSoCMND(soCMND);
        gv.setGhiChu(ghiChu);
                
        return cn.themGV(gv);
    }
    
    public int suaGV(String maGV, String hoTenGV, String gioiTinhGV, String ngaySinhGV, String ngayVeTruong,
            String ngayVaoDang, String chucDanh, String chucVu, String dienThoaiGV, 
            String emailGV, String diaChi, String boMon, String chiBo, String soTK, 
            String msThue, String soCMND, String ghiChu){
        
        Info_GV gv = new Info_GV();
        
        gv.setMaGV(maGV);
        gv.setHoTenGV(hoTenGV);
        gv.setGioiTinhGV(gioiTinhGV);
        gv.setNgaySinhGV(ngaySinhGV);
        gv.setNgayVeTruong(ngayVeTruong);
        gv.setNgayVaoDang(ngayVaoDang);
        gv.setChucDanh(chucDanh);
        gv.setChucVu(chucVu);
        gv.setDienThoaiGV(dienThoaiGV);
        gv.setEmailGV(emailGV);
        gv.setDiaChi(diaChi);
        gv.setBoMon(boMon);
        gv.setChiBo(chiBo);
        gv.setSoTK(soTK);
        gv.setMsThue(msThue);
        gv.setSoCMND(soCMND);
        gv.setGhiChu(ghiChu);
                
        return cn.suaGV(gv);
    }
    
    public int xoaGV(Info_GV gv){
        return cn.xoaGV(gv);
    }
    
////////////////////////////////////////////////
    public int themDA(String maDoAn, String tenDeTai, String maSV, String tenSV,
            String dienThoaiSV, String emailSV, String tenLop, String khoa,
            String kyHoc, String namHoc, String maGVHD, String tenGVHD,
            String maGVPB, String tenGVPB, String ngayBaoVe, String ngayThanhLy, String ghiChu){
        Info_DA da = new Info_DA();
        
        da.setMaDoAn(maDoAn);
        da.setTenDeTai(tenDeTai);
        da.setMaSV(maSV);
        da.setTenSV(tenSV);
        da.setDienThoaiSV(dienThoaiSV);
        da.setEmailSV(emailSV);
        da.setTenLop(tenLop);
        da.setKhoa(khoa);
        da.setKyHoc(kyHoc);
        da.setNamHoc(namHoc);
        da.setMaGVHD(maGVHD);
        da.setTenGVHD(tenGVHD);
        da.setMaGVPB(maGVPB);
        da.setTenGVPB(tenGVPB);
        da.setNgayBaoVe(ngayBaoVe);
        da.setNgayThanhLy(ngayThanhLy);
        da.setGhiChu(ghiChu);
        
        return cn.themDA(da);
    }
    
    public int suaDA(String maDoAn, String tenDeTai, String maSV, String tenSV,
            String dienThoaiSV, String emailSV, String tenLop, String khoa,
            String kyHoc, String namHoc, String maGVHD, String tenGVHD,
            String maGVPB, String tenGVPB, String ngayBaoVe, String ngayThanhLy, String ghiChu){
        Info_DA da = new Info_DA();
        
        da.setMaDoAn(maDoAn);
        da.setTenDeTai(tenDeTai);
        da.setMaSV(maSV);
        da.setTenSV(tenSV);
        da.setDienThoaiSV(dienThoaiSV);
        da.setEmailSV(emailSV);
        da.setTenLop(tenLop);
        da.setKhoa(khoa);
        da.setKyHoc(kyHoc);
        da.setNamHoc(namHoc);
        da.setMaGVHD(maGVHD);
        da.setTenGVHD(tenGVHD);
        da.setMaGVPB(maGVPB);
        da.setTenGVPB(tenGVPB);
        da.setNgayBaoVe(ngayBaoVe);
        da.setNgayThanhLy(ngayThanhLy);
        da.setGhiChu(ghiChu);
        
        return cn.suaDA(da);
    }
    
    public int xoaDA(Info_DA da){
        return cn.xoaDA(da);
    }
    
/////////////////////////////////////////////////////
    public int themMDA(String maMuonDA, String maDoAn, String tenNguoiMuon,
            String dienThoai, String email, String ngayMuon, String ngayTra,
            String ngayTraDuKien, String tinhTrang, String baoLanh, String ghiChu){
        
        Info_MDA mda = new Info_MDA();
        
        mda.setMaMuonDA(maMuonDA);
        mda.setMaDoAn(maDoAn);
        mda.setTenNguoiMuon(tenNguoiMuon);
        mda.setDienThoai(dienThoai);
        mda.setEmail(email);
        mda.setNgayMuon(ngayMuon);
        mda.setNgayTra(ngayTra);
        mda.setNgayTraDuKien(ngayTraDuKien);
        mda.setTinhTrang(tinhTrang);
        mda.setBaoLanh(baoLanh);
        mda.setGhiChu(ghiChu);
        
        return cn.themMDA(mda);
    }
    
    public int suaMDA(String maMuonDA, String maDoAn, String tenNguoiMuon,
            String dienThoai, String email, String ngayMuon, String ngayTra,
            String ngayTraDuKien, String tinhTrang, String baoLanh, String ghiChu){
        
        Info_MDA mda = new Info_MDA();
        
        mda.setMaMuonDA(maMuonDA);
        mda.setMaDoAn(maDoAn);
        mda.setTenNguoiMuon(tenNguoiMuon);
        mda.setDienThoai(dienThoai);
        mda.setEmail(email);
        mda.setNgayMuon(ngayMuon);
        mda.setNgayTra(ngayTra);
        mda.setNgayTraDuKien(ngayTraDuKien);
        mda.setTinhTrang(tinhTrang);
        mda.setBaoLanh(baoLanh);
        mda.setGhiChu(ghiChu);
        
        return cn.suaMDA(mda);
    }
    
    public int xoaMDA(Info_MDA mda){
        return cn.xoaMDA(mda);
    }
    
    /////////////////////////////////////
    
    public int themND(String tenTK, String matKhau, String hoTen, String gioiTinh,
             String email, String phanQuyen, String trangThai){
        
        Info_NguoiDung nd = new Info_NguoiDung();
        
        nd.setTenDangNhap(tenTK);
        nd.setMatKhau(matKhau);
        nd.setHoTen(hoTen);
        nd.setGioiTinh(gioiTinh);
        nd.setEmail(email);
        nd.setPhanQuyen(phanQuyen);
        nd.setTrangThai(trangThai);
        
        return cn.themND(nd);
    }
    
    public int suaND(String tenTK, String matKhau, String hoTen, String gioiTinh,
             String email, String phanQuyen, String trangThai){
        
        Info_NguoiDung nd = new Info_NguoiDung();
        
        nd.setTenDangNhap(tenTK);
        nd.setMatKhau(matKhau);
        nd.setHoTen(hoTen);
        nd.setGioiTinh(gioiTinh);
        nd.setEmail(email);
        nd.setPhanQuyen(phanQuyen);
        nd.setTrangThai(trangThai);
        
        return cn.suaND(nd);
    }
    
    public int xoaND(Info_NguoiDung nd){
        return cn.xoaND(nd);
    }
}