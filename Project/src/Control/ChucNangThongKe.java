
package Control;

import Model.LoadData;
import Model.ThongKe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ChucNangThongKe {
    LoadData ld = new LoadData();
    String sql = "";
    ThongKe thongKe = new ThongKe();
    
    public void TKCBCongTac(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb){
        String s = jcb.getSelectedItem().toString();
        sql = "  Select DISTINCT Bomon, (Select Count(*) From GiangVien GV Where GV.Ghichu = 'DANG CONG TAC' AND Bomon = ?), " +
            "   (Select Count(*) From GiangVien GV Where GV.Ghichu = 'NGHI HUU' AND Bomon = ?), " +
            "   (Select Count(*) From GiangVien GV Where GV.Ghichu = 'NCS NN' AND Bomon = ?)" +
            "    From GiangVien Where Bomon = ?";
            Object[] arr1 = {"STT", "Bộ Môn", "Đang Công Tác", "Nghỉ Hưu", "NCS NN"};
            
        dtm.setRowCount(0);
        ld.loadDataTK(jt, dtm, jsp, jcb, null, null, null, sql, arr1, 4);

    }
    
    public void TKCBTheoChucDanh(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb){
        
        sql = "Select DISTINCT Bomon, (Select Count(*) From GiangVien Where Chucdanh = 'GS' AND Bomon = ?), "
                + "(Select Count(*) From GiangVien Where Chucdanh = 'PGS.TS' AND Bomon = ?), "
                + "(Select Count(*) From GiangVien Where Chucdanh = 'TS' AND Bomon = ?), "
                + "(Select Count(*) From GiangVien Where Chucdanh = 'Ths' AND Bomon = ?), "
                + "(Select Count(*) From GiangVien Where Chucdanh = 'KS' AND Bomon = ?), "
                + "(Select Count(*) From GiangVien Where Chucdanh != 'GS' AND Chucdanh != 'PGS.TS'"
                + " AND Chucdanh != 'TS' AND Chucdanh != 'ThS' AND Chucdanh != 'KS' AND Bomon = ?) "
                + "From GiangVien Where Bomon = ?" ;
            Object[] arr1 = {"STT", "Bộ Môn", "GS", "PGS.TS", "TS", "ThS", "KS", "Khác"};
            
        dtm.setRowCount(0);
        ld.loadDataTK(jt, dtm, jsp, jcb, null, null, null, sql, arr1, 7);
        
    }
    
    public void TKDangVien(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb){
        
        sql = "  Select Bomon, Chibo, Count(*) From GiangVien Where Ngayvaodang != '' AND Bomon = ? Group By Chibo, Bomon";
        Object[] arr1 = {"STT", "Bộ Môn", "Chi Bộ", "Số Lượng Đảng Viên"};
        
            
        dtm.setRowCount(0);
        
        ld.loadData(jt, dtm, jsp, jcb, null, null, null, sql, arr1);
        
    }
    
    public void TKDAMuon(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb){
                
        sql = "Select Tennguoimuon, Count(*) From MuonDoAn Where Year(Ngaymuon) = ? Group By Tennguoimuon";
        Object[] arr1 = {"STT", "Họ Tên Người Mượn", "Số Lượng"};
        dtm.setRowCount(0);
        ld.loadDataTK(jt, dtm, jsp, jcb, null, null, null, sql, arr1, 1);
  
    }
}
