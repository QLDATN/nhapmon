
package Control;

import Model.LoadData;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class ChucNangTimKiem {
    
    LoadData ld = new LoadData();
    String sql = "";
    
    public void timKiemDSCB(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb1, JComboBox jcb2){

        if(jcb2.getSelectedItem().toString().equals("DS Cán Bộ")){
            sql = "Select *"
            + " From GiangVien"
            + " Where Bomon = ?";
            dtm.setRowCount(0);
            ld.loadDataTable(jt, dtm, jsp, jcb1, null, null, null, sql);
        }
        else{
            sql = "Select *"
                    + " From GiangVien Where Bomon = ? AND Ngayvaodang != ''";
            dtm.setRowCount(0);
            ld.loadDataTable(jt, dtm, jsp, jcb1, null, null, null, sql);
        }
    }
    
    public void TKDSDA(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb){
        
        sql = "Select DoAn.*"
                + " From DoAn, GiangVien"
                + " Where Bomon = ? AND DoAn.MaGVHD = GiangVien.MaGV";
            
        dtm.setRowCount(0);
        ld.loadDataTable(jt, dtm, jsp, jcb, null, null, null, sql);
    }
    
    public void TKDSMDA(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JTextField jtf1, JTextField jtf2){
        sql = "Select * from MuonDoAn Where Ngaymuon BETWEEN ? AND ?";
        dtm.setRowCount(0);
        ld.loadDataTable(jt, dtm, jsp, null, null, jtf1, jtf2, sql);
    }
    
    public void TKDSDAThanhLy(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JTextField jtf1, JTextField jtf2){
        sql = "Select *"
                + " From DoAn"
                + " Where DATEDIFF(DAY, Ngaybaove, Ngaythanhly) > 0 AND Ngaythanhly BETWEEN ? AND ?";
        dtm.setRowCount(0);
        ld.loadDataTable(jt, dtm, jsp, null, null, jtf1, jtf2, sql);
    }
    
    public void TKDSMDAQuaHan(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JTextField jtf1, JTextField jtf2){
        sql = "Select *"
                + " From MuonDoAn"
                + " Where DATEDIFF ( day , Ngaytradukien , Ngaytra ) > 0 AND Ngaymuon BETWEEN ? AND ?";
       dtm.setRowCount(0);
        ld.loadDataTable(jt, dtm, jsp, null, null, jtf1, jtf2, sql);
    }
    
    public void TKDSMDADungHan(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JTextField jtf1, JTextField jtf2){
    
        sql = "Select *"
                + " From MuonDoAn"
                + " Where DATEDIFF ( day , Ngaytradukien , Ngaytra ) <= 0 AND Ngaymuon BETWEEN ? AND ?";
        dtm.setRowCount(0);
        ld.loadDataTable(jt, dtm, jsp, null, null, jtf1, jtf2, sql);

    }
    
    public void TK(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JTextField jtf, String sql){
        dtm.setRowCount(0);
        ld.loadDataTableTK(jt, dtm, jsp, jtf, sql);
    }
    
}
