package Control;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.TableModel;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteToExcel {
		
	public boolean writeExcell(JTable table, File file) {

	    boolean kt = true;
            FileOutputStream FOS = null;
            try {
                //new WorkbookFactory();
                
                //Workbook wb = new XSSFWorkbook(); //Excell workbook
                
                XSSFWorkbook wb = new XSSFWorkbook();
                Sheet sheet = wb.createSheet(); //WorkSheet
                //   Row row = sheet.createRow(2); //Row created at line 3
                Row row = sheet.createRow(1); //Row created at line 2
                TableModel model = table.getModel(); //Table model
                Row headerRow = sheet.createRow(0); //Create row at line 0
                for(int headings = 0; headings < model.getColumnCount(); headings++){ //For each column
                    headerRow.createCell(headings).setCellValue(model.getColumnName(headings));//Write column name
                }
                for(int rows = 0; rows < model.getRowCount(); rows++){ //For each table row
                    for(int cols = 0; cols < table.getColumnCount(); cols++){ //For each table column
                        row.createCell(cols).setCellValue(model.getValueAt(rows, cols).toString()); //Write value
                    }
                    
                    //Set the row to the next one in the sequence
                    //row = sheet.createRow((rows + 3));
                    row = sheet.createRow((rows + 2));
                    
                }
                //wb.write(new FileOutputStream(path.toString())); //Save the file
                FOS = new FileOutputStream(file);
                wb.write(FOS);                              
            } catch (FileNotFoundException ex) {
                kt = false;
                ex.getMessage();
            } catch (IOException ex) {
                kt = false;
                ex.getMessage();
            } finally {
                try {
                    FOS.close();
                } catch (IOException ex) {
                    kt = false;
                    ex.getMessage();
                }
            }
            return kt;
        }
        
}


