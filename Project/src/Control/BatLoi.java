package Control;

import Model.Info_DA;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BatLoi {

    public BatLoi() {
    }

    DBConnection dbc = new DBConnection();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public boolean kTraTrung(String ma, String sql) {
        boolean kt = false;

        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement(sql);

            rs = ps.executeQuery();

            while (rs.next()) {
                String temp = rs.getString(1);
                if (temp.equals(ma)) {
                    kt = true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BatLoi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kt;
    }

    public boolean nhapSaiTenGVHD(String ten, String s) {
        boolean kt = true;
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement("Select HotenGV From GiangVien GV Where MaGV = ?");
            ps.setString(1, s);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String temp = rs.getString("HotenGV");
                if (temp.equals(ten)) {
                    kt = false;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(BatLoi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kt;
    }
    
    public boolean nhapSaiTenGVPB(String ten, String s) {
        boolean kt = true;
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement("Select HotenGV From GiangVien Where MaGV = ?");
            ps.setString(1, s);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String temp = rs.getString("HotenGV");
                if (temp.equals(ten)) {
                    kt = false;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(BatLoi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kt;
    }
    
    
    String regex = "\\s*";// Bat loi de trong
    String regexEmail = "\\w+@\\w+(\\.\\w+){1,2}";
    String regexPhone = "0\\d{9,10}";
    String regexNgay = "\\d\\d\\d\\d-\\d{1,2}-\\d{1,2}";
    String regexCMND = "\\d{9}";
    String regexNumber = "\\d*";
    
    public String ktNhapGV(String maGV, String tenGV, String ngaySinhGV, String ngayVeTruong
                            , String dienThoaiGV, String emailGV, String diaChiGV, String boMon, String chiBo
                            , String soTK, String msThue, String soCMND){
                            
        
        
        if(maGV.matches(regex) && tenGV.matches(regex) && ngaySinhGV.matches(regex) && ngayVeTruong.matches(regex) && dienThoaiGV.matches(regex)
                 && emailGV.matches(regex) && diaChiGV.matches(regex) && boMon.matches(regex) && chiBo.matches(regex)
                 && soTK.matches(regex) && msThue.matches(regex) && soCMND.matches(regex)){
            return "Chua Nhap Du Lieu";
        }
        else if(maGV.matches(regex)){
            return "Chua Nhap Ma GV";
        }
        else if(tenGV.matches(regex)){
            return "Chua Nhap Ten GV";
        }
        else if(ngaySinhGV.matches(regex)){
            return "Chua Nhap Ngay Sinh";
        }
        else if(!ngaySinhGV.matches(regexNgay)){
            return "Nhap Sai Dinh Dang Ngay Sinh";
        }
        else if(ngayVeTruong.matches(regex)){
            return "Chua Nhap Ngay Ve Truong";
        }
        else if(!ngayVeTruong.matches(regexNgay)){
            return "Nhap Sai Dinh Dang Ngay Ve Truong";
        }
        else if(dienThoaiGV.matches(regex)){
            return "Chua Nhap Dien Thoai";
        }
        else if(!dienThoaiGV.matches(regexPhone)){
            return "Day Khong Phai So Dien Thoai";
        }
        else if(emailGV.matches(regex)){
            return "Chua Nhap Email";
        }
        else if(!emailGV.matches(regexEmail)){
            return "Day Khong Phai Email";
        }
        else if(diaChiGV.matches(regex)){
            return "Chua Nhap Dia Chi";
        }
        else if(boMon.matches(regex)){
            return "Chua Nhap Bo Mon";
        }
        else if(chiBo.matches(regex)){
            return "Chua Nhap Chi Bo";
        }
        else if(soTK.matches(regex)){
            return "Chua Nhap So Tai Khoan";
        }
        else if(!soTK.matches(regexNumber)){
            return "Day Khong Phai So Tai Khoan";
        }
        else if(msThue.matches(regex)){
            return "Chua Nhap Ma So Thue";
        }
        else if(msThue.matches(regex)){
            return "Day Khong Phai Ma So Thue";
        }
        else if(soCMND.matches(regex)){
            return "Chua Nhap So CMND";
        }
        else if(!soCMND.matches(regexCMND)){
            return "Day Khong Phai So CMND";
        }
        else{
            return "Du Lieu Thoa Man";
        }
    }
    
    public String ktNhapDA(String maDA, String tenDeTai, String maSV, String tenSV, String dienThoaiSV, 
            String emailSV, String tenLop, String khoa, String maGVHD, String tenGVHD, String maGVPB, 
            String tenGVPB, String ngayBaoVe, String ngayThanhLy, String ghiChuDA){
        
        if(maDA.matches(regex) && tenDeTai.matches(regex) && maSV.matches(regex) && tenSV.matches(regex) && 
                dienThoaiSV.matches(regex) && emailSV.matches(regex) && tenLop.matches(regex) && khoa.matches(regex) && 
                maGVHD.matches(regex) && tenGVHD.matches(regex) && maGVPB.matches(regex) && tenGVPB.matches(regex)
                && ngayBaoVe.matches(regex) && ngayThanhLy.matches(regex) && ghiChuDA.matches(regex)){
            return "Chua Nhap Du Lieu";
        }
        else if(maDA.matches(regex)){
            return "Chua Nhap Ma Do An";
        }
        else if(tenDeTai.matches(regex)){
            return "Chua Nhap Ten De Tai";
        }
        else if(maSV.matches(regex)){
            return "Chua Nhap Ma Sinh Vien";
        }
        else if(tenSV.matches(regex)){
            return "Chua Nhap Ten Sinh Vien";
        }
        else if(dienThoaiSV.matches(regex)){
            return "Chua Nhap So Dien Thoai";
        }
        else if(!dienThoaiSV.matches(regexPhone)){
            return "Day Khong Phai So Dien Thoai";
        }
        else if(emailSV.matches(regex)){
            return "Chua Nhap Email";
        }
        else if(!emailSV.matches(regexEmail)){
            return "Day Khong Phai Email";
        }
        else if(tenLop.matches(regex)){
            return "Chua Nhap Ten Lop";
        }
        else if(khoa.matches(regex)){
            return "Chua Nhap Khoa";
        }
        else if(maGVHD.matches(regex)){
            return "Chua Nhap Ma GVHD";
        }
        else if(tenGVHD.matches(regex)){
            return "Chua Nhap Ten GVHD";
        }
        else if(maGVPB.matches(regex)){
            return "Chua Nhap Ma GVPB";
        }
        else if(tenGVPB.matches(regex)){
            return "Chua Nhap Ten GVPB";
        }
        else if(ngayBaoVe.matches(regex)){
            return "Chua Nhap Ngay Bao Ve";
        }
        else if(!ngayBaoVe.matches(regexNgay)){
            return " ngayBaoVe Khong Dung Dinh Dang Ngay";
        }
        else if(ngayThanhLy.matches(regex)){
            return "Chua Nhap Ngay Thanh Ly";
        }
        else if(!ngayThanhLy.matches(regexNgay)){
            return "ngayThanhLy Khong Dung Dinh Dang Ngay";
        }
//        else if(ghiChuDA.matches(regex)){
//            return "Chua Nhap Ghi Chu";
//        }
        else{
            return "Du Lieu Thoa Man";
        }
    }
    
    public String ktNhapMDA(String maMuonDA, String maDA, String tenNguoiMuon, String dienThoaiMDA, 
            String emailMDA, String ngayMuon, String ngayTra, String ngayTraDuKien, String tinhTrang,
            String baoLanh, String ghiChuMDA){
        
        if(maMuonDA.matches(regex) && maDA.matches(regex) && tenNguoiMuon.matches(regex) && dienThoaiMDA.matches(regex)
                 && emailMDA.matches(regex) && ngayMuon.matches(regex) && ngayTra.matches(regex)
                && ngayTraDuKien.matches(regex) && baoLanh.matches(regex) && ghiChuMDA.matches(regex)){
                
            return "Chua Nhap Du lieu";
        }
        else if(maMuonDA.matches(regex)){
            return "Chua Nhap Ma Muon Do An";
        }
        else if(maDA.matches(regex)){
            return "Chua Nhap Ma Do An";
        }
        else if(tenNguoiMuon.matches(regex)){
            return "Chua Nhap Ten Nguoi Muon";
        }
        else if(dienThoaiMDA.matches(regex)){
            return "Chua Nhap Dien Thoai";
        }
        else if(!dienThoaiMDA.matches(regexPhone)){
            return "Khong Phai So Dien Thoai";
        }
        else if(emailMDA.matches(regex)){
            return "Chua Nhap Email";
        }
        else if(!emailMDA.matches(regexEmail)){
            return "Khong Phai Email";
        }
        else if(ngayMuon.matches(regex)){
            return "Chua Nhap Ngay Muon";
        }
        else if(!ngayMuon.matches(regexNgay)){
            return "Ngay Muon Khong Dung Dinh Dang";
        }
        else if(ngayTra.matches(regex)){
            return "Chua Nhap Ngay Tra";
        }
        else if(!ngayTra.matches(regexNgay)){
            return "Ngay Tra Khong Dung Dinh Dang";
        }
        else if(ngayTraDuKien.matches(regex)){
            return "Chua Nhap Ngay Tra Du Kien";
        }
        else if(!ngayTraDuKien.matches(regexNgay)){
            return "Ngay Tra Du Kien Khong Dung Dinh Dang";
        }
        else if(baoLanh.matches(regex)){
            return "Chua Nhap Bao Lanh";
        }
//        else if(ghiChuMDA.matches(regex)){
//            return "Chua Nhap Bao Lanh";
//        }
        else{
            return "Du Lieu Thoa Man";
        }
    }
    
    public String ktNhapQLND(String tenDangNhap, String matKhau, String hoTenND, String emailND){
        
        if(tenDangNhap.matches(regex) && matKhau.matches(regex) && hoTenND.matches(regex) && emailND.matches(regex)){
            return "Chua Nhap Du lieu";
        }
        else if(tenDangNhap.matches(regex)){
            return "Chua Nhap Ten Dang Nhap";
        }
        else if(matKhau.matches(regex)){
            return "Chua Nhap Mat Khau";
        }
        else if(hoTenND.matches(regex)){
            return "Chua Nhap Ho Ten Nguoi Dung";
        }
        else if(emailND.matches(regex)){
            return "Chua Nhap Email Nguoi Dung";
        }
        else if(!emailND.matches(regexEmail)){
            return "Email Khong Dung Dinh Dang";
        }
        else{
            return "Du Lieu Thoa Man";
        }
    }
    
    public String ktNhapBaoCao(String ngayBD, String ngayKT){
        if(ngayBD.matches(regex) && ngayKT.matches(regex)){
            return "Ban chua nhap du lieu";
        }
        else if(ngayBD.matches(regex)){
            return "Ban chua nhap ngay bat dau";
        }
        else if(!ngayBD.matches(regexNgay)){
            return "Ngay bat dau khong dung dinh dang";
        }
        else if(ngayKT.matches(regex)){
            return "Ban chua nhap ngay ket thuc";
        }
        else if(!ngayKT.matches(regexNgay)){
            return "Ngay ket thuc khong dung dinh dang";
        }
        return "Du lieu thoa man";
    }
}
