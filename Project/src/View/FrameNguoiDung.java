package View;

import Control.BatLoi;
import Control.ChucNangCapNhat;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Control.DBConnection;
import Model.Info_MDA;
import Model.Info_NguoiDung;
import Model.LoadData;

//import model.MyConnect;
//import model.TaiKhoan;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.imageio.ImageIO;
import javax.naming.spi.DirStateFactory.Result;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.*;

import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPasswordField;

public class FrameNguoiDung extends JFrame {

    private JPanel contentPane;
    private JTextField tfHoTen;
    private JTextField tfEmail;
    private JPasswordField tfMatKhau;
    private JTextField tfTenTK;
    private JComboBox cboxGioiTinh;
    private JComboBox cboxPhanQuyen;
    private JComboBox cboxTrangThai;

    private JTable table;
    private DefaultTableModel model;
    private JScrollPane jsp;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {

                    FrameNguoiDung frame = new FrameNguoiDung();
                    frame.setVisible(true);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    DBConnection dbc = new DBConnection();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public FrameNguoiDung() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setBounds(0, 21, 1050, 650);
        setBounds(0, 0, 1400, 900);
        setBackground(new Color(218, 223, 225));
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        contentPane.setLayout(null);

        //panel hiển thị thông tin người dùng
        JPanel pnThongTin = new JPanel();
        pnThongTin.setBackground(Color.WHITE);
        pnThongTin.setBounds(29, 37, 966, 233);
        pnThongTin.setBorder(BorderFactory.createTitledBorder("Thông tin chi tiết"));

        contentPane.add(pnThongTin);
        pnThongTin.setLayout(null);

        JLabel lblHoten = new JLabel("Họ tên");
        lblHoten.setBounds(634, 31, 46, 14);
        pnThongTin.add(lblHoten);

        JLabel lblGioiTinh = new JLabel("Giới tính");
        lblGioiTinh.setBounds(634, 83, 51, 14);
        pnThongTin.add(lblGioiTinh);

        JLabel lblEmail = new JLabel("Email");
        lblEmail.setBounds(634, 142, 46, 14);
        pnThongTin.add(lblEmail);

        JLabel lblTenDN = new JLabel("Tên đăng nhập");
        lblTenDN.setBounds(101, 31, 86, 14);
        pnThongTin.add(lblTenDN);

        JLabel lblMatKhau = new JLabel("Mật khẩu");
        lblMatKhau.setBounds(101, 83, 68, 14);
        pnThongTin.add(lblMatKhau);

        JLabel lblPhanQuyen = new JLabel("Phân quyền");
        lblPhanQuyen.setBounds(101, 142, 68, 14);
        pnThongTin.add(lblPhanQuyen);

        tfHoTen = new JTextField();
        tfHoTen.setBounds(726, 27, 165, 22);
        pnThongTin.add(tfHoTen);
        tfHoTen.setColumns(10);

        tfEmail = new JTextField();
        tfEmail.setBounds(726, 131, 165, 22);
        pnThongTin.add(tfEmail);
        tfEmail.setColumns(10);

        tfTenTK = new JTextField();
        tfTenTK.setBounds(223, 27, 165, 22);
        pnThongTin.add(tfTenTK);
        tfTenTK.setColumns(10);

        tfMatKhau = new JPasswordField();
        tfMatKhau.setBounds(223, 79, 165, 22);
        pnThongTin.add(tfMatKhau);
        tfMatKhau.setColumns(10);

        //combox phan quyen
        String[] s1 = {"Member", "Admin"};
        cboxPhanQuyen = new JComboBox(s1);
        cboxPhanQuyen.setBounds(223, 131, 165, 22);
        pnThongTin.add(cboxPhanQuyen);

        //combobox gioi tinh
        String[] s2 = {"Nam", "Nữ"};
        cboxGioiTinh = new JComboBox(s2);
        cboxGioiTinh.setBounds(726, 79, 165, 22);
        pnThongTin.add(cboxGioiTinh);

        JLabel lblTrangThai = new JLabel("Trạng thái");
        lblTrangThai.setBounds(101, 186, 86, 14);
        pnThongTin.add(lblTrangThai);

        String[] s3 = {"Hoạt Động", "Tạm Ngưng"};
        cboxTrangThai = new JComboBox(s3);
        cboxTrangThai.setBounds(223, 183, 165, 20);
        pnThongTin.add(cboxTrangThai);

        //panel hiển thị bảng
        JPanel pnBangND = new JPanel();
        pnBangND.setBackground(Color.WHITE);
        pnBangND.setBounds(29, 313, 966, 364);
        pnBangND.setBorder(BorderFactory.createTitledBorder("Danh sách"));
        contentPane.add(pnBangND);
        pnBangND.setLayout(null);

        jsp = new JScrollPane();

        jsp.setBounds(10, 22, 946, 332);
        table = new JTable();
        jsp.setViewportView(table);

        loadData();

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {

                conn = dbc.Connect();
                try {

                    int row = table.getSelectedRow();
                    String table_click = (table.getModel().getValueAt(row, 0).toString());

                    String sql = "select * from Login where TenDN='" + table_click + "'";

                    Statement ts = conn.createStatement();
                    ResultSet rs = ts.executeQuery(sql);

                    if (rs.next()) {

                        tfTenTK.setText(rs.getString(1));

                        tfMatKhau.setText(rs.getString(2));
                        tfHoTen.setText(rs.getString(3));
                        cboxGioiTinh.setSelectedItem(rs.getString(4));
                        tfEmail.setText(rs.getString(5));
                        cboxPhanQuyen.setSelectedItem(rs.getString(6));
                        cboxTrangThai.setSelectedItem(rs.getString(7));

                    }

                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });
        pnBangND.add(jsp);

        //các button xử lý sự kiện
        JButton btnThemTK = new JButton(" Thêm Tài Khoản");
        btnThemTK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                themTK();
                loadData();

            }
        });
        btnThemTK.setFont(new Font("Tahoma", Font.PLAIN, 13));
        btnThemTK.setBounds(1105, 150, 184, 55);

//		Icon icon1 = new ImageIcon("G:/Data Eclipse/BTL OOP/img/icon-add3.png");
        Icon icon1 = new ImageIcon("icon-add3.png");

        btnThemTK.setHorizontalTextPosition(JButton.RIGHT);
        btnThemTK.setVerticalTextPosition(JButton.CENTER);
        btnThemTK.setIcon(icon1);

        contentPane.add(btnThemTK);

        JButton btnXoaTK = new JButton(" Xóa Tài Khoản");
        btnXoaTK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                xoaTK();
                loadData();
            }
        });
        btnXoaTK.setFont(new Font("Tahoma", Font.PLAIN, 13));
        btnXoaTK.setBounds(1105, 430, 184, 55);

//		Icon icon2 = new ImageIcon("G:/Data Eclipse/BTL OOP/img/icon-delete.png");
        Icon icon2 = new ImageIcon("icon-delete.png");

        btnXoaTK.setHorizontalTextPosition(JButton.RIGHT);
        btnXoaTK.setVerticalTextPosition(JButton.CENTER);
        btnXoaTK.setIcon(icon2);

        contentPane.add(btnXoaTK);
//		Icon icon4 = new ImageIcon("G:/Data Eclipse/BTL OOP/img/icon-lock.png");
        Icon icon4 = new ImageIcon("icon-lock.png");

        JButton btnSuaTT = new JButton(" Sửa Thông Tin");
        btnSuaTT.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                SuaTT();
                loadData();
            }
        });
        btnSuaTT.setFont(new Font("Tahoma", Font.PLAIN, 13));
        btnSuaTT.setBounds(1105, 290, 184, 55);
//		Icon icon3 = new ImageIcon("G:/Data Eclipse/BTL OOP/img/icon-repair2.png");
        Icon icon3 = new ImageIcon("icon-repair2.png");

        btnSuaTT.setHorizontalTextPosition(JButton.RIGHT);
        btnSuaTT.setVerticalTextPosition(JButton.CENTER);
        btnSuaTT.setIcon(icon3);

        contentPane.add(btnSuaTT);

        JButton btnRefresh = new JButton("Refresh");
        btnRefresh.setFont(new Font("Tahoma", Font.PLAIN, 17));

//		Icon icon5 = new ImageIcon("G:/Data Eclipse/BTL OOP/img/icon-refresh.png");
        Icon icon5 = new ImageIcon("icon-refresh.png");

        btnRefresh.setHorizontalTextPosition(JButton.RIGHT);
        btnRefresh.setVerticalTextPosition(JButton.CENTER);
        btnRefresh.setIcon(icon5);
        btnRefresh.setBounds(1105, 570, 184, 55);
        contentPane.add(btnRefresh);

        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                tfTenTK.setText("");
                tfMatKhau.setText("");
                tfHoTen.setText("");
                cboxGioiTinh.setSelectedItem("Null");
                tfEmail.setText("");
                cboxPhanQuyen.setSelectedItem("Member");
                cboxTrangThai.setSelectedItem("Hoạt Động");

            }

        });

    }

    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand() == "Refresh") {
            loadData();
            return;
        }

        if (e.getActionCommand() == "Thêm TK") {
            themTK();
            return;
        }
        if (e.getActionCommand() == "Xóa TK") {
            xoaTK();
        }

        if (e.getActionCommand() == "Sửa TT") {
            SuaTT();
        }
        if (e.getActionCommand() == "Khóa TK") {
            KhoaTK();
        }

    }

    public void loadData() {

        try {

            dbc = new DBConnection();
            conn = dbc.Connect();

            model = new DefaultTableModel();

            ps = conn.prepareStatement("Select * from Login");
            rs = ps.executeQuery();

            ResultSetMetaData rsMD = rs.getMetaData();
            int colNumber = rsMD.getColumnCount();
            String[] arr = new String[colNumber];

            for (int i = 0; i < colNumber; i++) {
                arr[i] = rsMD.getColumnName(i + 1);
            }

            model.setColumnIdentifiers(arr);

            while (rs.next()) {
                for (int i = 0; i < colNumber; i++) {
                    arr[i] = rs.getString(i + 1);
                }
                model.addRow(arr);
            }
            table.setModel(model);

        } catch (SQLException e) {
            System.out.println("Kết nối thất bại");
        }
    }

    BatLoi batLoi = new BatLoi();
    ChucNangCapNhat update = new ChucNangCapNhat();
    LoadData ld = new LoadData();
    //DBConnection dbc = DBConnection();

    public void themTK() {
        String tenDangNhap = tfTenTK.getText();
        String matKhau = tfMatKhau.getText();
        String hoTenND = tfHoTen.getText();
        String emailND = tfEmail.getText();
        String gioiTinh = cboxGioiTinh.getSelectedItem().toString();
        String phanQuyen = cboxPhanQuyen.getSelectedItem().toString();
        String trangThai = cboxTrangThai.getSelectedItem().toString();

        String kt = batLoi.ktNhapQLND(tenDangNhap, matKhau, hoTenND, emailND);

        if(kt.equals("Chua Nhap Du Lieu")){
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            
        }
        else if (kt.equals("Chua Nhap Ten Dang Nhap")) {
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Tài Khoản");
        } else if (batLoi.kTraTrung(tfTenTK.getText(), "Select TenDN From Login")) {
            JOptionPane.showMessageDialog(null, "Tên Tài Khoản Đã Tồn Tại");
        } else if (kt.equals("Chua Nhap Mat Khau")) {
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mật Khẩu");
        } else if (kt.equals("Chua Nhap Ho Ten Nguoi Dung")) {
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Họ Tên Người Dùng");
        } else if (kt.equals("Chua Nhap Email Nguoi Dung")) {
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhap Email");
        } else if (kt.equals("Email Khong Dung Dinh Dang")) {
            JOptionPane.showMessageDialog(null, "Email Không Đúng Định Dạng");
        } else if (kt.equals("Du Lieu Thoa Man")) {

            try {
                int temp = update.themND(tenDangNhap, matKhau, hoTenND, gioiTinh, emailND, phanQuyen, trangThai);
                if (temp > 0) {
                    JOptionPane.showMessageDialog(this, "Cập Nhật Thành Công");
                    model.setRowCount(0);
                    ld.loadDataTable(table, model, jsp, null, null, null, null, "Select * From Login");
                } else {
                    JOptionPane.showMessageDialog(this, "Loi");
                }
            } catch (Exception ex) {
                System.out.println("Loi: " + ex.getMessage());
            }

        }
//            
//            
//		if(tfTenTK.getText().equals("") || tfMatKhau.getText().equals("")) {
//			
//			JOptionPane.showMessageDialog(null,"Tên đăng nhập và mật khẩu không được để trống");
//		
//		}
//		
//		else {
//			String s1 =tfTenTK.getText();
//			String s2 = tfMatKhau.getText();
//			String s3 = tfHoTen.getText();
//			String s4 =(String) cboxGioiTinh.getSelectedItem();
//			String s5 = tfEmail.getText();
//			String s6 =(String) cboxPhanQuyen.getSelectedItem();
//			String s7 =(String) cboxTrangThai.getSelectedItem();
//			
//			
//			try {
//				
//				
//				 DBConnection dbc = new DBConnection();
//	             Connection conn = dbc.Connect();
//				
//				PreparedStatement pst1 = null;
//				String sqlCommand1 = "Select TenDN from Login where TenDN=?";
//				pst1 = conn.prepareStatement(sqlCommand1);
//				
//				
//				 pst1.setString(1,s1.trim());
//				
//				 
//				 ResultSet rs = pst1.executeQuery();
//					int count = 0;
//					
//					while(rs.next()) {
//						count++;
//					}
//					if(count == 1) {
//						JOptionPane.showMessageDialog(null,"Tên tài khoản đã tồn tại>> Lựa chọn tên khác!");
//						tfTenTK.requestFocus();// con chuột ở ô tên tài khoản
//						
//						
//					}
//					else {
//						
//						String sqlCommand = "insert into Login(TenDN,MatKhau,HoTen,GioiTinh,Email,PhanQuyen,TrangThai) values(?,?,?,?,?,?,?)";
//						
//						
//						PreparedStatement pst = null;
//						
//						pst = conn.prepareStatement(sqlCommand);
//							
//							pst.setString(1, s1);
//							pst.setString(2, s2);
//							pst.setString(3, s3);
//							pst.setString(4, s4);
//							pst.setString(5, s5);
//							pst.setString(6, s6);
//							pst.setString(7, s7);
//							
//							if(pst.executeUpdate() > 0) {
//								JOptionPane.showMessageDialog(null,"Thêm tài khoản thành công");
//							}
//							else {
//								System.out.println("Them TK that bai");
//							}
//
//						
//						
//					}
//				 
//				
//					
//				
//			}  catch (SQLException e) {
//				System.out.println("Loi: "+e);
//			}
//			
//			}
//		}
    }

    public void xoaTK() {
        String tenDangNhap = tfTenTK.getText();
        String matKhau = tfMatKhau.getText();
        String hoTenND = tfHoTen.getText();
        String emailND = tfEmail.getText();
        String gioiTinh = cboxGioiTinh.getSelectedItem().toString();
        String phanQuyen = cboxPhanQuyen.getSelectedItem().toString();
        String trangThai = cboxTrangThai.getSelectedItem().toString();

        int click = JOptionPane.showConfirmDialog(this, "Bạn Có Muốn Xóa Không ?");
        if (click == JOptionPane.YES_OPTION) {
            try {

                Info_NguoiDung nd = new Info_NguoiDung();
                nd.setTenDangNhap(tenDangNhap);
                int temp = update.xoaND(nd);
                if (temp > 0) {
                    JOptionPane.showMessageDialog(this, "Xoa Thanh Cong");
                    model.setRowCount(0);
                    ld.loadDataTable(table, model, jsp, null, null, null, null, "Select * From Login");
                } else {
                    JOptionPane.showMessageDialog(this, "Loi");
                }

            } catch (Exception ex) {
                System.out.println("Loi: " + ex.getMessage());
            }
        }

//        try {
//    
//            conn = dbc.Connect();
//
//            PreparedStatement ps = conn.prepareStatement("delete from Login where TenDN = ?");
//            ps.setString(1, tfTenTK.getText());
//
//            int temp = ps.executeUpdate();
//
//            if (temp > 0) {
//                JOptionPane.showMessageDialog(this, "Xóa thành công!");
//                DefaultTableModel model = new DefaultTableModel();
//                model.setRowCount(0);
//                table.setModel(model);
//
//            } else {
//                JOptionPane.showMessageDialog(this, "Lựa chọn tên tài khoản muốn xóa");
//            }
//
//        } catch (Exception ex) {
//            System.out.println("Loi: " + ex.getMessage());
//        }
    }

    public void SuaTT() {
        String tenDangNhap = tfTenTK.getText();
        String matKhau = tfMatKhau.getText();
        String hoTenND = tfHoTen.getText();
        String emailND = tfEmail.getText();
        String gioiTinh = cboxGioiTinh.getSelectedItem().toString();
        String phanQuyen = cboxPhanQuyen.getSelectedItem().toString();
        String trangThai = cboxTrangThai.getSelectedItem().toString();

        String kt = batLoi.ktNhapQLND(tenDangNhap, matKhau, hoTenND, emailND);

        if(kt.equals("Chua Nhap Du Lieu")){
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            
        }
        if (kt.equals("Chua Nhap Ten Dang Nhap")) {
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Tài Khoản");
           
        } else if (kt.equals("Chua Nhap Mat Khau")) {
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mật Khẩu");
            
        } else if (kt.equals("Chua Nhap Ho Ten Nguoi Dung")) {
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Họ Tên Người Dùng");
           
        } else if (kt.equals("Chua Nhap Email Nguoi Dung")) {
            JOptionPane.showMessageDialog(null, "Bạn Chưa Nhap Email");
            
        } else if (kt.equals("Email Khong Dung Dinh Dang")) {
            JOptionPane.showMessageDialog(null, "Email Không Đúng Định Dạng");
            
        } else if (kt.equals("Du Lieu Thoa Man")) {
            try {
                int temp = update.suaND(tenDangNhap, matKhau, hoTenND, gioiTinh, emailND, phanQuyen, trangThai);
                if (temp > 0) {
                    JOptionPane.showMessageDialog(this, "Cập Nhật Thành Công");
                    model.setRowCount(0);
                    ld.loadDataTable(table, model, jsp, null, null, null, null, "Select * From Login");
                } else {
                    JOptionPane.showMessageDialog(this, "Lỗi: Bạn Không Thể Sửa Tên Đăng Nhập");
                }
            } catch (Exception ex) {
                System.out.println("Loi: " + ex.getMessage());
            }
        }
//        try {
//            
//   
//            conn = dbc.Connect();
//
//            String sql = "Update Login Set MatKhau = ?, HoTen = ?, GioiTinh = ?, Email = ?, PhanQuyen = ?, TrangThai = ?  Where TenDN = ?";
//
//            PreparedStatement ps = conn.prepareStatement(sql);
//
//            ps.setString(7, tfTenTK.getText());
//            ps.setString(1, tfMatKhau.getText());
//            ps.setString(2, tfHoTen.getText());
//            ps.setString(3, (String) cboxGioiTinh.getSelectedItem());
//            ps.setString(4, tfEmail.getText());
//            ps.setString(5, (String) cboxPhanQuyen.getSelectedItem());
//            ps.setString(6, (String) cboxTrangThai.getSelectedItem());
//
//            int temp = ps.executeUpdate();
//            if (temp > 0) {
//                JOptionPane.showMessageDialog(this, "Đã cập nhật thông tin!");
//                DefaultTableModel model = new DefaultTableModel();
//                model.setRowCount(0);
//                table.setModel(model);
//            } else {
//                JOptionPane.showMessageDialog(this, "Loi: Chỉ sửa thông tin tài khoản đã chọn");
//            }
//
//        } catch (SQLException e) {
//            System.out.println("Loi: " + e);
//        }
    }

    public void KhoaTK() {

        try {
            conn = dbc.Connect();
            String sql = "Update Login Set TrangThai='Tạm Ngưng' Where TenDN = ?";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, tfTenTK.getText());

            int temp = ps.executeUpdate();
            if (temp > 0) {
                JOptionPane.showMessageDialog(this, "Tài khoản đã bị khóa!");
                DefaultTableModel model = new DefaultTableModel();
                model.setRowCount(0);
                table.setModel(model);
            } else {
                JOptionPane.showMessageDialog(this, "Loi: Chỉ sửa thông tin tài khoản đã chọn");
            }

        } catch (SQLException e) {
            System.out.println("Loi: " + e);
        }

    }
}
