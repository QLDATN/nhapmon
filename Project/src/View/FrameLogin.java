package View;



import javax.swing.*;

import Control.ChucNangLogin;
import Control.DBConnection;

import java.awt.*;


import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.*;

import java.awt.event.ActionEvent;

public class FrameLogin extends JFrame {

	private JPanel contentPane;
	public JTextField tFTKhoan;
	public JPasswordField passFMkhau;
	private JPanel panel;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameLogin frame = new FrameLogin();
					frame.setTitle("Đăng nhập hệ thống");
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 450);
		contentPane = new JPanel();
		//contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 241, 422);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setBackground(new Color(52, 152, 219));
		
		JLabel lblLogobk = new JLabel("");
		
//		Icon img = new ImageIcon("G:/Data Eclipse/BTL OOP/img/bk2.png");
		Icon img = new ImageIcon("bk2.png");

		
		lblLogobk.setIcon(img);
		
		lblLogobk.setBounds(27, 64, 185, 273);
		panel.add(lblLogobk);
		
		
		
		
		
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(240, 0, 394, 422);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		panel_1.setBackground(new Color(129, 207, 224));
		
		tFTKhoan = new JTextField();
		tFTKhoan.setBounds(90, 162, 230, 27);
		panel_1.add(tFTKhoan);
		tFTKhoan.setColumns(10);
		
		JLabel lblTenDN = new JLabel("Tài khoản");
		lblTenDN.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTenDN.setBounds(10, 167, 70, 14);
		panel_1.add(lblTenDN);
		
		JLabel lblMkhau = new JLabel("Mật khẩu");
		lblMkhau.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblMkhau.setBounds(10, 221, 70, 14);
		panel_1.add(lblMkhau);
		
		passFMkhau = new JPasswordField();
		passFMkhau.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					
					XuLyLogin();
					
					
				
				}
				
				
			}
		});
		passFMkhau.setBounds(90, 216, 230, 27);
		panel_1.add(passFMkhau);
		
		JLabel lbltieude = new JLabel("ĐĂNG NHẬP HỆ THỐNG");
		lbltieude.setFont(new Font("Arial", Font.BOLD, 20));
		lbltieude.setBounds(57, 52, 251, 27);
		panel_1.add(lbltieude);
		
		JButton btnDNhap = new JButton("ĐĂNG NHẬP");
		btnDNhap.setForeground(Color.WHITE);
		btnDNhap.setBackground(new Color(68,108,179));
		//2 dòng ->đổ nền cho button. stackoverflow@@
		btnDNhap.setContentAreaFilled(false);
		btnDNhap.setOpaque(true);
		
		
		btnDNhap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				XuLyLogin();
				}
		});
		btnDNhap.setFont(new Font("Arial", Font.BOLD, 15));
		btnDNhap.setBounds(90, 271, 230, 27);
		panel_1.add(btnDNhap);
	}
	
	public void closeFrame() {
		super.dispose();
	}
	public void  XuLyLogin() {
		try {
			
	        
	        DBConnection dbc = new DBConnection();
            Connection con = dbc.Connect();
	        
	        PreparedStatement pst = con.prepareStatement("SELECT TenDN,MatKhau FROM login WHERE TenDN=? and MatKhau=?");
	        pst.setString(1, tFTKhoan.getText().trim());
	        pst.setString(2, passFMkhau.getText());
			
			
			ResultSet rs = pst.executeQuery();
			int count = 0;
			
			while(rs.next()) {
				count++;
			}
		
				pst = con.prepareStatement("Select TrangThai,PhanQuyen FROM Login WHERE TenDN=?");
				pst.setString(1, tFTKhoan.getText());
				rs = pst.executeQuery();
				
				String s1 = null,s2 = null;
				while(rs.next()) {
					s1 = rs.getString("TrangThai");
					s2 = rs.getString("PhanQuyen");
				}
				
				if(count ==1 && s1.equals("Hoạt Động") && s2.equals("Admin")) {
					JOptionPane.showMessageDialog(null,"Đăng nhập thành công!");
					//gọi hàm đóng frame
					closeFrame();
					//mở frame tiếp theo
					MainFrame frame = new MainFrame();
					frame.setTitle("Quản lý đồ án");
					//frame.setResizable(false);
					frame.setVisible(true);
						
				}
				if(count ==1 && s1.equals("Hoạt Động") && s2.equals("Member")) {
					JOptionPane.showMessageDialog(null,"Đăng nhập thành công!");
					//gọi hàm đóng frame
					closeFrame();
					//mở frame tiếp theo
					MainFrame frame = new MainFrame();
					frame.setTitle("Quản lý đồ án");
					//frame.setResizable(false);
					frame.setVisible(true);
					
					frame.mnCNDL.setEnabled(false);
					frame.mnNguoiDung.setEnabled(false);
						
					
				}
					
			if(count == 1 && s1.equals("Tạm Ngưng")) {
					JOptionPane.showMessageDialog(null,"Tài khoản đang tạm khóa. Liên hệ AD để mở TK");
					tFTKhoan.requestFocus();
				}
				
			
			if(count == 0) {
				JOptionPane.showMessageDialog(null,"Tên Tài Khoản or Mật Khẩu không chính xác ???");

				tFTKhoan.requestFocus();// con chuột ở ô tên tài khoản
				
			}
					
		}  catch (SQLException e) {
			System.out.println("Loi: "+e);
		}
		
		
	}
	
}
