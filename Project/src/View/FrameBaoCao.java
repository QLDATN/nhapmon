package View;

import Control.BatLoi;
import Control.ChucNangBaoCao;
import Control.WriteToExcel;
import Model.LoadData;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class FrameBaoCao extends JFrame implements ActionListener {

    public JTabbedPane jtb;
    public JPanel jpDSCB, jpDSDoAn, jpDSMuonDoAn, jpDSMuonDoAn1, jpDSMuonDoAn2, jpDSMuonDoAn3,
            jpThanhLyDoAn, jpMuonDoAnQuaHan, jpMuonDoAnDungHan, jpThanhLyDoAn3, jpMuonDoAnQuaHan3, jpMuonDoAnDungHan3, jpTitle, jpMainBaoCao,
            jpDSCB1, jpDSCB2, jpDSCB3, jpDSDoAn1, jpDSDoAn2, jpDSDoAn3,
            jpThanhLyDoAn1, jpThanhLyDoAn2, jpMuonDoAnQuaHan1, jpMuonDoAnQuaHan2,
            jpMuonDoAnDungHan1, jpMuonDoAnDungHan2;
    public JLabel jlTitle, jlDSCB, jlDSDoAn, jlDSMuonDA1, jlDSMuonDA2, jlThanhLyDA1, jlThanhLyDA2,
            jlDAMuonQuaHan1, jlDAMuonQuaHan2, jlDAMuonDungHan1, jlDAMuonDungHan2;
    public JTextField jtfDSMuonDA1, jtfDSMuonDA2, jtfThanhLyDA1, jtfThanhLyDA2,
            jtfDAMuonQuaHan1, jtfDAMuonQuaHan2, jtfDAMuonDungHan1, jtfDAMuonDungHan2;
    public JTable jtDSCB, jtDSDoAn, jtDSMuonDoAn,
            jtThanhLyDoAn, jtMuonDoAnQuaHan, jtMuonDoAnDungHan;
    public DefaultTableModel dtmDSCB, dtmDSDoAn, dtmDSMuonDoAn,
            dtmThanhLyDoAn, dtmMuonDoAnQuaHan, dtmMuonDoAnDungHan;
    public JScrollPane jspDSCB, jspDSDoAn, jspDSMuonDoAn,
            jspThanhLyDoAn, jspMuonDoAnQuaHan, jspMuonDoAnDungHan;
    public JComboBox jcbDSCB1, jcbDSCB2, jcbDSDoAn;
    public DefaultComboBoxModel dcmDSCB1, dcmDSCB2, dcmDSDoAn;
    public JButton jbDSCB, jbDSDoAn, jbDSMuonDA, jbThanhLyDA, jbDAMuonQuaHan, jbDAMuonDungHan,
            jbTKDSCB, jbTKDSDoAn, jbTKDSMuonDA, jbTKDAThanhLy,
            jbTKDAMuonQuaHan, jbTKDAMuonDungHan;

    LoadData ld = new LoadData();

    public FrameBaoCao() {

        jtb = new JTabbedPane();
        jpDSCB = new JPanel();
        jpDSCB.setToolTipText("Danh Sách Cán Bộ Đảng Viên Theo Bộ Môn, Viện");
        jpDSDoAn = new JPanel();
        jpDSDoAn.setToolTipText("Danh Sách Đồ Án Theo Bộ Môn");
        jpDSMuonDoAn = new JPanel();
        jpDSMuonDoAn1 = new JPanel();
        jpDSMuonDoAn2 = new JPanel();
        jpDSMuonDoAn3 = new JPanel();
        jpDSMuonDoAn.setToolTipText("Danh Sách Mượn Đồ Án Theo Thời Gian");
        jpThanhLyDoAn = new JPanel();
        jpThanhLyDoAn1 = new JPanel();
        jpThanhLyDoAn2 = new JPanel();
        jpThanhLyDoAn3 = new JPanel();
        jpThanhLyDoAn.setToolTipText("Bảng Kê Đồ Án Đã Được Thanh Lý");
        jpMuonDoAnQuaHan = new JPanel();
        jpMuonDoAnQuaHan1 = new JPanel();
        jpMuonDoAnQuaHan2 = new JPanel();
        jpMuonDoAnQuaHan3 = new JPanel();
        jpMuonDoAnQuaHan.setToolTipText("Bảng Kê Cán Bộ Mượn Đồ Án Quá Hạn");
        jpMuonDoAnDungHan = new JPanel();
        jpMuonDoAnDungHan1 = new JPanel();
        jpMuonDoAnDungHan2 = new JPanel();
        jpMuonDoAnDungHan3 = new JPanel();
        jpMuonDoAnDungHan.setToolTipText("Bảng Kê Mượn Đồ Án Đúng Hạn");
        jpTitle = new JPanel();
        jpMainBaoCao = new JPanel();
        jpDSCB1 = new JPanel();
        jpDSCB2 = new JPanel();
        jpDSCB3 = new JPanel();
        jpDSDoAn1 = new JPanel();
        jpDSDoAn2 = new JPanel();
        jpDSDoAn3 = new JPanel();
        jlTitle = new JLabel("BÁO CÁO");
        jlDSCB = new JLabel("Bộ Môn : ");
        jlDSDoAn = new JLabel("Bộ Môn : ");
        jlDSMuonDA1 = new JLabel("Từ: ");
        jlDSMuonDA2 = new JLabel("Đến: ");
        jlThanhLyDA1 = new JLabel("Từ: ");
        jlThanhLyDA2 = new JLabel("Đến: ");
        jlDAMuonQuaHan1 = new JLabel("Từ: ");
        jlDAMuonQuaHan2 = new JLabel("Đến: ");
        jlDAMuonDungHan1 = new JLabel("Từ: ");
        jlDAMuonDungHan2 = new JLabel("Đến: ");
        jtfDSMuonDA1 = new JTextField(10);
        jtfDSMuonDA2 = new JTextField(10);
        jtfThanhLyDA1 = new JTextField(10);
        jtfThanhLyDA2 = new JTextField(10);
        jtfDAMuonQuaHan1 = new JTextField(10);
        jtfDAMuonQuaHan2 = new JTextField(10);
        jtfDAMuonDungHan1 = new JTextField(10);
        jtfDAMuonDungHan2 = new JTextField(10);
        jcbDSCB1 = new JComboBox();
        jcbDSCB1.setSize(100, 50);
        jcbDSCB2 = new JComboBox();
        jcbDSCB2.setSize(100, 50);
        jcbDSDoAn = new JComboBox();
        dcmDSCB1 = new DefaultComboBoxModel();
        dcmDSCB2 = new DefaultComboBoxModel();
        dcmDSDoAn = new DefaultComboBoxModel();
        jbDSCB = new JButton("OK");
        jbDSDoAn = new JButton("OK");
        jbDSMuonDA = new JButton("OK");
        jbThanhLyDA = new JButton("OK");
        jbDAMuonQuaHan = new JButton("OK");
        jbDAMuonDungHan = new JButton("OK");
        jbTKDSCB = new JButton("Xuất File");
        jbTKDSDoAn = new JButton("Xuất File");
        jbTKDSMuonDA = new JButton("Xuất File");
        jbTKDAThanhLy = new JButton("Xuất File");
        jbTKDAMuonQuaHan = new JButton("Xuất File");
        jbTKDAMuonDungHan = new JButton("Xuất File");

        jbDSCB.addActionListener(this);
        jbDSDoAn.addActionListener(this);

        jbTKDSCB.addActionListener(this);
        jbTKDSDoAn.addActionListener(this);
        jbTKDSMuonDA.addActionListener(this);
        jbTKDAThanhLy.addActionListener(this);
        jbTKDAMuonQuaHan.addActionListener(this);
        jbTKDAMuonDungHan.addActionListener(this);
        jbDSMuonDA.addActionListener(this);
        jbThanhLyDA.addActionListener(this);
        jbDAMuonQuaHan.addActionListener(this);
        jbDAMuonDungHan.addActionListener(this);

        jtDSCB = new JTable();
        jtDSDoAn = new JTable();
        jtDSMuonDoAn = new JTable();
        jtThanhLyDoAn = new JTable();
        jtMuonDoAnQuaHan = new JTable();
        jtMuonDoAnDungHan = new JTable();

        dtmDSCB = new DefaultTableModel();
        dtmDSDoAn = new DefaultTableModel();
        dtmDSMuonDoAn = new DefaultTableModel();
        dtmThanhLyDoAn = new DefaultTableModel();
        dtmMuonDoAnQuaHan = new DefaultTableModel();
        dtmMuonDoAnDungHan = new DefaultTableModel();

        jspDSCB = new JScrollPane();
        jspDSDoAn = new JScrollPane();
        jspDSMuonDoAn = new JScrollPane();
        jspThanhLyDoAn = new JScrollPane();
        jspMuonDoAnQuaHan = new JScrollPane();
        jspMuonDoAnDungHan = new JScrollPane();

        jlTitle.setFont(new Font("Arial", Font.PLAIN, 18));
        jpTitle.add(jlTitle);

        jpDSCB.setLayout(new BorderLayout());
        jpDSCB1.setBackground(Color.LIGHT_GRAY);
        jpDSCB1.add(jcbDSCB2);
        jpDSCB1.add(jlDSCB);
        jpDSCB1.add(jcbDSCB1);
        jpDSCB1.add(jbDSCB);

        jpDSCB2.setLayout(null);

        jspDSCB.setBounds(0, 0, 1350, 500);

        jpDSCB2.setBackground(Color.LIGHT_GRAY);
        jpDSCB2.add(jspDSCB);
        jpDSCB.add(jpDSCB1, BorderLayout.PAGE_START);
        jpDSCB.add(jpDSCB2, BorderLayout.CENTER);
        jpDSCB3.setBounds(670, 520, 70, 30);
        jpDSCB2.add(jpDSCB3);

        jpDSDoAn.setLayout(new BorderLayout());
        jpDSDoAn1.setBackground(Color.LIGHT_GRAY);
        jpDSDoAn1.add(jlDSDoAn);
        jpDSDoAn1.add(jcbDSDoAn);
        jpDSDoAn1.add(jbDSDoAn);
        jpDSDoAn2.setLayout(null);
        jspDSDoAn.setBounds(0, 0, 1350, 500);
        jpDSDoAn2.setBackground(Color.LIGHT_GRAY);
        //jpDSDoAn2.setBackground(Color.red);
        jpDSDoAn2.add(jspDSDoAn);
        jpDSDoAn.add(jpDSDoAn1, BorderLayout.PAGE_START);
        jpDSDoAn.add(jpDSDoAn2, BorderLayout.CENTER);

        jpDSDoAn3.setBounds(670, 520, 70, 30);
        jpDSDoAn2.add(jpDSDoAn3);

        jpMainBaoCao.setLayout(new BorderLayout());
        jpMainBaoCao.add(jtb);
        jtb.addTab("Danh Sách Cán Bộ - Đảng Viên", jpDSCB);
        jtb.addTab("Danh Sách Đồ Án", jpDSDoAn);
        jtb.addTab("Danh Sách Mượn Đồ Án", jpDSMuonDoAn);
        jtb.addTab("Đồ Án Được Thanh Lý", jpThanhLyDoAn);
        jtb.addTab("Đồ Án Mượn Quá Hạn", jpMuonDoAnQuaHan);
        jtb.addTab("Đồ Án Mượn Đúng Hạn", jpMuonDoAnDungHan);

        jpThanhLyDoAn.setLayout(new BorderLayout());
        jspThanhLyDoAn.setBounds(0, 0, 1350, 500);
        jpThanhLyDoAn.setBackground(Color.LIGHT_GRAY);
        jpDSMuonDoAn.setLayout(new BorderLayout());
        jspDSMuonDoAn.setBounds(0, 0, 1350, 500);
        jpDSMuonDoAn.setBackground(Color.LIGHT_GRAY);
        jpMuonDoAnQuaHan.setLayout(new BorderLayout());
        jspMuonDoAnQuaHan.setBounds(0, 0, 1350, 500);
        jpMuonDoAnQuaHan.setBackground(Color.LIGHT_GRAY);
        jpMuonDoAnDungHan.setLayout(new BorderLayout());
        jspMuonDoAnDungHan.setBounds(0, 0, 1350, 500);
        jpMuonDoAnDungHan.setBackground(Color.LIGHT_GRAY);

        jpDSMuonDoAn2.setLayout(null);
        jpThanhLyDoAn2.setLayout(null);
        jpMuonDoAnQuaHan2.setLayout(null);
        jpMuonDoAnDungHan2.setLayout(null);

        //jpThanhLyDoAn2.setBounds(0, 0, 1350, 800);
        //jpThanhLyDoAn3.setBounds(0, 500, 1350, 300);
        jpThanhLyDoAn.add(jpThanhLyDoAn1, BorderLayout.PAGE_START);
        jpThanhLyDoAn1.setBackground(Color.LIGHT_GRAY);
        jpThanhLyDoAn1.add(jlThanhLyDA1);
        jpThanhLyDoAn1.add(jtfThanhLyDA1);
        jpThanhLyDoAn1.add(jlThanhLyDA2);
        jpThanhLyDoAn1.add(jtfThanhLyDA2);
        jpThanhLyDoAn1.add(jbThanhLyDA);
        jpThanhLyDoAn.add(jpThanhLyDoAn2, BorderLayout.CENTER);
        jpThanhLyDoAn2.add(jspThanhLyDoAn);
        jpThanhLyDoAn3.setBounds(670, 530, 70, 30);
        jpThanhLyDoAn2.add(jpThanhLyDoAn3);

        //jpDSMuonDoAn2.setBounds(0, 0, 1350, 800);
        //jpDSMuonDoAn3.setBounds(0, 500, 1350, 300);
        jpDSMuonDoAn.add(jpDSMuonDoAn1, BorderLayout.PAGE_START);
        jpDSMuonDoAn1.setBackground(Color.LIGHT_GRAY);
        jpDSMuonDoAn1.add(jlDSMuonDA1);
        jpDSMuonDoAn1.add(jtfDSMuonDA1);
        jpDSMuonDoAn1.add(jlDSMuonDA2);
        jpDSMuonDoAn1.add(jtfDSMuonDA2);
        jpDSMuonDoAn1.add(jbDSMuonDA);
        jpDSMuonDoAn.add(jpDSMuonDoAn2, BorderLayout.CENTER);
        jpDSMuonDoAn2.add(jspDSMuonDoAn);
        jpDSMuonDoAn3.setBounds(670, 530, 70, 30);
        jpDSMuonDoAn2.add(jpDSMuonDoAn3);

        //jpMuonDoAnQuaHan2.setBounds(0, 0, 1350, 800);
        //jpMuonDoAnQuaHan3.setBounds(0, 500, 1350, 300);
        jpMuonDoAnQuaHan.add(jpMuonDoAnQuaHan1, BorderLayout.PAGE_START);
        jpMuonDoAnQuaHan1.setBackground(Color.LIGHT_GRAY);
        jpMuonDoAnQuaHan1.add(jlDAMuonQuaHan1);
        jpMuonDoAnQuaHan1.add(jtfDAMuonQuaHan1);
        jpMuonDoAnQuaHan1.add(jlDAMuonQuaHan2);
        jpMuonDoAnQuaHan1.add(jtfDAMuonQuaHan2);
        jpMuonDoAnQuaHan1.add(jbDAMuonQuaHan);
        jpMuonDoAnQuaHan.add(jpMuonDoAnQuaHan2, BorderLayout.CENTER);
        jpMuonDoAnQuaHan2.add(jspMuonDoAnQuaHan);
        jpMuonDoAnQuaHan3.setBounds(670, 530, 70, 30);
        jpMuonDoAnQuaHan2.add(jpMuonDoAnQuaHan3);

        //jpMuonDoAnDungHan2.setBounds(0, 0, 1350, 800);
        //jpMuonDoAnDungHan3.setBounds(0, 500, 1350, 300);
        jpMuonDoAnDungHan.add(jpMuonDoAnDungHan1, BorderLayout.PAGE_START);
        jpMuonDoAnDungHan1.setBackground(Color.LIGHT_GRAY);
        jpMuonDoAnDungHan1.add(jlDAMuonDungHan1);
        jpMuonDoAnDungHan1.add(jtfDAMuonDungHan1);
        jpMuonDoAnDungHan1.add(jlDAMuonDungHan2);
        jpMuonDoAnDungHan1.add(jtfDAMuonDungHan2);
        jpMuonDoAnDungHan1.add(jbDAMuonDungHan);
        jpMuonDoAnDungHan.add(jpMuonDoAnDungHan2, BorderLayout.CENTER);
        jpMuonDoAnDungHan2.add(jspMuonDoAnDungHan);
        jpMuonDoAnDungHan3.setBounds(670, 530, 70, 30);
        jpMuonDoAnDungHan2.add(jpMuonDoAnDungHan3);

        jpDSCB3.setBackground(Color.LIGHT_GRAY);
        jpDSDoAn3.setBackground(Color.LIGHT_GRAY);
        jpDSMuonDoAn3.setBackground(Color.LIGHT_GRAY);
        jpThanhLyDoAn3.setBackground(Color.LIGHT_GRAY);
        jpMuonDoAnQuaHan3.setBackground(Color.LIGHT_GRAY);
        jpMuonDoAnDungHan3.setBackground(Color.LIGHT_GRAY);

        jpMuonDoAnDungHan2.setBackground(Color.LIGHT_GRAY);
        jpMuonDoAnQuaHan2.setBackground(Color.LIGHT_GRAY);
        jpDSMuonDoAn2.setBackground(Color.LIGHT_GRAY);
        jpThanhLyDoAn2.setBackground(Color.LIGHT_GRAY);

        jpDSCB3.add(jbTKDSCB);
        jpDSDoAn3.add(jbTKDSDoAn);
        jpDSMuonDoAn3.add(jbTKDSMuonDA);
        jpThanhLyDoAn3.add(jbTKDAThanhLy);
        jpMuonDoAnQuaHan3.add(jbTKDAMuonQuaHan);
        jpMuonDoAnDungHan3.add(jbTKDAMuonDungHan);

//      Danh Sach Muon Do An
//      Đồ Án được thanh lý
        
//      Mượn Đồ Án quá hạn        
        
//      Mượn Đồ Án đúng hạn        
        
        this.setLayout(new BorderLayout());
        this.getContentPane().add(jpTitle, BorderLayout.PAGE_START);
        this.getContentPane().add(jpMainBaoCao, BorderLayout.CENTER);

        this.setSize(1000, 600);
        //this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);

        dcmDSCB2.addElement("DS Cán Bộ");
        dcmDSCB2.addElement("DS Đảng Viên");
        jcbDSCB2.setModel(dcmDSCB2);

        ld.loadCombobox(jcbDSCB1, "Select Bomon From GiangVien Group By Bomon", "Bomon");
        ld.loadCombobox(jcbDSDoAn, "Select Bomon From GiangVien Group By Bomon", "Bomon");

    }

    public static void main(String[] args) {
        new FrameBaoCao().setVisible(true);
    }

    ChucNangBaoCao baoCao = new ChucNangBaoCao();
//    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == jbDSCB) {
            
            baoCao.DSCB(jtDSCB, dtmDSCB, jspDSCB, jcbDSCB1, jcbDSCB2);
        }
        if (e.getSource() == jbDSDoAn) {
            
            baoCao.DSDoAn(jtDSDoAn, dtmDSDoAn, jspDSDoAn, jcbDSDoAn);
        }

        BatLoi batLoi = new BatLoi();
        String ngayBD = "";
        String ngayKT = "";
        if (e.getSource() == jbDSMuonDA) {
            ngayBD = jtfDSMuonDA1.getText();
            ngayKT = jtfDSMuonDA2.getText();
            String kt = batLoi.ktNhapBaoCao(ngayBD, ngayKT);
            if(kt.equals("Ban chua nhap du lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            }
            else if(kt.equals("Ban chua nhap ngay bat dau")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bắt Đầu");
            }
            else if(kt.equals("Ngay bat dau khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Bắt Đầu Không Đúng Định Dạng");
            }
            else if(kt.equals("Ban chua nhap ngay ket thuc")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Kết Thúc");
            }
            else if(kt.equals("Ngay ket thuc khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Không Đúng Định Dạng");
            }
            else{
                baoCao.DSMuonDoAn(jtDSMuonDoAn, dtmDSMuonDoAn, jspDSMuonDoAn, jtfDSMuonDA1, jtfDSMuonDA2);
            }
        }

        if (e.getSource() == jbThanhLyDA) {
            ngayBD = jtfThanhLyDA1.getText();
            ngayKT = jtfThanhLyDA2.getText();
            String kt = batLoi.ktNhapBaoCao(ngayBD, ngayKT);
            if(kt.equals("Ban chua nhap du lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            }
            else if(kt.equals("Ban chua nhap ngay bat dau")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bắt Đầu");
            }
            else if(kt.equals("Ngay bat dau khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Bắt Đầu Không Đúng Định Dạng");
            }
            else if(kt.equals("Ban chua nhap ngay ket thuc")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Kết Thúc");
            }
            else if(kt.equals("Ngay ket thuc khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Không Đúng Định Dạng");
            }
            else{
                baoCao.DAThanhLy(jtThanhLyDoAn, dtmThanhLyDoAn, jspThanhLyDoAn, jtfThanhLyDA1, jtfThanhLyDA2);
            }
        }

        if (e.getSource() == jbDAMuonQuaHan) {
            ngayBD = jtfDAMuonQuaHan1.getText();
            ngayKT = jtfDAMuonQuaHan2.getText();
            String kt = batLoi.ktNhapBaoCao(ngayBD, ngayKT);
            if(kt.equals("Ban chua nhap du lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            }
            else if(kt.equals("Ban chua nhap ngay bat dau")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bắt Đầu");
            }
            else if(kt.equals("Ngay bat dau khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Bắt Đầu Không Đúng Định Dạng");
            }
            else if(kt.equals("Ban chua nhap ngay ket thuc")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Kết Thúc");
            }
            else if(kt.equals("Ngay ket thuc khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Không Đúng Định Dạng");
            }
            else{
                baoCao.DAMuonQuaHan(jtMuonDoAnQuaHan, dtmMuonDoAnQuaHan, jspMuonDoAnQuaHan, jtfDAMuonQuaHan1, jtfDAMuonQuaHan2);
            }
        }

        if (e.getSource() == jbDAMuonDungHan) {
            ngayBD = jtfDAMuonDungHan1.getText();
            ngayKT = jtfDAMuonDungHan2.getText();
            String kt = batLoi.ktNhapBaoCao(ngayBD, ngayKT);
            if(kt.equals("Ban chua nhap du lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            }
            else if(kt.equals("Ban chua nhap ngay bat dau")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bắt Đầu");
            }
            else if(kt.equals("Ngay bat dau khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Bắt Đầu Không Đúng Định Dạng");
            }
            else if(kt.equals("Ban chua nhap ngay ket thuc")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Kết Thúc");
            }
            else if(kt.equals("Ngay ket thuc khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Không Đúng Định Dạng");
            }
            else{
                baoCao.DAMuonDunghan(jtMuonDoAnDungHan, dtmMuonDoAnDungHan, jspMuonDoAnDungHan, jtfDAMuonDungHan1, jtfDAMuonDungHan2);
            }
        }

        WriteToExcel wte = new WriteToExcel();
        if (e.getSource() == jbTKDSCB) {
            boolean kt = wte.writeExcell(jtDSCB, new File("BaoCaoDSCB-DV.xlsx"));
            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }
        }
        if (e.getSource() == jbTKDSDoAn) {
            boolean kt = wte.writeExcell(jtDSDoAn, new File("BaoCaoDSDoAn.xlsx"));
            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }
        }
        if (e.getSource() == jbTKDSMuonDA) {
            boolean kt = wte.writeExcell(jtDSMuonDoAn, new File("BaoCaoMuonDA.xlsx"));
            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }
        }
        if (e.getSource() == jbTKDAThanhLy) {
            boolean kt = wte.writeExcell(jtThanhLyDoAn, new File("BaoCaoDAThanhLy.xlsx"));
            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }
        }
        if (e.getSource() == jbTKDAMuonQuaHan) {

            boolean kt = wte.writeExcell(jtMuonDoAnQuaHan, new File("BaoCaoDAMuonQH.xlsx"));
            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }

        }
        if (e.getSource() == jbTKDAMuonDungHan) {
            boolean kt = wte.writeExcell(jtMuonDoAnDungHan, new File("BaoCaoDAMuonDH.xlsx"));

            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }
        }

    }

}
