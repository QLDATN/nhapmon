/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Control.ChucNangThongKe;
import Control.WriteToExcel;
import Model.LoadData;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.BoxLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Asus
 */
public class FrameThongKe extends JFrame implements ActionListener {

    JLabel jlTitle, jlDAMuon;
    //JTextField jtfDAMuon;
    JTabbedPane jtp;
    JPanel jpTitle, jpMainTK, jpTKDAMuon, jpTKCBCongTac, jpTKCBTheoChucDanh, jpTKDangVien, jpTKDAMuon1, jpTKDAMuon2, jpTKCBCongTac1, jpTKCBCongTac2,
            jpTKCBTheoChucDanh1, jpTKCBTheoChucDanh2, jpTKCBTheoChucDanh3, jpTKDangVien1, jpTKDangVien2, jpTKDangVien3, jpTKCBCongTac3, jpTKDAMuon3;
    JComboBox jcbTKDAMuon, jcbTKCBCongTac, jcbTKCBTheoChucDanh, jcbTKDangVien;
    DefaultComboBoxModel dcmTKDAMuon, dcmTKCBCongTac, dcmTKCBTheoChucDanh, dcmTKDangVien;
    JButton jbTKDAMuon, jbTKCBCongTac, jbTKCBTheoChucDanh, jbTKDangVien, jbPrintExcel1, jbPrintExcel2, jbPrintExcel3, jbPrintExcel4;
    JTable jtTKDAMuon, jtTKCBCongTac, jtTKCBTheoChucDanh, jtTKDangVien;
    DefaultTableModel dtmTKDAMuon, dtmTKCBCongTac, dtmTKCBTheoChucDanh, dtmTKDangVien;
    JScrollPane jspTKDAMuon, jspTKCBCongTac, jspTKCBTheoChucDanh, jspTKDangVien;

    LoadData ld = new LoadData();

    public FrameThongKe() {

        jlTitle = new JLabel("Thống Kê");
        jlDAMuon = new JLabel("Năm học: ");
        //jtfDAMuon = new JTextField(5);
        jtp = new JTabbedPane();
        jpTKCBCongTac = new JPanel();
        jpTKCBTheoChucDanh = new JPanel();
        jpTKDAMuon = new JPanel();
        jpTKDangVien = new JPanel();

        jpTKCBCongTac1 = new JPanel();
        jpTKCBTheoChucDanh1 = new JPanel();
        jpTKDAMuon1 = new JPanel();
        jpTKDangVien1 = new JPanel();

        jpTKCBCongTac2 = new JPanel();
        jpTKCBCongTac3 = new JPanel();
        jpTKCBTheoChucDanh2 = new JPanel();
        jpTKCBTheoChucDanh3 = new JPanel();
        jpTKDAMuon2 = new JPanel();
        jpTKDAMuon3 = new JPanel();
        jpTKDangVien2 = new JPanel();
        jpTKDangVien3 = new JPanel();

        jpTitle = new JPanel();
        jpMainTK = new JPanel();
        jcbTKCBCongTac = new JComboBox();
        jcbTKCBTheoChucDanh = new JComboBox();
        jcbTKDAMuon = new JComboBox();
        jcbTKDangVien = new JComboBox();

        dcmTKCBCongTac = new DefaultComboBoxModel();
        dcmTKCBTheoChucDanh = new DefaultComboBoxModel();
        dcmTKDAMuon = new DefaultComboBoxModel();
        dcmTKDangVien = new DefaultComboBoxModel();

        jbTKCBCongTac = new JButton("OK");
        jbTKCBTheoChucDanh = new JButton("OK");
        jbTKDAMuon = new JButton("OK");
        jbTKDangVien = new JButton("OK");
        jtTKCBCongTac = new JTable();
        jtTKCBTheoChucDanh = new JTable();
        jtTKDAMuon = new JTable();
        jtTKDangVien = new JTable();
        dtmTKCBCongTac = new DefaultTableModel();
        dtmTKCBTheoChucDanh = new DefaultTableModel();
        dtmTKDAMuon = new DefaultTableModel();
        dtmTKDangVien = new DefaultTableModel();
        jspTKCBCongTac = new JScrollPane();
        jspTKCBTheoChucDanh = new JScrollPane();
        jspTKDAMuon = new JScrollPane();
        jspTKDangVien = new JScrollPane();

        jlTitle.setFont(new Font("Arial", Font.PLAIN, 18));
        jpTitle.add(jlTitle);

        jpMainTK.setLayout(new BorderLayout());
        jpMainTK.add(jtp);

        jtp.addTab("Thống Kê Đồ Án Mượn", jpTKDAMuon);
        jtp.addTab("Thống Kê Cán Bộ Công Tác", jpTKCBCongTac);
        jtp.addTab("Thống Kê Cán Bộ Theo Bộ Môn", jpTKCBTheoChucDanh);
        jtp.addTab("Thống Kê Đảng Viên", jpTKDangVien);

        jpTKDAMuon.setLayout(new BorderLayout());
        jpTKDAMuon1.add(jlDAMuon);
        jpTKDAMuon1.add(jcbTKDAMuon);
        jpTKDAMuon1.add(jbTKDAMuon);
        jpTKDAMuon.add(jpTKDAMuon1, BorderLayout.PAGE_START);
        jpTKDAMuon.add(jpTKDAMuon2, BorderLayout.CENTER);

        jpTKDAMuon2.setBackground(Color.LIGHT_GRAY);
        jpTKCBCongTac2.setBackground(Color.LIGHT_GRAY);
        jpTKCBTheoChucDanh2.setBackground(Color.LIGHT_GRAY);
        jpTKDangVien2.setBackground(Color.LIGHT_GRAY);

        //jpTKDAMuon2.setBackground(Color.BLUE);
        jpTKDAMuon2.setLayout(null);
        jspTKDAMuon.setBounds(0, 0, 1350, 500);
        jpTKDAMuon3.setBounds(670, 520, 70, 30);
        jpTKDAMuon2.add(jpTKDAMuon3);
        jspTKDAMuon.setBackground(Color.LIGHT_GRAY);
        jpTKDAMuon2.add(jspTKDAMuon);

        jpTKCBCongTac.setLayout(new BorderLayout());
        jpTKCBCongTac1.add(jcbTKCBCongTac);
        jpTKCBCongTac1.add(jbTKCBCongTac);
        jpTKCBCongTac.add(jpTKCBCongTac1, BorderLayout.PAGE_START);
        jpTKCBCongTac.add(jpTKCBCongTac2, BorderLayout.CENTER);

        //jpTKCBCongTac2.setBackground(Color.BLUE);
        jpTKCBCongTac2.setLayout(null);
        jspTKCBCongTac.setBounds(0, 0, 1350, 500);
        jspTKCBCongTac.setBackground(Color.LIGHT_GRAY);
        jpTKCBCongTac2.add(jspTKCBCongTac, BorderLayout.CENTER);
        jpTKCBCongTac3.setBounds(670, 520, 70, 30);
        jpTKCBCongTac2.add(jpTKCBCongTac3);

        jpTKCBTheoChucDanh.setLayout(new BorderLayout());
        jpTKCBTheoChucDanh1.add(jcbTKCBTheoChucDanh);
        jpTKCBTheoChucDanh1.add(jbTKCBTheoChucDanh);
        jpTKCBTheoChucDanh.add(jpTKCBTheoChucDanh1, BorderLayout.PAGE_START);
        jpTKCBTheoChucDanh.add(jpTKCBTheoChucDanh2, BorderLayout.CENTER);

        //jpTKCBTheoChucDanh2.setBackground(Color.BLUE);
        jpTKCBTheoChucDanh2.setLayout(null);
        jspTKCBTheoChucDanh.setBounds(0, 0, 1350, 500);
        jspTKCBTheoChucDanh.setBackground(Color.LIGHT_GRAY);
        jpTKCBTheoChucDanh2.add(jspTKCBTheoChucDanh);
        jpTKCBTheoChucDanh3.setBounds(670, 520, 70, 30);
        jpTKCBTheoChucDanh2.add(jpTKCBTheoChucDanh3);

        jpTKDangVien.setLayout(new BorderLayout());
        jpTKDangVien1.add(jcbTKDangVien);

        jpTKDangVien1.add(jbTKDangVien);
        jpTKDangVien.add(jpTKDangVien1, BorderLayout.PAGE_START);
        jpTKDangVien.add(jpTKDangVien2, BorderLayout.CENTER);

        jpTKDangVien.setBackground(Color.LIGHT_GRAY);
        jpTKDangVien2.setLayout(null);
        jspTKDangVien.setBounds(0, 0, 1350, 500);
        jpTKDangVien2.add(jspTKDangVien);
        jpTKDangVien3.setBounds(670, 520, 70, 30);
        jpTKDangVien2.add(jpTKDangVien3);

        ld.loadCombobox(jcbTKDAMuon, "Select DISTINCT Year(Ngaymuon) AS Year From MuonDoAn", "Year");

        jbPrintExcel1 = new JButton("Xuất File");
        jbPrintExcel1.setToolTipText("In bảng ra file ThongKe1.xlsx");
        jpTKDAMuon3.add(jbPrintExcel1);
        jpTKDAMuon3.setBackground(Color.LIGHT_GRAY);
        ld.loadCombobox(jcbTKCBCongTac, "Select Bomon From GiangVien Group By Bomon", "Bomon");

        jbPrintExcel2 = new JButton("Xuất File");
        jbPrintExcel2.setToolTipText("In bảng ra file ThongKe2.xlsx");
        jpTKCBCongTac3.add(jbPrintExcel2);
        jpTKCBCongTac3.setBackground(Color.LIGHT_GRAY);
        ld.loadCombobox(jcbTKCBTheoChucDanh, "Select Bomon From GiangVien Group By Bomon", "Bomon");

        jbPrintExcel3 = new JButton("Xuất File");
        jbPrintExcel3.setToolTipText("In bảng ra file ThongKe3.xlsx");
        jpTKCBTheoChucDanh3.add(jbPrintExcel3);
        jpTKCBTheoChucDanh3.setBackground(Color.LIGHT_GRAY);
        ld.loadCombobox(jcbTKDangVien, "Select Bomon From GiangVien Group By Bomon", "Bomon");

        jbPrintExcel4 = new JButton("Xuất File");
        jbPrintExcel4.setToolTipText("In bảng ra file ThongKe4.xlsx");
        //jbPrintExcel4.setSize(70, 40);
        jpTKDangVien3.add(jbPrintExcel4);
        jpTKDangVien3.setBackground(Color.LIGHT_GRAY);

        this.getContentPane().add(jpTitle, BorderLayout.PAGE_START);
        this.getContentPane().add(jpMainTK, BorderLayout.CENTER);
        this.setSize(900, 600);
        //this.setVisible(true);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        jbTKDAMuon.addActionListener(this);
        jbTKCBCongTac.addActionListener(this);
        jbTKCBTheoChucDanh.addActionListener(this);
        jbTKDangVien.addActionListener(this);
        jbPrintExcel1.addActionListener(this);
        jbPrintExcel2.addActionListener(this);
        jbPrintExcel3.addActionListener(this);
        jbPrintExcel4.addActionListener(this);

    }

    public static void main(String[] args) {
        new FrameThongKe().setVisible(true);
    }

    ChucNangThongKe thongKe = new ChucNangThongKe();

    WriteToExcel wte = new WriteToExcel();

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == jbTKCBCongTac) {
            thongKe.TKCBCongTac(jtTKCBCongTac, dtmTKCBCongTac, jspTKCBCongTac, jcbTKCBCongTac);
        }

        if (e.getSource() == jbTKCBTheoChucDanh) {
            thongKe.TKCBTheoChucDanh(jtTKCBTheoChucDanh, dtmTKCBTheoChucDanh, jspTKCBTheoChucDanh, jcbTKCBTheoChucDanh);
        }

        if (e.getSource() == jbTKDangVien) {
            thongKe.TKDangVien(jtTKDangVien, dtmTKDangVien, jspTKDangVien, jcbTKDangVien);
        }

        if (e.getSource() == jbTKDAMuon) {
            
            thongKe.TKDAMuon(jtTKDAMuon, dtmTKDAMuon, jspTKDAMuon, jcbTKDAMuon);
            
        }

        if (e.getSource() == jbPrintExcel1) {

            //click button ok rồi click print. nếu 0 sẽ ko load đc jtable
            boolean kt = wte.writeExcell(jtTKDAMuon, new File("TKDAMuon.xlsx"));
            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }
        }

        if (e.getSource() == jbPrintExcel2) {

            boolean kt = wte.writeExcell(jtTKCBCongTac, new File("TKCBCongTac.xlsx"));
            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }

        }

        if (e.getSource() == jbPrintExcel3) {

            boolean kt = wte.writeExcell(jtTKCBTheoChucDanh, new File("TKCBTheoChucDanh.xlsx"));
            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }
        }

        if (e.getSource() == jbPrintExcel4) {

            boolean kt = wte.writeExcell(jtTKDangVien, new File("TKDangVien.xlsx"));

            if (kt) {
                JOptionPane.showMessageDialog(null, "Xuất File Thành Công");
            } else {
                JOptionPane.showMessageDialog(null, "Xuất File Thất Bại");
            }

        }

    }

}
