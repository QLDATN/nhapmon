package View;

import Control.BatLoi;
import Control.ChucNangTimKiem;
import Model.LoadData;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class FrameTimKiem extends JFrame  implements ActionListener{

    public JTabbedPane jtb;
    public JPanel jpTKDSCB, jpTKDSDoAn, jpTKDSMuonDoAn,
            jpTKThanhLyDoAn, jpTKMuonDoAnQuaHan, jpTKMuonDoAnDungHan,
            jpTKTitle, jpMainTimKiem,
            jpTKDSCB1, jpTKDSCB2, jpTKDSDoAn1, jpTKDSDoAn2, jpTKDSMuonDoAn1, jpTKDSMuonDoAn2,
            jpTKThanhLyDoAn1, jpTKThanhLyDoAn2, jpTKMuonDoAnQuaHan1, jpTKMuonDoAnQuaHan2,
            jpTKMuonDoAnDungHan1, jpTKMuonDoAnDungHan2, jpTKDSCB3, jpTKDSDoAn3, jpTKDSMuonDoAn3, 
            jpTKThanhLyDoAn3, jpTKMuonDoAnQuaHan3, jpTKMuonDoAnDungHan3;
    public JLabel jlTKTitle, jlTKDSCB, jlTKDSDoAn, jlTKMuonDA1, jlTKMuonDA2, jlTKThanhLyDA1, jlTKThanhLyDA2,
            jlTKDAMuonQH1, jlTKDAMuonQH2, jlTKDAMuonDH1, jlTKDAMuonDH2, jlTKDSCB3, jlTKDSDoAn3, 
            jlTKMuonDA3, jlTKThanhLyDA3, jlTKDAMuonQH3, jlTKDAMuonDungHan3;
    public JTextField  jtfTKMuonDA1, jtfTKMuonDA2, jtfTKThanhLyDA1, jtfTKThanhLyDA2,
            jtfTKDAMuonQH1, jtfTKDAMuonQH2, jtfTKDAMuonDH1, jtfTKDAMuonDH2, jtfTKDSCB3, jtfTKDSDoAn3,
            jtfTKMuonDA3, jtfTKThanhLyDA3, jtfTKDAMuonQH3, jtfTKDAMuonDH3;
    public JTable jtTKDSCB, jtTKDSDoAn, jtTKDSMuonDoAn,
            jtTKThanhLyDoAn, jtTKMuonDoAnQuaHan, jtTKMuonDoAnDungHan;
    public DefaultTableModel dtmTKDSCB, dtmTKDSDoAn, dtmTKDSMuonDoAn,
            dtmTKThanhLyDoAn, dtmTKMuonDoAnQuaHan, dtmTKMuonDoAnDungHan;
    public JScrollPane jspTKDSCB, jspTKDSDoAn, jspTKDSMuonDoAn,
            jspTKThanhLyDoAn, jspTKMuonDoAnQuaHan, jspTKMuonDoAnDungHan;
    public JComboBox jcbTKDSCB1, jcbTKDSCB2, jcbTKDSDoAn;
    public DefaultComboBoxModel dcmTKDSCB1, dcmTKDSCB2, dcmTKDSDoAn;
    public JButton jbTKDSCB, jbTKDSDoAn, jbTKMuonDA, jbTKThanhLyDA, jbTKDAMuonQH, jbTKDAMuonDH,
            jbTKDSCB3, jbTKDSDoAn3, jbTKMuonDA3, jbTKThanhLyDA3, jbTKDAMuonQH3, jbTKDAMuonDH3;

    LoadData ld = new LoadData();
    ChucNangTimKiem tk = new ChucNangTimKiem();

    public FrameTimKiem() {

        jtb = new JTabbedPane();
        jtb.setSize(1000, 800);
        jpTKDSCB = new JPanel();
        jpTKDSCB.setToolTipText("Danh Sách Cán Bộ Đảng Viên Theo Bộ Môn, Viện");
        jpTKDSDoAn = new JPanel();
        jpTKDSDoAn.setToolTipText("Danh Sách Đồ Án Theo Bộ Môn");
        jpTKDSMuonDoAn = new JPanel();
        jpTKDSMuonDoAn.setToolTipText("Danh Sách Mượn Đồ Án Theo Thời Gian");
        jpTKThanhLyDoAn = new JPanel();
        jpTKThanhLyDoAn.setToolTipText("Danh Sách Đồ Án Đã Được Thanh Lý");
        jpTKMuonDoAnQuaHan = new JPanel();
        jpTKMuonDoAnQuaHan.setToolTipText("Danh Sách Mượn Đồ Án Quá Hạn");
        jpTKMuonDoAnDungHan = new JPanel();
        jpTKMuonDoAnDungHan.setToolTipText("Danh Sách Trả Đồ Án Đúng Hạn");
        jpTKTitle = new JPanel();
        jpMainTimKiem = new JPanel();
        jpTKDSCB1 = new JPanel();
        jpTKDSCB2 = new JPanel();
        jpTKDSCB3 = new JPanel();
        jpTKDSDoAn1 = new JPanel();
        jpTKDSDoAn2 = new JPanel();
        jpTKDSDoAn3 = new JPanel();
        jpTKDSMuonDoAn1 = new JPanel();
        jpTKDSMuonDoAn2 = new JPanel();
        jpTKDSMuonDoAn3 = new JPanel();
        jpTKThanhLyDoAn1 = new JPanel();
        jpTKThanhLyDoAn2 = new JPanel();
        jpTKThanhLyDoAn3 = new JPanel();
        jpTKMuonDoAnQuaHan1 = new JPanel();
        jpTKMuonDoAnQuaHan2 = new JPanel();
        jpTKMuonDoAnQuaHan3 = new JPanel();
        jpTKMuonDoAnDungHan1 = new JPanel();
        jpTKMuonDoAnDungHan2 = new JPanel();
        jpTKMuonDoAnDungHan3 = new JPanel();
        
        jlTKTitle = new JLabel("Tìm Kiếm");
        jlTKDSCB = new JLabel("Bộ Môn : ");
        jlTKDSDoAn = new JLabel("Bộ Môn : ");
        jlTKMuonDA1 = new JLabel("Từ: ");
        jlTKThanhLyDA1 = new JLabel("Từ: ");
        jlTKDAMuonQH1 = new JLabel("Từ: ");
        jlTKDAMuonDH1 = new JLabel("Từ: ");
        jlTKMuonDA2 = new JLabel("Đến: ");
        jlTKThanhLyDA2 = new JLabel("Đến: ");
        jlTKDAMuonQH2 = new JLabel("Đến: ");
        jlTKDAMuonDH2 = new JLabel("Đến: ");
 
        jtfTKMuonDA1 = new JTextField(10);
        jtfTKMuonDA2 = new JTextField(10);
        jtfTKThanhLyDA1 = new JTextField(10);
        jtfTKThanhLyDA2 = new JTextField(10);
        jtfTKDAMuonQH1 = new JTextField(10);
        jtfTKDAMuonQH2 = new JTextField(10);
        jtfTKDAMuonDH1 = new JTextField(10);
        jtfTKDAMuonDH2 = new JTextField(10);
        jtfTKDSCB3 = new JTextField(30);
        jtfTKDSDoAn3 = new JTextField(30);
        jtfTKMuonDA3 = new JTextField(30);
        jtfTKThanhLyDA3 = new JTextField(30);
        jtfTKDAMuonQH3 = new JTextField(30);
        jtfTKDAMuonDH3 = new JTextField(30);
        
        jcbTKDSCB1 = new JComboBox();
        jcbTKDSCB1.setSize(100, 50);
        jcbTKDSCB2 = new JComboBox();
        jcbTKDSCB2.setSize(100, 50);
        jcbTKDSDoAn = new JComboBox();
        dcmTKDSCB1 = new DefaultComboBoxModel();
        dcmTKDSCB2 = new DefaultComboBoxModel();
        dcmTKDSDoAn = new DefaultComboBoxModel();
        
        jbTKDSCB = new JButton("OK");
        jbTKDSDoAn = new JButton("OK");
        jbTKMuonDA = new JButton("OK");
        jbTKThanhLyDA = new JButton("OK");
        jbTKDAMuonQH = new JButton("OK");
        jbTKDAMuonDH = new JButton("OK");
        jbTKDSCB3 = new JButton("Tìm Kiếm");
        jbTKDSDoAn3 = new JButton("Tìm Kiếm");
        jbTKMuonDA3 = new JButton("Tìm Kiếm");
        jbTKThanhLyDA3 = new JButton("Tìm Kiếm");
        jbTKDAMuonQH3 = new JButton("Tìm Kiếm");
        jbTKDAMuonDH3 = new JButton("Tìm Kiếm");

        jbTKDSCB.addActionListener(this);
        jbTKDSDoAn.addActionListener(this);
        jbTKMuonDA.addActionListener(this);
        jbTKThanhLyDA.addActionListener(this);
        jbTKDAMuonQH.addActionListener(this);
        jbTKDAMuonDH.addActionListener(this);
        jbTKDSCB3.addActionListener(this);
        jbTKDSDoAn3.addActionListener(this);
        jbTKMuonDA3.addActionListener(this);
        jbTKThanhLyDA3.addActionListener(this);
        jbTKDAMuonQH3.addActionListener(this);
        jbTKDAMuonDH3.addActionListener(this);

        jtTKDSCB = new JTable();
        jtTKDSDoAn = new JTable();
        jtTKDSMuonDoAn = new JTable();
        jtTKThanhLyDoAn = new JTable();
        jtTKMuonDoAnQuaHan = new JTable();
        jtTKMuonDoAnDungHan = new JTable();

        dtmTKDSCB = new DefaultTableModel();
        dtmTKDSDoAn = new DefaultTableModel();
        dtmTKDSMuonDoAn = new DefaultTableModel();
        dtmTKThanhLyDoAn = new DefaultTableModel();
        dtmTKMuonDoAnQuaHan = new DefaultTableModel();
        dtmTKMuonDoAnDungHan = new DefaultTableModel();

        jspTKDSCB = new JScrollPane();
        jspTKDSDoAn = new JScrollPane();
        jspTKDSMuonDoAn = new JScrollPane();
        jspTKThanhLyDoAn = new JScrollPane();
        jspTKMuonDoAnQuaHan = new JScrollPane();
        jspTKMuonDoAnDungHan = new JScrollPane();

        jlTKTitle.setFont(new Font("Arial", Font.PLAIN, 18));
        jpTKTitle.add(jlTKTitle);

        jpTKDSCB.setLayout(new BorderLayout());   
        jpTKDSCB1.setBackground(Color.LIGHT_GRAY);
        jpTKDSCB1.add(jcbTKDSCB2);
        jpTKDSCB1.add(jlTKDSCB);
        jpTKDSCB1.add(jcbTKDSCB1);
        jpTKDSCB1.add(jbTKDSCB);
        jpTKDSCB2.setLayout(null);
        //jpTKDSCB2.setBackground(Color.red);
        jspTKDSCB.setBounds(0, 0, 1350, 500);
        jpTKDSCB3.setBounds(0, 550, 1350, 50);
        jpTKDSCB2.setBackground(Color.LIGHT_GRAY);
        jpTKDSCB3.setBackground(Color.LIGHT_GRAY);
        jpTKDSCB2.add(jspTKDSCB);
        jpTKDSCB2.add(jpTKDSCB3);
        jpTKDSCB3.add(jtfTKDSCB3);
        jpTKDSCB3.add(jbTKDSCB3);
        jpTKDSCB.add(jpTKDSCB1, BorderLayout.PAGE_START);
        jpTKDSCB.add(jpTKDSCB2, BorderLayout.CENTER);
        //jpTKDSCB2.add(jpTKDSCB3, BorderLayout.PAGE_END);

        jpTKDSDoAn.setLayout(new BorderLayout());
        jpTKDSDoAn1.setBackground(Color.LIGHT_GRAY);
        jpTKDSDoAn1.add(jlTKDSDoAn);
        jpTKDSDoAn1.add(jcbTKDSDoAn);
        jpTKDSDoAn1.add(jbTKDSDoAn);
        jpTKDSDoAn2.setLayout(null);
        jpTKDSDoAn2.setBackground(Color.LIGHT_GRAY);
        jpTKDSDoAn3.setBackground(Color.LIGHT_GRAY);
        jspTKDSDoAn.setBounds(0, 0, 1350, 500);
        jpTKDSDoAn3.setBounds(0, 550, 1350, 50);
        //jpTKDSDoAn2.setBackground(Color.red);
        jpTKDSDoAn2.add(jspTKDSDoAn);
        jpTKDSDoAn2.add(jpTKDSDoAn3);
        jpTKDSDoAn3.add(jtfTKDSDoAn3);
        jpTKDSDoAn3.add(jbTKDSDoAn3);
        jpTKDSDoAn.add(jpTKDSDoAn1, BorderLayout.PAGE_START);
        jpTKDSDoAn.add(jpTKDSDoAn2, BorderLayout.CENTER);
        //jpTKDSDoAn.add(jpTKDSDoAn3, BorderLayout.PAGE_END);

        jpMainTimKiem.setLayout(new BorderLayout());
        jpMainTimKiem.add(jtb);
        jtb.setSize(1000, 500);
        jtb.addTab("DS Cán Bộ - Đảng Viên", jpTKDSCB);
        jtb.addTab("Danh Sách Đồ Án", jpTKDSDoAn);
        jtb.addTab("Danh Sách Mượn Đồ Án", jpTKDSMuonDoAn);
        jtb.addTab("DS Đồ Án Được Thanh Lý", jpTKThanhLyDoAn);
        jtb.addTab("DS Đồ Án Mượn Quá Hạn", jpTKMuonDoAnQuaHan);
        jtb.addTab("DS Đồ Án Mượn Đúng Hạn", jpTKMuonDoAnDungHan);

        
        jpTKThanhLyDoAn.setLayout(new BorderLayout());
        jpTKThanhLyDoAn.add(jpTKThanhLyDoAn1, BorderLayout.PAGE_START);
        jpTKThanhLyDoAn.add(jpTKThanhLyDoAn2, BorderLayout.CENTER);
        //jpTKThanhLyDoAn.add(jpTKThanhLyDoAn3, BorderLayout.PAGE_END);
        jpTKThanhLyDoAn1.add(jlTKThanhLyDA1);
        jpTKThanhLyDoAn1.add(jtfTKThanhLyDA1);
        jpTKThanhLyDoAn1.add(jlTKThanhLyDA2);
        jpTKThanhLyDoAn1.add(jtfTKThanhLyDA2);
        jpTKThanhLyDoAn1.add(jbTKThanhLyDA);
        jpTKThanhLyDoAn2.setLayout(null);
        jpTKThanhLyDoAn2.add(jspTKThanhLyDoAn);
        //jpTKThanhLyDoAn2.add(jpTKThanhLyDoAn3);
        jpTKThanhLyDoAn3.add(jtfTKThanhLyDA3);
        jpTKThanhLyDoAn3.add(jbTKThanhLyDA3);
        jspTKThanhLyDoAn.setBounds(0, 0, 1350, 500);
        jpTKThanhLyDoAn3.setBounds(0, 550, 1350, 50);
        jpTKThanhLyDoAn.setBackground(Color.LIGHT_GRAY);
        jpTKThanhLyDoAn1.setBackground(Color.LIGHT_GRAY);
        jpTKThanhLyDoAn2.setBackground(Color.LIGHT_GRAY);
        jpTKThanhLyDoAn3.setBackground(Color.LIGHT_GRAY);
        
        jpTKDSMuonDoAn.setLayout(new BorderLayout());
        jpTKDSMuonDoAn.add(jpTKDSMuonDoAn1, BorderLayout.PAGE_START);
        jpTKDSMuonDoAn.add(jpTKDSMuonDoAn2, BorderLayout.CENTER);
        //jpTKDSMuonDoAn.add(jpTKDSMuonDoAn3, BorderLayout.PAGE_END);
        jpTKDSMuonDoAn1.add(jlTKMuonDA1);
        jpTKDSMuonDoAn1.add(jtfTKMuonDA1);
        jpTKDSMuonDoAn1.add(jlTKMuonDA2);
        jpTKDSMuonDoAn1.add(jtfTKMuonDA2);
        jpTKDSMuonDoAn1.add(jbTKMuonDA);
        jpTKDSMuonDoAn2.setLayout(null);
        jpTKDSMuonDoAn2.add(jspTKDSMuonDoAn);
        jpTKDSMuonDoAn2.add(jpTKDSMuonDoAn3);
        jpTKDSMuonDoAn3.add(jtfTKMuonDA3);
        jpTKDSMuonDoAn3.add(jbTKMuonDA3);
        jspTKDSMuonDoAn.setBounds(0, 0, 1350, 500);
        jpTKDSMuonDoAn3.setBounds(0, 550, 1350, 50);
        jpTKDSMuonDoAn.setBackground(Color.LIGHT_GRAY);
        jpTKDSMuonDoAn1.setBackground(Color.LIGHT_GRAY);
        jpTKDSMuonDoAn2.setBackground(Color.LIGHT_GRAY);
        jpTKDSMuonDoAn3.setBackground(Color.LIGHT_GRAY);
        
        jpTKMuonDoAnQuaHan.setLayout(new BorderLayout());
        jpTKMuonDoAnQuaHan.add(jpTKMuonDoAnQuaHan1, BorderLayout.PAGE_START);
        jpTKMuonDoAnQuaHan.add(jpTKMuonDoAnQuaHan2, BorderLayout.CENTER);
        //jpTKMuonDoAnQuaHan.add(jpTKMuonDoAnQuaHan3, BorderLayout.PAGE_END);
        jpTKMuonDoAnQuaHan1.add(jlTKDAMuonQH1);
        jpTKMuonDoAnQuaHan1.add(jtfTKDAMuonQH1);
        jpTKMuonDoAnQuaHan1.add(jlTKDAMuonQH2);
        jpTKMuonDoAnQuaHan1.add(jtfTKDAMuonQH2);
        jpTKMuonDoAnQuaHan1.add(jbTKDAMuonQH);
        jpTKMuonDoAnQuaHan2.setLayout(null);
        jpTKMuonDoAnQuaHan2.add(jspTKMuonDoAnQuaHan);
        //jpTKMuonDoAnQuaHan2.add(jpTKMuonDoAnQuaHan3);
        jpTKMuonDoAnQuaHan3.add(jtfTKDAMuonQH3);
        jpTKMuonDoAnQuaHan3.add(jbTKDAMuonQH3);
        jspTKMuonDoAnQuaHan.setBounds(0, 0, 1350, 500);
        jpTKMuonDoAnQuaHan3.setBounds(0, 550, 1350, 50);
        jpTKMuonDoAnQuaHan.setBackground(Color.LIGHT_GRAY);
        jpTKMuonDoAnQuaHan1.setBackground(Color.LIGHT_GRAY);
        jpTKMuonDoAnQuaHan2.setBackground(Color.LIGHT_GRAY);
        jpTKMuonDoAnQuaHan3.setBackground(Color.LIGHT_GRAY);
        
        jpTKMuonDoAnDungHan.setLayout(new BorderLayout());
        jpTKMuonDoAnDungHan.add(jpTKMuonDoAnDungHan1, BorderLayout.PAGE_START);
        jpTKMuonDoAnDungHan.add(jpTKMuonDoAnDungHan2, BorderLayout.CENTER);
        //jpTKMuonDoAnDungHan.add(jpTKMuonDoAnDungHan3, BorderLayout.PAGE_END);
        jpTKMuonDoAnDungHan1.add(jlTKDAMuonDH1);
        jpTKMuonDoAnDungHan1.add(jtfTKDAMuonDH1);
        jpTKMuonDoAnDungHan1.add(jlTKDAMuonDH2);
        jpTKMuonDoAnDungHan1.add(jtfTKDAMuonDH2);
        jpTKMuonDoAnDungHan1.add(jbTKDAMuonDH);
        jspTKMuonDoAnDungHan.setBounds(0, 0, 1350, 500);
        jpTKMuonDoAnDungHan3.setBounds(0, 550, 1350, 50);
        jpTKMuonDoAnDungHan.setBackground(Color.LIGHT_GRAY); 
        jpTKMuonDoAnDungHan1.setBackground(Color.LIGHT_GRAY); 
        jpTKMuonDoAnDungHan2.setBackground(Color.LIGHT_GRAY); 
        jpTKMuonDoAnDungHan3.setBackground(Color.LIGHT_GRAY); 
        jpTKMuonDoAnDungHan2.setLayout(null);
        jpTKMuonDoAnDungHan2.add(jspTKMuonDoAnDungHan);
        //jpTKMuonDoAnDungHan2.add(jpTKMuonDoAnDungHan3);
        jpTKMuonDoAnDungHan3.add(jtfTKDAMuonDH3);
        jpTKMuonDoAnDungHan3.add(jbTKDAMuonDH3);

        this.setLayout(new BorderLayout());
        jpTKTitle.setSize(1000, 50);
        jpMainTimKiem.setSize(1000, 800);
        this.getContentPane().add(jpTKTitle, BorderLayout.PAGE_START);
        this.getContentPane().add(jpMainTimKiem, BorderLayout.CENTER);

        this.setSize(1400, 900);
        //this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        
        dcmTKDSCB2.addElement("DS Cán Bộ");
        dcmTKDSCB2.addElement("DS Đảng Viên");
        jcbTKDSCB2.setModel(dcmTKDSCB2);
        
        ld.loadCombobox(jcbTKDSCB1, "Select Bomon From GiangVien Group By Bomon", "Bomon");
        ld.loadCombobox(jcbTKDSDoAn, "Select Bomon From GiangVien Group By Bomon", "Bomon");

    }

    public static void main(String[] args) {
        new FrameTimKiem().setVisible(true);
    }

//    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == jbTKDSCB) {
            
            tk.timKiemDSCB(jtTKDSCB, dtmTKDSCB, jspTKDSCB, jcbTKDSCB1, jcbTKDSCB2);
            jtfTKDSCB3.setText("");
        
        }
        if (e.getSource() == jbTKDSDoAn) {
            
            tk.TKDSDA(jtTKDSDoAn, dtmTKDSDoAn, jspTKDSDoAn, jcbTKDSDoAn);
            jtfTKDSDoAn3.setText("");

        }
        BatLoi batLoi = new BatLoi();
        String ngayBD = "";
        String ngayKT = "";
        
        if(e.getSource() == jbTKMuonDA){
            jtfTKMuonDA3.setText("");
            ngayBD = jtfTKMuonDA1.getText();
            ngayKT = jtfTKMuonDA2.getText();
            String kt = batLoi.ktNhapBaoCao(ngayBD, ngayKT);
            if(kt.equals("Ban chua nhap du lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            }
            else if(kt.equals("Ban chua nhap ngay bat dau")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bắt Đầu");
            }
            else if(kt.equals("Ngay bat dau khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Bắt Đầu Không Đúng Định Dạng: YYYY-MM-dd");
            }
            else if(kt.equals("Ban chua nhap ngay ket thuc")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Kết Thúc");
            }
            else if(kt.equals("Ngay ket thuc khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Không Đúng Định Dạng: YYYY-MM-dd");
            }
            else{
                tk.TKDSMDA(jtTKDSMuonDoAn, dtmTKDSMuonDoAn, jspTKDSMuonDoAn, jtfTKMuonDA1, jtfTKMuonDA2);
            }
        }
        if(e.getSource() == jbTKThanhLyDA){
            ngayBD = jtfTKThanhLyDA2.getText();
            ngayKT = jtfTKThanhLyDA2.getText();
            String kt = batLoi.ktNhapBaoCao(ngayBD, ngayKT);
            if(kt.equals("Ban chua nhap du lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            }
            else if(kt.equals("Ban chua nhap ngay bat dau")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bắt Đầu");
            }
            else if(kt.equals("Ngay bat dau khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Bắt Đầu Không Đúng Định Dạng: YYYY-MM-dd");
            }
            else if(kt.equals("Ban chua nhap ngay ket thuc")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Kết Thúc");
            }
            else if(kt.equals("Ngay ket thuc khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Không Đúng Định Dạng: YYYY-MM-dd");
            }
            else{
                tk.TKDSDAThanhLy(jtTKThanhLyDoAn, dtmTKThanhLyDoAn, jspTKThanhLyDoAn, jtfTKThanhLyDA1, jtfTKThanhLyDA2);
            }
        }
        if(e.getSource() == jbTKDAMuonQH){
            ngayBD = jtfTKDAMuonQH1.getText();
            ngayKT = jtfTKDAMuonQH2.getText();
            String kt = batLoi.ktNhapBaoCao(ngayBD, ngayKT);
            if(kt.equals("Ban chua nhap du lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            }
            else if(kt.equals("Ban chua nhap ngay bat dau")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bắt Đầu");
            }
            else if(kt.equals("Ngay bat dau khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Bắt Đầu Không Đúng Định Dạng: YYYY-MM-dd");
            }
            else if(kt.equals("Ban chua nhap ngay ket thuc")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Kết Thúc");
            }
            else if(kt.equals("Ngay ket thuc khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Không Đúng Định Dạng: YYYY-MM-dd");
            }
            else{
                tk.TKDSMDAQuaHan(jtTKMuonDoAnQuaHan, dtmTKMuonDoAnQuaHan, jspTKMuonDoAnQuaHan, jtfTKDAMuonQH1, jtfTKDAMuonQH2);
            }
        }
        if(e.getSource() == jbTKDAMuonDH){
            ngayBD = jtfTKDAMuonDH1.getText();
            ngayKT = jtfTKDAMuonDH2.getText();
            String kt = batLoi.ktNhapBaoCao(ngayBD, ngayKT);
            if(kt.equals("Ban chua nhap du lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
            }
            else if(kt.equals("Ban chua nhap ngay bat dau")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bắt Đầu");
            }
            else if(kt.equals("Ngay bat dau khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Bắt Đầu Không Đúng Định Dạng");
            }
            else if(kt.equals("Ban chua nhap ngay ket thuc")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Kết Thúc");
            }
            else if(kt.equals("Ngay ket thuc khong dung dinh dang")){
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Không Đúng Định Dạng");
            }
            else{
                tk.TKDSMDADungHan(jtTKMuonDoAnDungHan, dtmTKMuonDoAnDungHan, jspTKMuonDoAnDungHan, jtfTKDAMuonDH1, jtfTKDAMuonDH2);
            }
        }
        String sql = "";
        if(e.getSource() == jbTKDSCB3){
            sql = "Select * From GiangVien Where HotenGV = ? OR MaGV = ? OR Chucdanh = ? OR Gioitinh = ? OR Chucvu = ?";
            tk.TK(jtTKDSCB, dtmTKDSCB, jspTKDSCB, jtfTKDSCB3, sql);
            jcbTKDSCB1.setSelectedItem(null);
            jcbTKDSCB2.setSelectedItem(null);
        }
        if(e.getSource() == jbTKDSDoAn3){
            sql = "Select * From DoAn Where Madoan = ? OR Masv = ? OR TenSV = ? OR TenGVHD = ? OR TenGVPB = ?";
            tk.TK(jtTKDSDoAn, dtmTKDSDoAn, jspTKDSDoAn, jtfTKDSDoAn3, sql);
            jcbTKDSDoAn.setSelectedItem(null);
        }
        if(e.getSource() == jbTKMuonDA3){
            sql = "Select * From MuonDoAn Where [Muon-doan] = ? OR MaDoAn = ? OR Tennguoimuon = ? OR Dienthoai = ? OR Email = ?";
            tk.TK(jtTKDSMuonDoAn, dtmTKDSMuonDoAn, jspTKDSMuonDoAn, jtfTKMuonDA3, sql);
            jtfTKMuonDA1.setText("");
            jtfTKMuonDA2.setText("");
        }
//        if(e.getSource() == jbTKThanhLyDA3){
//            sql = "Select * From DoAn Where Madoan = ? OR Masv = ? OR TenSV = ? OR TenGVHD = ? OR TenGVPB = ?";
//            tk.TK(jtTKDSDoAn, dtmTKDSDoAn, jspTKDSDoAn, jtfTKDSDoAn3, sql);
//        }
//        if(e.getSource() == jbTKDAMuonQH3){
//            sql = "Select * From MuonDoAn Where Muon-doan = ? OR MaDoAn = ? OR Tennguoimuon = ? OR Ngaymuon = ? OR Ngaytra = ?";
//            tk.TK(jtTKDSMuonDoAn, dtmTKDSMuonDoAn, jspTKDSMuonDoAn, jtfTKMuonDA3, sql);
//        }
//        if(e.getSource() == jbTKDAMuonDH3){
//            sql = "Select * From MuonDoAn Where [Muon-doan] = ? OR MaDoAn = ? OR Tennguoimuon = ? OR Ngaymuon = ? OR Ngaytra = ?";
//            tk.TK(jtTKDSMuonDoAn, dtmTKDSMuonDoAn, jspTKDSMuonDoAn, jtfTKMuonDA3, sql);
//        }

    }

}
