package View;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.SwingConstants;

public class MainFrame extends JFrame {

    private JPanel contentPane;

    private JPanel pnWellcome;
    JLabel jlPicture;
    public JMenu mnNguoiDung, mnCNDL, mnTimKiem, mnBaoCao, mnThongKe, mnTroGiup;

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainFrame frame = new MainFrame();
                    

                    frame.setVisible(true);
                    frame.setLocationRelativeTo(null);
                    //ChucNangCapNhat update = new ChucNangCapNhat();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public MainFrame() {
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, 1370, 800);
        this.setLocationRelativeTo(null);

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel pnParent = new JPanel();
        pnParent.setBackground(Color.WHITE);
        pnParent.setBounds(0, 0, 1400, 750);
        contentPane.add(pnParent);
        pnParent.setLayout(new CardLayout(0, 0));

        pnWellcome = new JPanel();

        pnWellcome.setLayout(null);

        ImageIcon image1 = new ImageIcon(new ImageIcon("b.jpg").getImage().getScaledInstance(1400, 750, Image.SCALE_REPLICATE));

        JLabel lblWellcome = new JLabel("");
        lblWellcome.setBounds(0, 45, 1400, 750);
        lblWellcome.setIcon(image1);

        pnWellcome.add(lblWellcome);
        pnParent.add(pnWellcome, "name_954165384863852");

        JPanel pnTieuDe = new JPanel();
        pnTieuDe.setBounds(0, 0, 1400, 48);
        pnTieuDe.setBackground(new Color(218, 223, 225));
        pnWellcome.add(pnTieuDe);
        pnTieuDe.setLayout(null);

        JLabel lblTieuDe = new JLabel("QUẢN LÝ QUYỂN ĐỒ ÁN TỐT NGHIỆP ĐẠI HỌC");
        lblTieuDe.setHorizontalAlignment(SwingConstants.CENTER);
        lblTieuDe.setFont(new Font("Arial", Font.PLAIN, 18));
        lblTieuDe.setBounds(52, 11, 1400, 27);
        lblTieuDe.setBackground(Color.LIGHT_GRAY);
        lblTieuDe.setForeground(Color.red);
        pnTieuDe.add(lblTieuDe);

        //tạo menu
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        mnNguoiDung = new JMenu("Người dùng");
        menuBar.add(mnNguoiDung);

        JMenuItem mnItemTaiKhoan = new JMenuItem("Quản lý tài khoản");
        
        mnItemTaiKhoan.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                FrameNguoiDung frame = new FrameNguoiDung();

                pnParent.removeAll();
                pnParent.add(frame.getRootPane());
                pnParent.repaint();
                pnParent.revalidate();

            }
        });
        mnNguoiDung.add(mnItemTaiKhoan);

        mnCNDL = new JMenu("Cập nhật dữ liệu");
        menuBar.add(mnCNDL);

        JMenuItem mnItemCapNhatDL = new JMenuItem("Cập nhật DL");
        mnItemCapNhatDL.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                FrameCapNhat frame = new FrameCapNhat();

                pnParent.removeAll();
                pnParent.add(frame.getRootPane());
                pnParent.repaint();
                pnParent.revalidate();

            }
        });
        mnCNDL.add(mnItemCapNhatDL);

        mnTimKiem = new JMenu("Tìm kiếm");
        menuBar.add(mnTimKiem);

        JMenuItem mnItemTimKiem = new JMenuItem("Tìm kiếm");
        mnItemTimKiem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                FrameTimKiem frame = new FrameTimKiem();

                pnParent.removeAll();
                pnParent.add(frame.getRootPane());
                pnParent.repaint();
                pnParent.revalidate();

            }
        });
        mnTimKiem.add(mnItemTimKiem);

        mnBaoCao = new JMenu("Báo cáo");
        menuBar.add(mnBaoCao);

        JMenuItem mnItemBaoCao = new JMenuItem("Báo cáo");
        mnItemBaoCao.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                FrameBaoCao frameBC = new FrameBaoCao();

                pnParent.removeAll();
                pnParent.add(frameBC.getRootPane());
                pnParent.repaint();
                pnParent.revalidate();
            }
        });
        mnBaoCao.add(mnItemBaoCao);

        mnThongKe = new JMenu("Thống kê");
        menuBar.add(mnThongKe);
        JMenuItem jmiThongKe = new JMenuItem("Thống Kê");
        jmiThongKe.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                FrameThongKe frameTK = new FrameThongKe();

                pnParent.removeAll();
                pnParent.add(frameTK.getRootPane());
                pnParent.repaint();
                pnParent.revalidate();
            }
        });
        mnThongKe.add(jmiThongKe);

        mnTroGiup = new JMenu("Trợ giúp");
        menuBar.add(mnTroGiup);

        JMenuItem mnItemGDChinh = new JMenuItem("Giao diện chính");
        mnItemGDChinh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                pnParent.removeAll();
                pnParent.add(pnWellcome);
                pnParent.repaint();
                pnParent.revalidate();
            }

        });
        mnTroGiup.add(mnItemGDChinh);

        JMenuItem mnItemThoatDN = new JMenuItem("Thoát đăng nhập");
        mnItemThoatDN.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                closeFrame();
                FrameLogin lg = new FrameLogin();
                lg.setTitle("Đăng nhập hệ thống");
                lg.setVisible(true);

            }
        });
        mnTroGiup.add(mnItemThoatDN);

    }

    public void closeFrame() {
        super.dispose();
    }
}
