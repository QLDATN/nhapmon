package View;

import Control.BatLoi;
import Control.ChucNangCapNhat;
import Control.DBConnection;
import Model.Info_DA;
import Model.Info_GV;
import Model.Info_MDA;
import Model.LoadData;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class FrameCapNhat extends JFrame implements ActionListener {

    public JPanel contentPane;
    public JTextField tfMaGV;
    public JTextField tfHoTen;
    public JTextField tfNgaySinh;
    public JTextField tfDienThoai;
    public JTextField tfEmail;
    public JTextField tfDienChi;
    public JTextField tfBoMon;
    public JTextField tfChiBo;
    public JTextField tfMaDA;
    public JTextField tfTenDeTai;
    public JTextField tfMaSV;
    public JTextField tfMaGVHD;
    public JTextField tfMaGVPB;
    public JTextField tfNgayBaoVe;
    public JTextField tfNgayThanhLy;
    public JTextField tfGhiChuDA;
    public JTextField tfMaDA2;
    public JTextField tfTenNguoiMuon;
    public JTextField tfDienThoai2;
    public JTextField tfEmail2;
    public JTextField tfNgayMuon;
    public JTextField tfNgayTra;
    public JTextField tfNgayTraDK;
    public JTextField tfBaoLanh;
    public JTextField tfGhiChu2;
    public JTextField tfMuonDa;
    public JComboBox cbboxChucDanh, cbboxChucVu, cbboxGhiChu, cbboxKyHoc, cbboxNamHoc;
    public JButton btnThemGV, btnSuaGV, btnXoaGV, btnRefreshGV, btnThemDA, btnSuaDA,
            btnXoaDA, btnRefreshDA, btnThemMDA, btnSuaMDA, btnXoaMDA, btnRefreshMDA;

    public JTable jtUpdateGV, jtUpdateDoAn, jtUpdateMuonDA;
    public DefaultTableModel dtmUpdateGV, dtmUpdateDoAn, dtmUpdateMuonDA;
    JButton jb3 = null;
    public JScrollPane jspUpdateGV, jspUpdateDoAn, jspUpdateMuonDA;
    JPanel jp = new JPanel();
    public JLabel jlSoCMND, jlMaSoThue, jlSoTK, jlGioiTinh, jlNgayVeTruong, jlNgayVaoDang,
            jlTenDoAnMuon, jlTenTacGia, jlTinhTrang, jlTenSV, jlDienThoaiSV, jlEmailSV, jlTenLopSV, jlKhoaSV;
    public JTextField jtfSoCMND, jtfMaSoThue, jtfSoTK, jtfNgayVeTruong, jtfNgayVaoDang, tfTenGVHD, tfTenGVPB,
            jtfTenDoAnMuon, jtfTenTacGia, jtfTinhTrang, jtfTenSV, jtfDienThoaiSV, jtfEmailSV, jtfTenLopSV, jtfKhoaSV;
    public JRadioButton jrbGTNam, jrbGTNu;
    public ButtonGroup bg;

    ChucNangCapNhat update = new ChucNangCapNhat();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    FrameCapNhat frame = new FrameCapNhat();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    LoadData ld = new LoadData();
    String sqlGV = "Select * from GiangVien";
    String sqlDA = "Select * from DoAn";
    String sqlMDA = "Select * from MuonDoAn";

    public FrameCapNhat() {

        jtUpdateGV = new JTable();
        dtmUpdateGV = new DefaultTableModel();
        jspUpdateGV = new JScrollPane();
        jtUpdateDoAn = new JTable();
        dtmUpdateDoAn = new DefaultTableModel();
        jspUpdateDoAn = new JScrollPane();
        jtUpdateMuonDA = new JTable();
        dtmUpdateMuonDA = new DefaultTableModel();
        jspUpdateMuonDA = new JScrollPane();
        bg = new ButtonGroup();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, 1370, 800);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(0, 38, 1350, 800);//573
        contentPane.add(tabbedPane);

        //tab giang vien
        JPanel pnGV = new JPanel();
        tabbedPane.addTab("Giảng viên", null, pnGV, null);
        pnGV.setLayout(null);
        pnGV.setBackground(Color.LIGHT_GRAY);

        JLabel lblMaGV = new JLabel("Mã GV");
        //lblMaGV.setFont(new Font("Tahoma", Font.PLAIN, 11));
        lblMaGV.setBounds(31, 352, 72, 20);
        pnGV.add(lblMaGV);

        JLabel lblHoTen = new JLabel("Họ Tên");
        lblHoTen.setBounds(31, 399, 72, 20);
        pnGV.add(lblHoTen);

        jlGioiTinh = new JLabel("Giới Tính");
        jlGioiTinh.setBounds(31, 446, 72, 20);
        pnGV.add(jlGioiTinh);

        JLabel lblNgaySinh = new JLabel("Ngày Sinh");
        lblNgaySinh.setBounds(31, 493, 72, 20);
        pnGV.add(lblNgaySinh);

        jlNgayVeTruong = new JLabel("Ngày Về Trường");
        jlNgayVeTruong.setBounds(360, 352, 150, 20);
        pnGV.add(jlNgayVeTruong);

        jlNgayVaoDang = new JLabel("Ngày Vào Đảng");
        jlNgayVaoDang.setBounds(360, 399, 150, 20);
        pnGV.add(jlNgayVaoDang);

        JLabel lblChucDanh = new JLabel("Chức Danh");
        lblChucDanh.setBounds(360, 446, 72, 20);
        pnGV.add(lblChucDanh);

        JLabel lblChucVu = new JLabel("Chức Vụ");
        lblChucVu.setBounds(360, 493, 72, 20);
        pnGV.add(lblChucVu);

        JLabel lblDienThoai = new JLabel("Điện Thoại");
        lblDienThoai.setBounds(680, 352, 72, 20);
        pnGV.add(lblDienThoai);

        JLabel lblEmail = new JLabel("Email");
        lblEmail.setBounds(680, 399, 72, 20);
        pnGV.add(lblEmail);

        JLabel lblDiaChi = new JLabel("Địa Chỉ");
        lblDiaChi.setBounds(680, 446, 72, 20);
        pnGV.add(lblDiaChi);

        JLabel lblChiBo = new JLabel("Chi Bộ");
        lblChiBo.setBounds(1050, 352, 72, 20);
        pnGV.add(lblChiBo);

        jlSoCMND = new JLabel("Số CMND");
        jlSoCMND.setBounds(1050, 399, 72, 20);
        pnGV.add(jlSoCMND);

        JLabel lblBoMon = new JLabel("Bộ Môn");
        lblBoMon.setBounds(680, 493, 72, 20);
        pnGV.add(lblBoMon);

        jlMaSoThue = new JLabel("Mã Số Thuế");
        jlMaSoThue.setBounds(1050, 446, 72, 20);
        pnGV.add(jlMaSoThue);

        jlSoTK = new JLabel("Số Tài Khoản");
        jlSoTK.setBounds(1050, 493, 90, 20);
        pnGV.add(jlSoTK);

        JLabel lblGhiChu = new JLabel("Ghi Chú");
        lblGhiChu.setBounds(1050, 540, 72, 20);
        pnGV.add(lblGhiChu);

        tfMaGV = new JTextField();
        tfMaGV.setBounds(119, 352, 165, 20);
        pnGV.add(tfMaGV);
        tfMaGV.setColumns(10);

        tfHoTen = new JTextField();
        tfHoTen.setBounds(119, 399, 165, 20);
        pnGV.add(tfHoTen);
        tfHoTen.setColumns(10);

        tfNgaySinh = new JTextField();
        tfNgaySinh.setBounds(119, 493, 165, 20);
        pnGV.add(tfNgaySinh);
        tfNgaySinh.setColumns(10);

        jrbGTNam = new JRadioButton("Nam");
        jrbGTNam.setBounds(119, 446, 80, 20);
        pnGV.add(jrbGTNam);
        jrbGTNu = new JRadioButton("Nữ");
        jrbGTNu.setBounds(200, 446, 80, 20);
        pnGV.add(jrbGTNu);
        // Chi cho phep chon 1 trong 2 lua chon.
        bg.add(jrbGTNam);
        bg.add(jrbGTNu);
        jrbGTNam.setSelected(true);

        jtfNgayVeTruong = new JTextField();
        jtfNgayVeTruong.setBounds(460, 352, 165, 20);
        pnGV.add(jtfNgayVeTruong);
        jtfNgayVeTruong.setColumns(10);

        jtfNgayVaoDang = new JTextField();
        jtfNgayVaoDang.setBounds(460, 399, 165, 20);
        pnGV.add(jtfNgayVaoDang);
        jtfNgayVaoDang.setColumns(10);

        String[] chucdanh = {"ThS", "GS", "PGS.TS", "TS", "ThS", "KS"};
        cbboxChucDanh = new JComboBox(chucdanh);
        cbboxChucDanh.setBounds(460, 446, 165, 20);
        pnGV.add(cbboxChucDanh);

        String[] chucvu = {"GV", "GVC", "VT", "VP", "TBM", "PBM"};
        cbboxChucVu = new JComboBox(chucvu);
        cbboxChucVu.setBounds(460, 493, 165, 20);
        pnGV.add(cbboxChucVu);

        String[] ghichu = {"DANG CONG TAC", "NGHI HUU", "NCS NN"};
        cbboxGhiChu = new JComboBox(ghichu);

        cbboxGhiChu.setBounds(1150, 540, 165, 20);
        pnGV.add(cbboxGhiChu);

        tfDienThoai = new JTextField();
        tfDienThoai.setBounds(800, 352, 165, 20);
        pnGV.add(tfDienThoai);
        tfDienThoai.setColumns(10);

        tfEmail = new JTextField();
        tfEmail.setBounds(800, 399, 165, 20);
        pnGV.add(tfEmail);
        tfEmail.setColumns(10);

        tfDienChi = new JTextField();
        tfDienChi.setBounds(800, 446, 165, 20);
        pnGV.add(tfDienChi);
        tfDienChi.setColumns(10);

        tfBoMon = new JTextField();
        tfBoMon.setBounds(800, 493, 165, 20);
        pnGV.add(tfBoMon);
        tfBoMon.setColumns(10);

        tfChiBo = new JTextField();
        tfChiBo.setBounds(1150, 352, 165, 20);
        pnGV.add(tfChiBo);
        tfChiBo.setColumns(10);

        jtfSoCMND = new JTextField();
        jtfSoCMND.setBounds(1150, 399, 165, 20);
        pnGV.add(jtfSoCMND);
        jtfSoCMND.setColumns(10);

        jtfMaSoThue = new JTextField();
        jtfMaSoThue.setBounds(1150, 446, 165, 20);
        pnGV.add(jtfMaSoThue);
        jtfMaSoThue.setColumns(10);

        jtfSoTK = new JTextField();
        jtfSoTK.setBounds(1150, 493, 165, 20);
        pnGV.add(jtfSoTK);
        jtfSoTK.setColumns(10);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBackground(Color.CYAN);
        panel.setBounds(0, 23, 1350, 300);
        panel.add(jspUpdateGV);
        pnGV.add(panel);

//      Nap du lieu cho bang Giang Vien
        LoadData ld = new LoadData();
        ld.loadDataTable(jtUpdateGV, dtmUpdateGV, jspUpdateGV, null, null, null, null, "Select * from GiangVien");

        btnThemGV = new JButton("Thêm");
        btnThemGV.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnThemGV.setBounds(320, 550, 130, 55);

        Icon icon1 = new ImageIcon("icon-add3.png");
        btnThemGV.setIcon(icon1);
        pnGV.add(btnThemGV);
        

        btnSuaGV = new JButton("Sửa");
        btnSuaGV.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnSuaGV.setBounds(505, 550, 130, 55);

        Icon icon2 = new ImageIcon("icon-repair2.png");
        btnSuaGV.setIcon(icon2);
        pnGV.add(btnSuaGV);
        

        btnXoaGV = new JButton("Xóa");
        btnXoaGV.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnXoaGV.setBounds(690, 550, 130, 55);

        Icon icon3 = new ImageIcon("icon-delete.png");
        btnXoaGV.setIcon(icon3);
        pnGV.add(btnXoaGV);
        

        btnRefreshGV = new JButton("Refresh");
        btnRefreshGV.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnRefreshGV.setBounds(874, 550, 130, 55);

        Icon icon4 = new ImageIcon("icon-refresh.png");
        btnRefreshGV.setIcon(icon4);
        pnGV.add(btnRefreshGV);
        
        btnThemGV.addActionListener(this);
        btnSuaGV.addActionListener(this);
        btnXoaGV.addActionListener(this);
        btnRefreshGV.addActionListener(this);

//        tab do an
        String[] kihoc = {"1", "2", "3"};

        String[] namhoc = {"2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010",
            "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000"};

        JPanel pnDoAn = new JPanel();
        tabbedPane.addTab("Đồ án", null, pnDoAn, null);
        pnDoAn.setLayout(null);
        pnDoAn.setBackground(Color.LIGHT_GRAY);

        JLabel lblTenDeTai = new JLabel("Tên đề tài");
        lblTenDeTai.setBounds(31, 399, 72, 20);
        pnDoAn.add(lblTenDeTai);

        JLabel lblMaSV = new JLabel("Mã SV");
        lblMaSV.setBounds(31, 446, 72, 20);
        pnDoAn.add(lblMaSV);

        jlTenSV = new JLabel("Tên SV");
        jlTenSV.setBounds(31, 493, 72, 20);
        pnDoAn.add(jlTenSV);

        jlDienThoaiSV = new JLabel("Điện Thoại");
        jlDienThoaiSV.setBounds(360, 352, 72, 20);
        pnDoAn.add(jlDienThoaiSV);

        jlEmailSV = new JLabel("Email");
        jlEmailSV.setBounds(360, 399, 72, 20);
        pnDoAn.add(jlEmailSV);

        jlTenLopSV = new JLabel("Lớp");
        jlTenLopSV.setBounds(360, 446, 72, 20);
        pnDoAn.add(jlTenLopSV);

        jlKhoaSV = new JLabel("Khoa");
        jlKhoaSV.setBounds(360, 493, 72, 20);
        pnDoAn.add(jlKhoaSV);

        JLabel lblKyHoc = new JLabel("Kỳ học");
        lblKyHoc.setBounds(680, 352, 72, 20);
        pnDoAn.add(lblKyHoc);

        JLabel lblNamHoc = new JLabel("Năm học");
        lblNamHoc.setBounds(680, 399, 72, 20);
        pnDoAn.add(lblNamHoc);

        JLabel lblMaGVHD = new JLabel("Mã GVHD");
        lblMaGVHD.setBounds(680, 446, 89, 20);
        pnDoAn.add(lblMaGVHD);

        JLabel lblTenGVHD = new JLabel("Tên GVHD");
        lblTenGVHD.setBounds(680, 493, 89, 20);
        pnDoAn.add(lblTenGVHD);

        JLabel lblMaGVPB = new JLabel("Mã GVPB");
        lblMaGVPB.setBounds(1050, 352, 89, 20);
        pnDoAn.add(lblMaGVPB);

        JLabel lblTenGVPB = new JLabel("Tên GVPB");
        lblTenGVPB.setBounds(1050, 399, 89, 20);
        pnDoAn.add(lblTenGVPB);

        JLabel lblNgayBaoVe = new JLabel("Ngày bảo vệ");
        lblNgayBaoVe.setBounds(1050, 446, 89, 20);
        pnDoAn.add(lblNgayBaoVe);

        JLabel lblNgayThanhLy = new JLabel("Ngày thanh lý");
        lblNgayThanhLy.setBounds(1050, 493, 89, 20);
        pnDoAn.add(lblNgayThanhLy);

        JLabel lblGhiChuDA = new JLabel("Ghi chú");
        lblGhiChuDA.setBounds(1050, 540, 89, 20);
        pnDoAn.add(lblGhiChuDA);

        JLabel lblMaDA = new JLabel("Mã ĐA");
        lblMaDA.setBounds(31, 352, 72, 20);
        pnDoAn.add(lblMaDA);

        tfMaDA = new JTextField();
        tfMaDA.setBounds(119, 352, 165, 20);
        pnDoAn.add(tfMaDA);
        tfMaDA.setColumns(10);

        tfTenDeTai = new JTextField();
        tfTenDeTai.setBounds(119, 399, 165, 20);
        pnDoAn.add(tfTenDeTai);
        tfTenDeTai.setColumns(10);

        tfMaSV = new JTextField();
        tfMaSV.setBounds(119, 446, 165, 20);
        pnDoAn.add(tfMaSV);
        tfMaSV.setColumns(10);

        jtfTenSV = new JTextField();
        jtfTenSV.setBounds(119, 493, 165, 20);
        pnDoAn.add(jtfTenSV);
        jtfTenSV.setColumns(10);

        jtfDienThoaiSV = new JTextField();
        jtfDienThoaiSV.setBounds(460, 352, 165, 20);
        pnDoAn.add(jtfDienThoaiSV);
        jtfDienThoaiSV.setColumns(10);

        jtfEmailSV = new JTextField();
        jtfEmailSV.setBounds(460, 399, 165, 20);
        pnDoAn.add(jtfEmailSV);
        jtfEmailSV.setColumns(10);

        jtfTenLopSV = new JTextField();
        jtfTenLopSV.setBounds(460, 446, 165, 20);
        pnDoAn.add(jtfTenLopSV);
        jtfTenLopSV.setColumns(10);

        jtfKhoaSV = new JTextField();
        jtfKhoaSV.setBounds(460, 493, 165, 20);
        pnDoAn.add(jtfKhoaSV);
        jtfKhoaSV.setColumns(10);

        cbboxKyHoc = new JComboBox(kihoc);
        cbboxKyHoc.setBounds(800, 352, 165, 20);
        pnDoAn.add(cbboxKyHoc);

        cbboxNamHoc = new JComboBox(namhoc);
        cbboxNamHoc.setBounds(800, 399, 165, 20);
        pnDoAn.add(cbboxNamHoc);

        tfMaGVHD = new JTextField();
        tfMaGVHD.setBounds(800, 446, 165, 20);
        pnDoAn.add(tfMaGVHD);
        tfMaGVHD.setColumns(10);

        tfTenGVHD = new JTextField();
        tfTenGVHD.setBounds(800, 493, 165, 20);
        pnDoAn.add(tfTenGVHD);
        tfTenGVHD.setColumns(10);

        tfMaGVPB = new JTextField();
        tfMaGVPB.setBounds(1150, 352, 165, 20);
        pnDoAn.add(tfMaGVPB);
        tfMaGVPB.setColumns(10);

        tfTenGVPB = new JTextField();
        tfTenGVPB.setBounds(1150, 399, 165, 20);
        pnDoAn.add(tfTenGVPB);
        tfTenGVPB.setColumns(10);

        tfNgayBaoVe = new JTextField();
        tfNgayBaoVe.setBounds(1150, 446, 165, 20);
        pnDoAn.add(tfNgayBaoVe);
        tfNgayBaoVe.setColumns(10);

        tfNgayThanhLy = new JTextField();
        tfNgayThanhLy.setBounds(1150, 493, 165, 20);
        pnDoAn.add(tfNgayThanhLy);
        tfNgayThanhLy.setColumns(10);

        tfGhiChuDA = new JTextField();
        tfGhiChuDA.setBounds(1150, 540, 165, 20);
        pnDoAn.add(tfGhiChuDA);
        tfGhiChuDA.setColumns(10);

        JPanel panel_1 = new JPanel();
        panel_1.setBackground(Color.CYAN);
        panel_1.setBounds(0, 23, 1350, 300);
        pnDoAn.add(panel_1);

//        Nap Du Lieu Cho Bang Do An
        panel_1.setLayout(new BorderLayout());
        panel_1.add(jspUpdateDoAn);
        ld.loadDataTable(jtUpdateDoAn, dtmUpdateDoAn, jspUpdateDoAn, null, null, null, null, sqlDA);

        btnThemDA = new JButton("Thêm");
        btnThemDA.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnThemDA.setBounds(320, 550, 130, 55);

        Icon icon1a = new ImageIcon("icon-add3.png");
        btnThemDA.setIcon(icon1a);
        pnDoAn.add(btnThemDA);

        btnSuaDA = new JButton("Sửa");
        btnSuaDA.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnSuaDA.setBounds(505, 550, 130, 55);
        Icon icon2a = new ImageIcon("icon-repair2.png");
        btnSuaDA.setIcon(icon2a);
        pnDoAn.add(btnSuaDA);

        btnXoaDA = new JButton("Xóa");
        btnXoaDA.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnXoaDA.setBounds(690, 550, 130, 55);
        Icon icon3a = new ImageIcon("icon-delete.png");
        btnXoaDA.setIcon(icon3a);
        pnDoAn.add(btnXoaDA);

        btnRefreshDA = new JButton("Refresh");
        btnRefreshDA.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnRefreshDA.setBounds(875, 550, 130, 55);
        Icon icon4a = new ImageIcon("icon-refresh.png");
        btnRefreshDA.setIcon(icon4a);
        pnDoAn.add(btnRefreshDA);

//        Thêm SK vào nút bấm trong tab Do An
        btnThemDA.addActionListener(this);
        btnSuaDA.addActionListener(this);
        btnXoaDA.addActionListener(this);
        btnRefreshDA.addActionListener(this);

//        tab muon do an
        JPanel pnMuonDA2 = new JPanel();
        tabbedPane.addTab("Mượn đồ án", null, pnMuonDA2, null);
        pnMuonDA2.setLayout(null);
        pnMuonDA2.setBackground(Color.LIGHT_GRAY);

        JLabel lblMuonDA = new JLabel("Mượn đồ án");
        lblMuonDA.setBounds(31, 32, 89, 14);
        pnMuonDA2.add(lblMuonDA);

        JLabel lblMaDA2 = new JLabel("Mã ĐA");
        lblMaDA2.setBounds(31, 77, 89, 20);
        pnMuonDA2.add(lblMaDA2);

        JLabel lblTenNguoiMuon = new JLabel("Tên người mượn");
        lblTenNguoiMuon.setBounds(31, 122, 105, 20);
        pnMuonDA2.add(lblTenNguoiMuon);

        JLabel lblDienThoai2 = new JLabel("Điện thoại");
        lblDienThoai2.setBounds(31, 167, 89, 20);
        pnMuonDA2.add(lblDienThoai2);

        JLabel lblNewLabel_5 = new JLabel("Email");
        lblNewLabel_5.setBounds(31, 212, 89, 20);
        pnMuonDA2.add(lblNewLabel_5);

        JLabel lblEmail2 = new JLabel("Ngày mượn");
        lblEmail2.setBounds(31, 257, 89, 20);
        pnMuonDA2.add(lblEmail2);

        JLabel lblNgayTra = new JLabel("Ngày trả");
        lblNgayTra.setBounds(31, 302, 89, 20);
        pnMuonDA2.add(lblNgayTra);

        JLabel lblNgayTraDK = new JLabel("Ngày trả dự kiến");
        lblNgayTraDK.setBounds(31, 347, 105, 20);
        pnMuonDA2.add(lblNgayTraDK);
        
        JLabel lblTinhTrang = new JLabel("Tình Trạng");
        lblTinhTrang.setBounds(31, 392, 105, 20);
        pnMuonDA2.add(lblTinhTrang);

        JLabel lblBaoLanh = new JLabel("Bảo lãnh");
        lblBaoLanh.setBounds(31, 437, 89, 20);
        pnMuonDA2.add(lblBaoLanh);

        JLabel lblGhiChu2 = new JLabel("Ghi chú");
        lblGhiChu2.setBounds(31, 482, 89, 20);
        pnMuonDA2.add(lblGhiChu2);

        tfMaDA2 = new JTextField();
        tfMaDA2.setBounds(146, 77, 165, 20);
        pnMuonDA2.add(tfMaDA2);
        tfMaDA2.setColumns(10);

        tfTenNguoiMuon = new JTextField();
        tfTenNguoiMuon.setBounds(146, 122, 165, 20);
        pnMuonDA2.add(tfTenNguoiMuon);
        tfTenNguoiMuon.setColumns(10);

        tfDienThoai2 = new JTextField();
        tfDienThoai2.setBounds(146, 167, 165, 20);
        pnMuonDA2.add(tfDienThoai2);
        tfDienThoai2.setColumns(10);

        tfEmail2 = new JTextField();
        tfEmail2.setBounds(146, 212, 165, 20);
        pnMuonDA2.add(tfEmail2);
        tfEmail2.setColumns(10);

        tfNgayMuon = new JTextField();
        tfNgayMuon.setBounds(146, 257, 165, 20);
        pnMuonDA2.add(tfNgayMuon);
        tfNgayMuon.setColumns(10);

        tfNgayTra = new JTextField();
        tfNgayTra.setBounds(146, 302, 165, 20);
        pnMuonDA2.add(tfNgayTra);
        tfNgayTra.setColumns(10);

        tfNgayTraDK = new JTextField();
        tfNgayTraDK.setBounds(146, 347, 165, 20);
        pnMuonDA2.add(tfNgayTraDK);
        tfNgayTraDK.setColumns(10);
        
        jtfTinhTrang = new JTextField();
        jtfTinhTrang.setBounds(146, 392, 165, 20);
        pnMuonDA2.add(jtfTinhTrang);
        jtfTinhTrang.setColumns(10);

        tfBaoLanh = new JTextField();
        tfBaoLanh.setBounds(146, 437, 165, 20);
        pnMuonDA2.add(tfBaoLanh);
        tfBaoLanh.setColumns(10);

        tfGhiChu2 = new JTextField();
        tfGhiChu2.setBounds(146, 482, 165, 20);
        pnMuonDA2.add(tfGhiChu2);
        tfGhiChu2.setColumns(10);

        tfMuonDa = new JTextField();
        tfMuonDa.setBounds(146, 32, 165, 20);
        pnMuonDA2.add(tfMuonDa);
        tfMuonDa.setColumns(10);

        JPanel panel_2 = new JPanel();
        panel_2.setBackground(Color.CYAN);
        panel_2.setBounds(349, 32, 960, 399);
        pnMuonDA2.add(panel_2);

//        Nap du lieu cho bang Muon Do An
        panel_2.setLayout(new BorderLayout());
        panel_2.add(jspUpdateMuonDA);
        ld.loadDataTable(jtUpdateMuonDA, dtmUpdateMuonDA, jspUpdateMuonDA, null, null, null, null, sqlMDA);

        btnThemMDA = new JButton("Thêm");
        btnThemMDA.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnThemMDA.setBounds(450, 510, 130, 55);
        Icon icon1c = new ImageIcon("icon-add3.png");
        btnThemMDA.setIcon(icon1c);
        pnMuonDA2.add(btnThemMDA);

        btnSuaMDA = new JButton("Sửa");
        btnSuaMDA.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnSuaMDA.setBounds(650, 510, 130, 55);
        Icon icon2c = new ImageIcon("icon-repair2.png");
        btnSuaMDA.setIcon(icon2c);
        pnMuonDA2.add(btnSuaMDA);

        btnXoaMDA = new JButton("Xóa");
        btnXoaMDA.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnXoaMDA.setBounds(850, 510, 130, 55);
        Icon icon3c = new ImageIcon("icon-delete.png");
        btnXoaMDA.setIcon(icon3c);
        pnMuonDA2.add(btnXoaMDA);

        btnRefreshMDA = new JButton("Refresh");
        btnRefreshMDA.setFont(new Font("Tahoma", Font.PLAIN, 17));
        btnRefreshMDA.setBounds(1050, 510, 130, 55);
        Icon icon4c = new ImageIcon("icon-refresh.png");
        btnRefreshMDA.setIcon(icon4c);
        pnMuonDA2.add(btnRefreshMDA);

        btnThemMDA.addActionListener(this);
        btnSuaMDA.addActionListener(this);
        btnXoaMDA.addActionListener(this);
        btnRefreshMDA.addActionListener(this);

        JLabel lblTieuDe = new JLabel("Cập nhật dữ liệu");
        lblTieuDe.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblTieuDe.setBounds(650, 11, 209, 27);

        Icon icon5 = new ImageIcon("icon-dangkymonhoc1.png");
        lblTieuDe.setHorizontalTextPosition(JLabel.RIGHT);
        lblTieuDe.setVerticalTextPosition(JLabel.CENTER);
        lblTieuDe.setIcon(icon5);

        contentPane.add(lblTieuDe);

//      Hien thi du lieu khi click vao bang GiangVien
        jtUpdateGV.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

                if (jtUpdateGV.getSelectedRow() >= 0) {
                    tfMaGV.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 0));
                    tfHoTen.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 1));

                    tfNgaySinh.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 3));
                    jtfNgayVeTruong.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 4));
                    jtfNgayVaoDang.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 5));
                    cbboxChucDanh.setSelectedItem((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 6));
                    cbboxChucVu.setSelectedItem((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 7));
                    cbboxGhiChu.setSelectedItem((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 16));
                    tfDienThoai.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 8));
                    tfEmail.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 9));
                    tfDienChi.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 10));
                    tfBoMon.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 11));
                    tfChiBo.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 12));
                    jtfSoTK.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 13));
                    jtfMaSoThue.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 14));
                    jtfSoCMND.setText((String) jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 15));
                    if (jtUpdateGV.getValueAt(jtUpdateGV.getSelectedRow(), 2).toString().equals("Nam")) {
                        jrbGTNam.setSelected(true);
                    } else {
                        jrbGTNu.setSelected(true);
                    }
                }
            }
        });
//      Hien thi du lieu khi click vao bang DoAn
        jtUpdateDoAn.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

                if (jtUpdateDoAn.getSelectedRow() >= 0) {
                    tfMaDA.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 0));
                    tfTenDeTai.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 1));

                    tfMaSV.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 2));
                    jtfTenSV.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 3));
                    jtfDienThoaiSV.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 4));
                    jtfEmailSV.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 5));
                    jtfTenLopSV.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 6));
                    jtfKhoaSV.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 7));
                    cbboxKyHoc.setSelectedItem((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 8));
                    cbboxNamHoc.setSelectedItem((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 9));
                    tfMaGVHD.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 10));
                    tfTenGVHD.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 11));
                    tfMaGVPB.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 12));
                    tfTenGVPB.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 13));
                    tfNgayBaoVe.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 14));
                    tfNgayThanhLy.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 15));
                    tfGhiChuDA.setText((String) jtUpdateDoAn.getValueAt(jtUpdateDoAn.getSelectedRow(), 16));

                }

            }
        });

//      Hien thi du lieu khi click vao bang MuonDoAn
        jtUpdateMuonDA.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

                if (jtUpdateMuonDA.getSelectedRow() >= 0) {
                    tfMuonDa.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 0));
                    tfMaDA2.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 1));

                    tfTenNguoiMuon.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 2));
                    tfDienThoai2.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 3));
                    tfEmail2.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 4));
                    tfNgayMuon.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 5));
                    tfNgayTra.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 6));
                    tfNgayTraDK.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 7));
                    jtfTinhTrang.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 8));
                    tfBaoLanh.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 9));
                    tfGhiChu2.setText((String) jtUpdateMuonDA.getValueAt(jtUpdateMuonDA.getSelectedRow(), 10));
                   

                }
            }
        });

    }

    DBConnection dbc = new DBConnection();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    BatLoi batLoi = new BatLoi();

    @Override
    public void actionPerformed(ActionEvent e) {
        
        //      Bat su kien Tab GiangVien
        String maGV = tfMaGV.getText();
        String hoTenGV = tfHoTen.getText();
        String gioiTinhGV = "";
        if (jrbGTNam.isSelected()) {
            gioiTinhGV = "Nam";
        } else if (jrbGTNu.isSelected()) {
            gioiTinhGV = "Nữ";
        }
        String ngaySinhGV = tfNgaySinh.getText();
        String ngayVeTruong = jtfNgayVeTruong.getText();
        String ngayVaoDang = jtfNgayVaoDang.getText();
        String chucDanh = cbboxChucDanh.getSelectedItem().toString();
        String chucVu = cbboxChucVu.getSelectedItem().toString();
        String dienthoaiGV = tfDienThoai.getText();
        String emailGV = tfEmail.getText();
        String diaChi = tfDienChi.getText();
        String boMon = tfBoMon.getText();
        String chiBo = tfChiBo.getText();
        String soTK = jtfSoTK.getText();
        String msThue = jtfMaSoThue.getText();
        String soCMND = jtfSoCMND.getText();
        String ghiChuGV = cbboxGhiChu.getSelectedItem().toString();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        
        if (e.getSource() == btnThemGV) {
            
            String kt = batLoi.ktNhapGV(maGV, hoTenGV, ngaySinhGV, ngayVeTruong,
                dienthoaiGV, emailGV, diaChi, boMon, chiBo, soTK, msThue,
                soCMND);
            if(kt.equals("Chua Nhap Du Lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
                return;
            }
            else if(kt.equals("Chua Nhap Ma GV")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Giảng Viên");
                return;
            }
            else if(batLoi.kTraTrung(tfMaGV.getText(), "Select MaGV From GiangVien")){
                JOptionPane.showMessageDialog(null, "Mã GV Đã Tồn tại");
                return;
            }
            else if(kt.equals("Chua Nhap Ten GV")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Giảng Viên");
                return;
            }  
            else if(kt.equals("Chua Nhap Ngay Sinh")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Sinh");
                return;
            }
            else if(kt.equals("Nhap Sai Dinh Dang Ngay Sinh")){
                JOptionPane.showMessageDialog(null, "Ngày Sinh Không Đúng Định Dạng: YYYY-MM-dd");
                return;
            }
            else if(kt.equals("Chua Nhap Ngay Ve Truong")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Về Trường");
                return;
            }
            else if(kt.equals("Nhap Sai Dinh Dang Ngay Ve Truong")){
                JOptionPane.showMessageDialog(null, "Ngày Về Trường Không Đúng Định Dạng: YYYY-MM-dd");
                return;
            }
            else if(kt.equals("Chua Nhap Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số Điện Thoại");
                return;
            }
            else if(kt.equals("Day Khong Phai So Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Số Điện Thoại Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Email")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Email");
                return;
            }
            else if(kt.equals("Day Khong Phai Email")){
                JOptionPane.showMessageDialog(null, "Email Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Dia Chi")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Địa Chỉ");
                return;
            }
            else if(kt.equals("Chua Nhap Bo Mon")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Bộ Môn");
                return;
            }
            else if(kt.equals("Chua Nhap Chi Bo")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Chi Bộ");
                return;
            }
            else if(kt.equals("Chua Nhap So Tai Khoan")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số Tài Khoản");
                return;
            }
            else if(kt.equals("Day Khong Phai So Tai Khoan")){
                JOptionPane.showMessageDialog(null, "Số Tài Khoản Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Ma So Thue")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Số Thuế");
                return;
            }
            else if(kt.equals("Day Khong Phai Ma So Thue")){
                JOptionPane.showMessageDialog(null, "Mã Số Thuế Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap So CMND")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số CMND");
                return;
            }
            else if(kt.equals("Day Khong Phai So CMND")){
                JOptionPane.showMessageDialog(null, "Số CMND Không Đúng Định Dạng");
                return;
            }            
            else if(kt.equals("Du Lieu Thoa Man")){
                int temp = 0;
                try {
                    temp = update.themGV(maGV, hoTenGV, gioiTinhGV, ngaySinhGV, ngayVeTruong, ngayVaoDang,
                            chucDanh, chucVu, dienthoaiGV, emailGV, diaChi, boMon, chiBo, soTK, msThue, soCMND, ghiChuGV);

                    if (temp > 0) {
                        JOptionPane.showMessageDialog(this, "Thêm Thành Công");
                        dtmUpdateGV.setRowCount(0);
                        ld.loadDataTable(jtUpdateGV, dtmUpdateGV, jspUpdateGV, null, null, null, null, sqlGV);
                    } else {
                        JOptionPane.showMessageDialog(this, "Lỗi");
                    }

                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }
            }
            
        }
        if (e.getSource() == btnSuaGV) {
            
            String kt = batLoi.ktNhapGV(maGV, hoTenGV, ngaySinhGV, ngayVeTruong,
                dienthoaiGV, emailGV, diaChi, boMon, chiBo, soTK, msThue,
                soCMND);
            if(kt.equals("Chua Nhap Du Lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
                return;
            }
            else if(kt.equals("Chua Nhap Ma GV")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Giảng Viên");
                return;
            }
            
            else if(kt.equals("Chua Nhap Ten GV")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Giảng Viên");
                return;
            }  
            else if(kt.equals("Chua Nhap Ngay Sinh")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Sinh");
                return;
            }
            else if(kt.equals("Nhap Sai Dinh Dang Ngay Sinh")){
                JOptionPane.showMessageDialog(null, "Ngày Sinh Không Đúng Định Dạng: YYYY-MM-dd");
                return;
            }
            else if(kt.equals("Chua Nhap Ngay Ve Truong")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Về Trường");
                return;
            }
            else if(kt.equals("Nhap Sai Dinh Dang Ngay Ve Truong")){
                JOptionPane.showMessageDialog(null, "Ngày Về Trường Không Đúng Định Dạng: YYYY-MM-dd");
                return;
            }
            else if(kt.equals("Chua Nhap Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số Điện Thoại");
                return;
            }
            else if(kt.equals("Day Khong Phai So Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Số Điện Thoại Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Email")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Email");
                return;
            }
            else if(kt.equals("Day Khong Phai Email")){
                JOptionPane.showMessageDialog(null, "Email Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Dia Chi")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Địa Chỉ");
                return;
            }
            else if(kt.equals("Chua Nhap Bo Mon")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Bộ Môn");
                return;
            }
            else if(kt.equals("Chua Nhap Chi Bo")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Chi Bộ");
                return;
            }
            else if(kt.equals("Chua Nhap So Tai Khoan")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số Tài Khoản");
                return;
            }
            else if(kt.equals("Day Khong Phai So Tai Khoan")){
                JOptionPane.showMessageDialog(null, "Số Tài Khoản Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Ma So Thue")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Số Thuế");
                return;
            }
            else if(kt.equals("Day Khong Phai Ma So Thue")){
                JOptionPane.showMessageDialog(null, "Mã Số Thuế Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap So CMND")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số CMND");
                return;
            }
            else if(kt.equals("Day Khong Phai So CMND")){
                JOptionPane.showMessageDialog(null, "Số CMND Không Đúng Định Dạng");
                return;
            }            
            else if(kt.equals("Du Lieu Thoa Man")){
            
                try {
                    int temp = update.suaGV(maGV, hoTenGV, gioiTinhGV, ngaySinhGV, ngayVeTruong, ngayVaoDang,
                            chucDanh, chucVu, dienthoaiGV, emailGV, diaChi, boMon, chiBo, soTK, msThue, soCMND, ghiChuGV);
                    if (temp > 0) {
                        JOptionPane.showMessageDialog(this, "Cập Nhật Thành Công");
                        dtmUpdateGV.setRowCount(0);
                        ld.loadDataTable(jtUpdateGV, dtmUpdateGV, jspUpdateGV, null, null, null, null, sqlGV);
                    } else {
                        JOptionPane.showMessageDialog(this, "Lỗi");
                    }
                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }
            }
        }
        if (e.getSource() == btnXoaGV) {
            int click = JOptionPane.showConfirmDialog(this, "Bạn Có Muốn Xóa Không ?");
            if (click == JOptionPane.YES_OPTION) {
                try {
                    Info_GV gv = new Info_GV();
                    gv.setMaGV(maGV);
                    int kt = update.xoaGV(gv);
                    if (kt > 0) {
                        dtmUpdateGV.setRowCount(0);
                        ld.loadDataTable(jtUpdateGV, dtmUpdateGV, jspUpdateGV, null, null, null, null, sqlGV);
                    } else {
                        JOptionPane.showMessageDialog(this, "Loi");
                    }

                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }
            }

        }
        if (e.getSource() == btnRefreshGV) {
            tfMaGV.setText("");
            tfHoTen.setText("");
            tfNgaySinh.setText("");
            jtfNgayVeTruong.setText("");
            jtfNgayVaoDang.setText("");
            tfDienThoai.setText("");
            tfEmail.setText("");
            tfDienChi.setText("");
            tfBoMon.setText("");
            tfChiBo.setText("");
            jtfSoTK.setText("");
            jtfMaSoThue.setText("");
            jtfSoCMND.setText("");
        }
        
        
        ////       Bat su kien Tab DoAn
        
        String maDoAn = tfMaDA.getText();
        String tenDetai = tfTenDeTai.getText();
        String maSV = tfMaSV.getText();
        String tenSV = jtfTenSV.getText();
        String dienThoaiSV = jtfDienThoaiSV.getText();
        String emailSV = jtfEmailSV.getText();
        String tenLop = jtfTenLopSV.getText();
        String khoa = jtfKhoaSV.getText();
        String kyHoc = cbboxKyHoc.getSelectedItem().toString();
        String namHoc = cbboxNamHoc.getSelectedItem().toString();
        String maGVHD = tfMaGVHD.getText();
        String tenGVHD = tfTenGVHD.getText();
        String maGVPB = tfMaGVPB.getText();
        String tenGVPB = tfTenGVPB.getText();
        String ngayBaoVe = tfNgayBaoVe.getText();
        String ngayThanhLy = tfNgayThanhLy.getText();
        String ghiChuDA = tfGhiChuDA.getText();

        
        if (e.getSource() == btnThemDA) {
            
            String kt = batLoi.ktNhapDA(maDoAn, tenDetai, maSV, tenSV, dienThoaiSV, emailSV, tenLop, khoa, maGVHD, tenGVHD, maGVPB, tenGVPB, ngayBaoVe, ngayThanhLy, ghiChuDA);
            
            if(kt.equals("Chua Nhap Du Lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
                return;
            }
            else if(kt.equals("Chua Nhap Ma Do An")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Đồ Án");
                return;
            }
            else if (batLoi.kTraTrung(tfMaDA.getText(), "Select Madoan From DoAn")) {
                JOptionPane.showMessageDialog(this, "Mã Đồ Án Đã Tồn Tại");
                return;
            }
            else if(kt.equals("Chua Nhap Ten De Tai")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Đề tài");
                return;
            }
            else if(kt.equals("Chua Nhap Ma Sinh Vien")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Sinh Viên");
                return;
            }
            else if (batLoi.kTraTrung(tfMaSV.getText(), "Select Masv From DoAn")){
                JOptionPane.showMessageDialog(null, "Mã SV Đã Tồn Tại");
                return;
            }
            else if(kt.equals("Chua Nhap Ten Sinh Vien")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Sinh Viên");
                return;
            }
            else if(kt.equals("Chua Nhap So Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số Điện Thoại");
                return;
            }
            else if(kt.equals("Day Khong Phai So Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Số Điện Thoại Không ĐÚng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Email")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Email");
                return;
            }
            else if(kt.equals("Day Khong Phai Email")){
                JOptionPane.showMessageDialog(null, "Email Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Ten Lop")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Lớp");
                return;
            }
            else if(kt.equals("Chua Nhap Khoa")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Khoa");
                return;
            }
            else if(kt.equals("Chua Nhap Ma GVHD")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã GVHD");
                return;
            }
            else if (!batLoi.kTraTrung(tfMaGVHD.getText(), "Select MaGV From GiangVien")) {// && batLoi.nhapSai(tfTenGVHD.getText(), "Select HotenGV From GiangVien, DoAn Where MaGV = MaGVHD")) {
                JOptionPane.showMessageDialog(this, "Mã GVHD Không Tồn Tại");
                return;
            }
            else if(kt.equals("Chua Nhap Ten GVHD")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên GVHD");
                return;
            }
            else if(batLoi.nhapSaiTenGVHD(tfTenGVHD.getText(), tfMaGVHD.getText())){
                JOptionPane.showMessageDialog(null, "Tên GVHD và Mã GVHD Không Trùng Khớp");
            }
            else if(kt.equals("Chua Nhap Ma GVPB")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã GVPB");
                return;
            }
            else if (!batLoi.kTraTrung(tfMaGVPB.getText(), "Select MaGV From GiangVien")) {// && batLoi.nhapSai(tfTenGVPB.getText(), "Select HotenGV From GiangVien, DoAn Where MaGV = MaGVPB")) {
                 JOptionPane.showMessageDialog(this, "Mã GVPB Không Tồn Tại");
                 return;
            }
            else if(kt.equals("Chua Nhap Ten GVPB")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên GVPB");
                return;
            }
            else if(batLoi.nhapSaiTenGVPB(tfTenGVPB.getText(), tfMaGVPB.getText())){
                JOptionPane.showMessageDialog(null, "Tên GVPB và Mã GVPB Không Trùng Khớp");
            }
            else if(kt.equals("Chua Nhap Ngay Bao Ve")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bảo Vệ");
                return;
            }
            else if(kt.equals("ngayBaoVe Khong Dung Dinh Dang Ngay")){
                JOptionPane.showMessageDialog(null, "Ngày Bảo Vệ Không Đúng Định Dạng: YYYY-MM-dd");
                return;
            }
            else if(kt.equals("Chua Nhap Ngay Thanh Ly")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Thanh Lý");
                return;
            }
            else if(kt.equals("ngayThanhLy Khong Dung Dinh Dang Ngay")){
                JOptionPane.showMessageDialog(null, "Ngày Thanh Lý Không Đúng Định Dạng: YYYY-MM-dd");
                return;
            }
//            else if(kt.equals("Chua Nhap Ghi Chu")){
//                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ghi Chú");
//            }
            else if(kt.equals("Du Lieu Thoa Man")){
                
                try {       
                    int temp = update.themDA(maDoAn, tenDetai, maSV, tenSV,
                            dienThoaiSV, emailSV, tenLop, khoa, kyHoc, namHoc,
                            maGVHD, tenGVHD, maGVPB, tenGVPB, ngayBaoVe, ngayThanhLy, ghiChuDA);
                    if (temp > 0) {
                        JOptionPane.showMessageDialog(this, "Thêm Thành Công");
                        dtmUpdateDoAn.setRowCount(0);
                        ld.loadDataTable(jtUpdateDoAn, dtmUpdateDoAn, jspUpdateDoAn, null, null, null, null, sqlDA);
                    } else {
                        JOptionPane.showMessageDialog(this, "Lỗi");
                    }

                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }
            }
            
        }

        if (e.getSource() == btnSuaDA) {

            String kt = batLoi.ktNhapDA(maDoAn, tenDetai, maSV, tenSV, dienThoaiSV, emailSV, tenLop, khoa, maGVHD, tenGVHD, maGVPB, tenGVPB, ngayBaoVe, ngayThanhLy, ghiChuDA);
            
            if(kt.equals("Chua Nhap Du Lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
                return;
            }
            else if(kt.equals("Chua Nhap Ma Do An")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Đồ Án");
                return;
            }
            
            else if(kt.equals("Chua Nhap Ten De Tai")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Đề tài");
                return;
            }
            else if(kt.equals("Chua Nhap Ma Sinh Vien")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Sinh Viên");
                return;
            }
            
            else if(kt.equals("Chua Nhap Ten Sinh Vien")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Sinh Viên");
                return;
            }
            else if(kt.equals("Chua Nhap So Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số Điện Thoại");
                return;
            }
            else if(kt.equals("Day Khong Phai So Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Số Điện Thoại Không ĐÚng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Email")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Email");
                return;
            }
            else if(kt.equals("Day Khong Phai Email")){
                JOptionPane.showMessageDialog(null, "Email Không Đúng Định Dạng");
                return;
            }
            else if(kt.equals("Chua Nhap Ten Lop")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Lớp");
                return;
            }
            else if(kt.equals("Chua Nhap Khoa")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Khoa");
                return;
            }
            else if(kt.equals("Chua Nhap Ma GVHD")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã GVHD");
                return;
            }
            else if (!batLoi.kTraTrung(tfMaGVHD.getText(), "Select MaGV From GiangVien")) {// && batLoi.nhapSai(tfTenGVHD.getText(), "Select HotenGV From GiangVien, DoAn Where MaGV = MaGVHD")) {
                JOptionPane.showMessageDialog(this, "Mã GVHD Không Tồn Tại");
                return;
            }
            else if(kt.equals("Chua Nhap Ten GVHD")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên GVHD");
                return;
            }
            else if(batLoi.nhapSaiTenGVHD(tfTenGVHD.getText(), tfMaGVHD.getText())){
                JOptionPane.showMessageDialog(null, "Tên GVHD và Mã GVHD Không Trùng Khớp");
            }
            else if(kt.equals("Chua Nhap Ma GVPB")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã GVPB");
                return;
            }
            else if (!batLoi.kTraTrung(tfMaGVPB.getText(), "Select MaGV From GiangVien")) {// && batLoi.nhapSai(tfTenGVPB.getText(), "Select HotenGV From GiangVien, DoAn Where MaGV = MaGVPB")) {
                 JOptionPane.showMessageDialog(this, "Mã GVPB Không Tồn Tại");
                 return;
            }
            else if(kt.equals("Chua Nhap Ten GVPB")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên GVPB");
                return;
            }
            else if(batLoi.nhapSaiTenGVPB(tfTenGVPB.getText(), tfMaGVPB.getText())){
                JOptionPane.showMessageDialog(null, "Tên GVPB và Mã GVPB Không Trùng Khớp");
            }
            else if(kt.equals("Chua Nhap Ngay Bao Ve")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Bảo Vệ");
                return;
            }
            else if(kt.equals("ngayBaoVe Khong Dung Dinh Dang Ngay")){
                JOptionPane.showMessageDialog(null, "Ngày Bảo Vệ Không Đúng Định Dạng: YYYY-MM-dd");
                return;
            }
            else if(kt.equals("Chua Nhap Ngay Thanh Ly")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Thanh Lý");
                return;
            }
            else if(kt.equals("ngayThanhLy Khong Dung Dinh Dang Ngay")){
                JOptionPane.showMessageDialog(null, "Ngày Thanh Lý Không Đúng Định Dạng: YYYY-MM-dd");
                return;
            }
//            else if(kt.equals("Chua Nhap Ghi Chu")){
//                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ghi Chú");
//            }
            else if(kt.equals("Du Lieu Thoa Man")){
                try {
                    int temp = update.suaDA(maDoAn, tenDetai, maSV, tenSV, dienThoaiSV,
                            emailSV, tenLop, khoa, kyHoc, namHoc, maGVHD, tenGVHD, maGVPB,
                            tenGVPB, ngayBaoVe, ngayThanhLy, ghiChuDA);
                    if (temp > 0) {
                        JOptionPane.showMessageDialog(this, "Cap Nhat Thanh Cong");
                        dtmUpdateDoAn.setRowCount(0);
                        ld.loadDataTable(jtUpdateDoAn, dtmUpdateDoAn, jspUpdateDoAn, null, null, null, null, sqlDA);
                    } else {
                        JOptionPane.showMessageDialog(this, "Lỗi");
                    }

                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }
            }
        }
        if (e.getSource() == btnXoaDA) {
            int click = JOptionPane.showConfirmDialog(this, "Bạn Có Muốn Xóa Không ?");
            if (click == JOptionPane.YES_OPTION) {
                try {
                    Info_DA da = new Info_DA();
                    da.setMaDoAn(maDoAn);
                    int temp = update.xoaDA(da);
                    if (temp > 0) {
                        JOptionPane.showMessageDialog(this, "Xoa Thanh Cong");
                        dtmUpdateDoAn.setRowCount(0);
                        ld.loadDataTable(jtUpdateDoAn, dtmUpdateDoAn, jspUpdateDoAn, null, null, null, null, sqlDA);
                    } else {
                        JOptionPane.showMessageDialog(this, "Lỗi");
                    }

                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }

            }

        }
        if (e.getSource() == btnRefreshDA) {

            tfMaDA.setText("");
            tfTenDeTai.setText("");
            tfMaSV.setText("");
            jtfTenSV.setText("");
            jtfDienThoaiSV.setText("");
            jtfEmailSV.setText("");
            jtfTenLopSV.setText("");
            jtfKhoaSV.setText("");
            tfMaGVHD.setText("");
            tfTenGVHD.setText("");
            tfMaGVPB.setText("");
            tfTenGVPB.setText("");
            tfNgayBaoVe.setText("");
            tfNgayThanhLy.setText("");
            tfGhiChuDA.setText("");

        }
        

////      Bat su kien Tab MuonDoAn
        String maMDA = tfMuonDa.getText();
        String maDA = tfMaDA2.getText();
        String tenNguoiMuon = tfTenNguoiMuon.getText();
        String dienThoaiMDA = tfDienThoai2.getText();
        String emailMDA = tfEmail2.getText();
        String ngayMuon = tfNgayMuon.getText();
        String ngayTra = tfNgayTra.getText();
        String ngayTraDuKien = tfNgayTraDK.getText();
        String tinhTrang = jtfTinhTrang.getText();
        String baoLanh = tfBaoLanh.getText();
        String ghiChuMDA = tfGhiChu2.getText();

        if (e.getSource() == btnThemMDA) {
            
            String kt = batLoi.ktNhapMDA(maMDA, maDA, tenNguoiMuon, dienThoaiMDA,
                    emailMDA, ngayMuon, ngayTra, ngayTraDuKien,tinhTrang, baoLanh, ghiChuMDA);
            
            if(kt.equals("Chua Nhap Du Lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
                
            }
            else if(kt.equals("Chua Nhap Ma Muon Do An")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Mượn Đồ Án");
               
            }
            else if (batLoi.kTraTrung(tfMuonDa.getText(), "Select [Muon-doan] From MuonDoAn")) {
                JOptionPane.showMessageDialog(this, "Mã Mượn Đồ Án Đã Tồn Tại");
                
            }           
            else if(kt.equals("Chua Nhap Ma Do An")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Đồ Án");
                
            }
            else if (!batLoi.kTraTrung(tfMaDA2.getText(), "Select Madoan From DoAn")) {
                JOptionPane.showMessageDialog(this, "Mã Đồ Án Không Tồn Tại");
                
            }
            else if(batLoi.kTraTrung(tfMaDA2.getText(), "Select MaDoAn From MuonDoAn")){
                JOptionPane.showMessageDialog(null, "Lỗi: Mã Đồ Án Này Đã Được Mượn");
                
            }
            else if(kt.equals("Chua Nhap Ten Nguoi Muon")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Người Mượn");
                
            }
            else if(kt.equals("Chua Nhap Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số Điện Thoại");
                
            }
            else if(kt.equals("Khong Phai So Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Số Điện Thoại Không Đúng Định Dạng");
                
            }
            else if(kt.equals("Chua Nhap Email")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Email");
                
            }
            else if(kt.equals("Khong Phai Email")){
                JOptionPane.showMessageDialog(null, "Email Không Đúng Định Dạng");
                
            }
            else if(kt.equals("Chua Nhap Ngay Muon")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Mượn");
                
            }
            else if(kt.equals("Ngay Muon Khong Dung Dinh Dang")){
                JOptionPane.showMessageDialog(null, "Ngày Mượn Không Đúng Định Dạng: YYYY-MM-dd");
                
            }
            else if(kt.equals("Chua Nhap Ngay Tra")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Trả");
                
            }
            else if(kt.equals("Ngay Tra Khong Dung Dinh Dang")){
                JOptionPane.showMessageDialog(null, "Ngày Trả Không Đúng Định Dạng: YYYY-MM-dd");
                
            }
            else if(kt.equals("Chua Nhap Ngay Tra Du Kien")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Trả Dự Kiến");
                
            }
            else if(kt.equals("Ngay Tra Du Kien Khong Dung Dinh Dang")){
                JOptionPane.showMessageDialog(null, "Ngày Trả Dự kiến Không Đúng Định Dạng: YYYY-MM-dd");
                
            }
//            else if(kt.equals("Chua Nhap Ghi Chu")){
//                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ghi Chú");
//                return;
//            }
            else if(kt.equals("Du Lieu Thoa Man")){
                try {
                           
                    int temp = update.themMDA(maMDA, maDA, tenNguoiMuon, dienThoaiMDA,
                            emailMDA, ngayMuon, ngayTra, ngayTraDuKien, tinhTrang, baoLanh, ghiChuMDA);
                    if (temp > 0) {
                        JOptionPane.showMessageDialog(this, "Thêm Thành Công");
                        dtmUpdateMuonDA.setRowCount(0);
                        ld.loadDataTable(jtUpdateMuonDA, dtmUpdateMuonDA, jspUpdateMuonDA, null, null, null, null, sqlMDA);
                    } else {
                        JOptionPane.showMessageDialog(this, "Lỗi");
                    }

                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }
            }
       
        }

        if (e.getSource() == btnSuaMDA) {
            
            String kt = batLoi.ktNhapMDA(maMDA, maDA, tenNguoiMuon, dienThoaiMDA,
                    emailMDA, ngayMuon, ngayTra, ngayTraDuKien, tinhTrang, baoLanh, ghiChuMDA);
            
            if(kt.equals("Chua Nhap Du Lieu")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Dữ Liệu");
                
            }
            else if(kt.equals("Chua Nhap Ma Muon Do An")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Mượn Đồ Án");
                
            }
                     
            else if(kt.equals("Chua Nhap Ma Do An")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Mã Đồ Án");
                
            }
                       
            else if(kt.equals("Chua Nhap Ten Nguoi Muon")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Tên Người Mượn");
                
            }
            else if(kt.equals("Chua Nhap Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Số Điện Thoại");
                
            }
            else if(kt.equals("Khong Phai So Dien Thoai")){
                JOptionPane.showMessageDialog(null, "Số Điện Thoại Không Đúng Định Dạng");
                
            }
            else if(kt.equals("Chua Nhap Email")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Email");
                
            }
            else if(kt.equals("Khong Phai Email")){
                JOptionPane.showMessageDialog(null, "Email Không Đúng Định Dạng");
                
            }
            else if(kt.equals("Chua Nhap Ngay Muon")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Mượn");
                
            }
            else if(kt.equals("Ngay Muon Khong Dung Dinh Dang")){
                JOptionPane.showMessageDialog(null, "Ngày Mượn Không Đúng Định Dạng: YYYY-MM-dd");
                
            }
            else if(kt.equals("Chua Nhap Ngay Tra")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Trả");
                
            }
            else if(kt.equals("Ngay Tra Khong Dung Dinh Dang")){
                JOptionPane.showMessageDialog(null, "Ngày Trả Không Đúng Định Dạng: YYYY-MM-dd");
                
            }
            else if(kt.equals("Chua Nhap Ngay Tra Du Kien")){
                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ngày Trả Dự Kiến");
                
            }
            else if(kt.equals("Ngay Tra Du Kien Khong Dung Dinh Dang")){
                JOptionPane.showMessageDialog(null, "Ngày Trả Dự kiến Không Đúng Định Dạng: YYYY-MM-dd");
                
            }
//            else if(kt.equals("Chua Nhap Ghi Chu")){
//                JOptionPane.showMessageDialog(null, "Bạn Chưa Nhập Ghi Chú");
//                return;
//            }
            else if(kt.equals("Du Lieu Thoa Man")){

                try {

                    int temp = update.suaMDA(maMDA, maDA, tenNguoiMuon, dienThoaiMDA, emailMDA,
                            ngayMuon, ngayTra, ngayTraDuKien, tinhTrang, baoLanh, ghiChuMDA);
                    if (temp > 0) {
                        JOptionPane.showMessageDialog(this, "Cap Nhat Thanh Cong");
                        dtmUpdateMuonDA.setRowCount(0);
                        ld.loadDataTable(jtUpdateMuonDA, dtmUpdateMuonDA, jspUpdateMuonDA, null, null, null, null, sqlMDA);
                    } else {
                        JOptionPane.showMessageDialog(this, "Lỗi");
                    }

                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }
            }

        }
        if (e.getSource() == btnXoaMDA) {
            int click = JOptionPane.showConfirmDialog(this, "Bạn Có Muốn Xóa Không ?");
            if (click == JOptionPane.YES_OPTION) {
                try {
                    
                    Info_MDA mda = new Info_MDA();
                    mda.setMaMuonDA(maMDA);
                    int temp = update.xoaMDA(mda);
                    if (temp > 0) {
                        JOptionPane.showMessageDialog(this, "Xoa Thanh Cong");
                        dtmUpdateMuonDA.setRowCount(0);
                        ld.loadDataTable(jtUpdateMuonDA, dtmUpdateMuonDA, jspUpdateMuonDA, null, null, null, null, sqlMDA);
                    } else {
                        JOptionPane.showMessageDialog(this, "Lỗi");
                    }

                } catch (Exception ex) {
                    System.out.println("Loi: " + ex.getMessage());
                }
            }

        }
        if (e.getSource() == btnRefreshMDA) {
            tfMuonDa.setText("");
            tfMaDA2.setText("");
            tfTenNguoiMuon.setText("");
            tfDienThoai2.setText("");
            tfEmail2.setText("");
            tfNgayMuon.setText("");
            tfNgayTra.setText("");
            tfNgayTraDK.setText("");
            tfBaoLanh.setText("");
            tfGhiChu2.setText("");
            
        }
    }
}
