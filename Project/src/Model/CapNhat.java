package Model;

import Control.DBConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CapNhat {

    DBConnection dbc = new DBConnection();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public ArrayList selectInfo_GV() {
        ArrayList arr = new ArrayList();
        String sql = "select * from GiangVien";
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Info_GV gv = new Info_GV();
                
                gv.setMaGV(rs.getString("MaGV"));
                gv.setHoTenGV(rs.getString("HotenGV"));
                gv.setGioiTinhGV(rs.getString("Gioitinh"));
                gv.setNgaySinhGV(rs.getString("Ngaysinh"));
                gv.setNgayVeTruong(rs.getString("Ngayvetruong"));
                gv.setNgayVaoDang(rs.getString("Ngayvaodang"));
                gv.setChucDanh(rs.getString("Chucdanh"));
                gv.setChucVu(rs.getString("Chucvu"));
                gv.setDienThoaiGV(rs.getString("Dienthoai"));
                gv.setEmailGV(rs.getString("Email"));
                gv.setDiaChi(rs.getString("Diachi"));
                gv.setBoMon(rs.getString("Bomon"));
                gv.setChiBo(rs.getString("Chibo"));
                gv.setSoTK(rs.getString("Sotaikhoan"));
                gv.setMsThue(rs.getString("Masothue"));
                gv.setSoCMND(rs.getString("SoCMND"));
                gv.setGhiChu(rs.getString("Ghichu"));
                
                arr.add(gv);
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return arr;
    }
    
    public int themGV(Info_GV gv){
        
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement("insert into GiangVien"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, gv.getMaGV());
            ps.setString(2, gv.getHoTenGV());
            ps.setString(3, gv.getGioiTinhGV());
            ps.setString(4, gv.getNgaySinhGV());
            ps.setString(5, gv.getNgayVeTruong());
            ps.setString(6, gv.getNgayVaoDang());
            ps.setString(7, gv.getChucDanh());
            ps.setString(8, gv.getChucVu());
            ps.setString(9, gv.getDienThoaiGV());
            ps.setString(10, gv.getEmailGV());
            ps.setString(11, gv.getDiaChi());
            ps.setString(12, gv.getBoMon());
            ps.setString(13, gv.getChiBo());
            ps.setString(14, gv.getSoTK());
            ps.setString(15, gv.getMsThue());
            ps.setString(16, gv.getSoCMND());
            ps.setString(17, gv.getGhiChu());

            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }

        return temp;
    }
    
    public int suaGV(Info_GV gv){
        
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            String sql = "Update GiangVien"
                        + " Set HotenGV = ?, Gioitinh = ?, Ngaysinh = ?,"
                        + " Ngayvetruong = ?, Ngayvaodang = ?, Chucdanh = ?, Chucvu = ?,"
                        + " Dienthoai = ?, Email = ?, Diachi = ?, Bomon = ?,"
                        + " Chibo = ?, Sotaikhoan = ?, Masothue = ?, SoCMND = ?, Ghichu = ?"
                        + " Where MaGV = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(17, gv.getMaGV());
            ps.setString(1, gv.getHoTenGV());
            ps.setString(2, gv.getGioiTinhGV());
            ps.setString(3, gv.getNgaySinhGV());
            ps.setString(4, gv.getNgayVeTruong());
            ps.setString(5, gv.getNgayVaoDang());
            ps.setString(6, gv.getChucDanh());
            ps.setString(7, gv.getChucVu());
            ps.setString(8, gv.getDienThoaiGV());
            ps.setString(9, gv.getEmailGV());
            ps.setString(10, gv.getDiaChi());
            ps.setString(11, gv.getBoMon());
            ps.setString(12, gv.getChiBo());
            ps.setString(13, gv.getSoTK());
            ps.setString(14, gv.getMsThue());
            ps.setString(15, gv.getSoCMND());
            ps.setString(16, gv.getGhiChu());

            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }

        return temp;
    }
    
    public int xoaGV(Info_GV gv) {
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            String sql = "delete from GiangVien where MaGV = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, gv.getMaGV());
            
            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return temp;
    }
/////////////////////////////////////
    public ArrayList selectInfo_DA() {
        ArrayList arr = new ArrayList();
        String sql = "select * from DoAn";
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Info_DA da = new Info_DA();
                
                da.setMaDoAn(rs.getString("Madoan"));
                da.setTenDeTai(rs.getString("Tendetai"));
                da.setMaSV(rs.getString("Masv"));
                da.setTenSV(rs.getString("TenSV"));
                da.setDienThoaiSV(rs.getString("DienThoai"));
                da.setEmailSV(rs.getString("Email"));
                da.setTenLop(rs.getString("TenLop"));
                da.setKhoa(rs.getString("Khoa"));
                da.setKyHoc(rs.getString("Kyhoc"));
                da.setNamHoc(rs.getString("Namhoc"));
                da.setMaGVHD(rs.getString("MaGVHD"));
                da.setTenGVHD(rs.getString("TenGVHD"));
                da.setMaGVPB(rs.getString("MaGVPB"));
                da.setTenGVPB(rs.getString("TenGVPB"));
                da.setNgayBaoVe(rs.getString("Ngaybaove"));
                da.setNgayThanhLy(rs.getString("Ngaythanhly"));
                da.setGhiChu(rs.getString("Ghichu"));
                
                arr.add(da);
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return arr;
    }
    
    public int themDA(Info_DA da){
        
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement("insert into DoAn"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, da.getMaDoAn());
            ps.setString(2, da.getTenDeTai());
            ps.setString(3, da.getMaSV());
            ps.setString(4, da.getTenSV());
            ps.setString(5, da.getDienThoaiSV());
            ps.setString(6, da.getEmailSV());
            ps.setString(7, da.getTenLop());
            ps.setString(8, da.getKhoa());
            ps.setString(9, da.getKyHoc());
            ps.setString(10, da.getNamHoc());
            ps.setString(11, da.getMaGVHD());
            ps.setString(12, da.getTenGVHD());
            ps.setString(13, da.getMaGVPB());
            ps.setString(14, da.getTenGVPB());
            ps.setString(15, da.getNgayBaoVe());
            ps.setString(16, da.getNgayThanhLy());
            ps.setString(17, da.getGhiChu());

            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }

        return temp;
    }
    
    public int suaDA(Info_DA da){
        
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            String sql = "Update DoAn"
                        + " Set Tendetai = ?, Masv = ?, TenSV = ?, DienThoai = ?,"
                        + " Email = ?, TenLop = ?, Khoa = ?, Kyhoc = ?,"
                        + " Namhoc = ?, MaGVHD = ?, TenGVHD = ?, MaGVPB = ?, TenGVPB = ?,"
                        + " Ngaybaove = ?, Ngaythanhly = ?, Ghichu = ?"
                        + " Where Madoan = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(17, da.getMaDoAn());
            ps.setString(1, da.getTenDeTai());
            ps.setString(2, da.getMaSV());
            ps.setString(3, da.getTenSV());
            ps.setString(4, da.getDienThoaiSV());
            ps.setString(5, da.getEmailSV());
            ps.setString(6, da.getTenLop());
            ps.setString(7, da.getKhoa());
            ps.setString(8, da.getKyHoc());
            ps.setString(9, da.getNamHoc());
            ps.setString(10, da.getMaGVHD());
            ps.setString(11, da.getTenGVHD());
            ps.setString(12, da.getMaGVPB());
            ps.setString(13, da.getTenGVPB());
            ps.setString(14, da.getNgayBaoVe());
            ps.setString(15, da.getNgayThanhLy());
            ps.setString(16, da.getGhiChu());

            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }

        return temp;
    }
    
    public int xoaDA(Info_DA da) {
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            String sql = "delete from DoAn where Madoan = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, da.getMaDoAn());
            
            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return temp;
    }
/////////////////////////////////////////////////
    public ArrayList selectInfo_MDA() {
        ArrayList arr = new ArrayList();
        String sql = "select * from MuonDoAn";
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Info_MDA mda = new Info_MDA();
                
                mda.setMaMuonDA(rs.getString("Muon-doan"));
                mda.setMaDoAn(rs.getString("MaDoAn"));
                mda.setTenNguoiMuon(rs.getString("Tennguoimuon"));
                mda.setDienThoai(rs.getString("Dienthoai"));
                mda.setEmail(rs.getString("Email"));
                mda.setNgayMuon(rs.getString("Ngaymuon"));
                mda.setNgayTra(rs.getString("Ngaytra"));
                mda.setNgayTraDuKien(rs.getString("Ngaytradukien"));
                mda.setTinhTrang(rs.getString("Tinhtrang"));
                mda.setBaoLanh(rs.getString("Baolanh"));
                mda.setGhiChu(rs.getString("Ghichu"));
       
                arr.add(mda);
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return arr;
    }
    
    public int themMDA(Info_MDA mda){
        
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement("insert into MuonDoAn"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, mda.getMaMuonDA());
            ps.setString(2, mda.getMaDoAn());
            ps.setString(3, mda.getTenNguoiMuon());
            ps.setString(4, mda.getDienThoai());
            ps.setString(5, mda.getEmail());
            ps.setString(6, mda.getNgayMuon());
            ps.setString(7, mda.getNgayTra());
            ps.setString(8, mda.getNgayTraDuKien());
            ps.setString(9, mda.getTinhTrang());
            ps.setString(10, mda.getBaoLanh());
            ps.setString(11, mda.getGhiChu());

            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }

        return temp;
    }
    
    public int suaMDA(Info_MDA mda){
        
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            String sql = "Update MuonDoAn"
                        + " Set MaDoAn = ?, Tennguoimuon = ?, Dienthoai = ?,"
                        + " Email = ?, Ngaymuon = ?, Ngaytra = ?, Ngaytradukien = ?,"
                        + " Tinhtrang = ?, Baolanh = ?, Ghichu = ?"
                        + " Where [Muon-doan] = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(11, mda.getMaMuonDA());
            ps.setString(1, mda.getMaDoAn());
            ps.setString(2, mda.getTenNguoiMuon());
            ps.setString(3, mda.getDienThoai());
            ps.setString(4, mda.getEmail());
            ps.setString(5, mda.getNgayMuon());
            ps.setString(6, mda.getNgayTra());
            ps.setString(7, mda.getNgayTraDuKien());
            ps.setString(8, mda.getTinhTrang());
            ps.setString(9, mda.getBaoLanh());
            ps.setString(10, mda.getGhiChu());
            

            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }

        return temp;
    }
    
    public int xoaMDA(Info_MDA mda) {
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            String sql = "delete from MuonDoAn where [Muon-doan] = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, mda.getMaMuonDA());
            
            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return temp;
    }
    
    //////////////////////////////////////////////////
    public ArrayList selectInfo_NguoiDung() {
        ArrayList arr = new ArrayList();
        String sql = "select * from Login";
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Info_NguoiDung nd = new Info_NguoiDung();
                
                nd.setTenDangNhap(rs.getString("TenDN"));
                nd.setMatKhau(rs.getString("MatKhau"));
                nd.setHoTen(rs.getString("HoTen"));
                nd.setGioiTinh(rs.getString("GioiTinh"));
                nd.setEmail(rs.getString("Email"));
                nd.setPhanQuyen(rs.getString("PhanQuyen"));
                nd.setTrangThai(rs.getString("TrangThai"));
             
                arr.add(nd);
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return arr;
    }
    
    public int themND(Info_NguoiDung nd){
        
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            ps = conn.prepareStatement("insert into Login"
                    + " values (?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, nd.getTenDangNhap());
            ps.setString(2, nd.getMatKhau());
            ps.setString(3, nd.getHoTen());
            ps.setString(4, nd.getGioiTinh());
            ps.setString(5, nd.getEmail());
            ps.setString(6, nd.getPhanQuyen());
            ps.setString(7, nd.getTrangThai());
            
            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }

        return temp;
    }
    
    public int suaND(Info_NguoiDung nd){
        
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            String sql = "Update Login"
                        + " Set MatKhau = ?, HoTen = ?, GioiTinh = ?,"
                        + " Email = ?, PhanQuyen = ?, TrangThai = ?"
                        + " Where TenDN = ?";
            
            ps = conn.prepareStatement(sql);
            ps.setString(7, nd.getTenDangNhap());
            ps.setString(1, nd.getMatKhau());
            ps.setString(2, nd.getHoTen());
            ps.setString(3, nd.getGioiTinh());
            ps.setString(4, nd.getEmail());
            ps.setString(5, nd.getPhanQuyen());
            ps.setString(6, nd.getTrangThai());
            

            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }

        return temp;
    }
    
    public int xoaND(Info_NguoiDung nd) {
        int temp = 0;
        
        try {
            conn = dbc.Connect();
            String sql = "delete from Login where TenDN = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, nd.getTenDangNhap());
            
            temp = ps.executeUpdate();
            if (temp > 0) {
                return temp;
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return temp;
    }
}
