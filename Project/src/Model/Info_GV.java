
package Model;

import java.sql.Date;

public class Info_GV {
    
    private String maGV;
    private String hoTenGV;
    private String gioiTinhGV;
    private String ngaySinhGV;
    private String ngayVeTruong;
    private String ngayVaoDang;
    private String chucDanh;
    private String chucVu;
    private String dienThoaiGV;
    private String emailGV;
    private String diaChi;
    private String boMon;
    private String chiBo;
    private String soTK;
    private String msThue;
    private String soCMND;
    private String ghiChu;

    public Info_GV() {
    }
    public Info_GV(String maGV, String hoTenGV, String gioiTinhGV, String ngaySinhGV, String ngayVeTruong,
            String ngayVaoDang, String chucDanh, String chucVu, String dienThoaiGV, 
            String emailGV, String diaChi, String boMon, String chiBo, String soTK, 
            String msThue, String soCMND, String ghiChu){
        
        this.maGV = maGV;
        this.hoTenGV = hoTenGV;
        this.gioiTinhGV = gioiTinhGV;
        this.ngaySinhGV = ngaySinhGV;
        this.ngayVeTruong = ngayVeTruong;
        this.ngayVaoDang = ngayVaoDang;
        this.chucDanh = chucDanh;
        this.chucVu = chucVu;
        this.dienThoaiGV = dienThoaiGV;
        this.emailGV = emailGV;
        this.diaChi = diaChi;
        this.boMon = boMon;
        this.chiBo = chiBo;
        this.soTK = soTK;
        this.msThue = msThue;
        this.soCMND = soCMND;
        this.ghiChu = ghiChu;
    }

    public String getMaGV() {
        return maGV;
    }

    public String getHoTenGV() {
        return hoTenGV;
    }

    public String getGioiTinhGV() {
        return gioiTinhGV;
    }

    public String getNgaySinhGV() {
        return ngaySinhGV;
    }

    public String getNgayVeTruong() {
        return ngayVeTruong;
    }

    public String getNgayVaoDang() {
        return ngayVaoDang;
    }

    public String getChucDanh() {
        return chucDanh;
    }

    public String getChucVu() {
        return chucVu;
    }

    public String getDienThoaiGV() {
        return dienThoaiGV;
    }

    public String getEmailGV() {
        return emailGV;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public String getBoMon() {
        return boMon;
    }

    public String getChiBo() {
        return chiBo;
    }

    public String getSoTK() {
        return soTK;
    }

    public String getMsThue() {
        return msThue;
    }

    public String getSoCMND() {
        return soCMND;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setMaGV(String maGV) {
        this.maGV = maGV;
    }

    public void setHoTenGV(String hoTenGV) {
        this.hoTenGV = hoTenGV;
    }

    public void setGioiTinhGV(String gioiTinhGV) {
        this.gioiTinhGV = gioiTinhGV;
    }

    public void setNgaySinhGV(String ngaySinhGV) {
        this.ngaySinhGV = ngaySinhGV;
    }

    public void setNgayVeTruong(String ngayVeTruong) {
        this.ngayVeTruong = ngayVeTruong;
    }

    public void setNgayVaoDang(String ngayVaoDang) {
        this.ngayVaoDang = ngayVaoDang;
    }

    public void setChucDanh(String chucDanh) {
        this.chucDanh = chucDanh;
    }

    public void setChucVu(String chucVu) {
        this.chucVu = chucVu;
    }

    public void setDienThoaiGV(String dienThoaiGV) {
        this.dienThoaiGV = dienThoaiGV;
    }

    public void setEmailGV(String emailGV) {
        this.emailGV = emailGV;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public void setBoMon(String boMon) {
        this.boMon = boMon;
    }

    public void setChiBo(String chiBo) {
        this.chiBo = chiBo;
    }

    public void setSoTK(String soTK) {
        this.soTK = soTK;
    }

    public void setMsThue(String msThue) {
        this.msThue = msThue;
    }

    public void setSoCMND(String soCMND) {
        this.soCMND = soCMND;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    
    
    
    
}
