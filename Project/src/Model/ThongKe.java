
package Model;

import Control.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;

public class ThongKe {
    
    DBConnection dbc = new DBConnection();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public ArrayList thongKeTheoChucDanh() {
        ArrayList arr = new ArrayList();
        String sql = "SELECT Chucdanh, COUNT(*) SoLuong From GiangVien Where Bomon = ? GROUP BY Chucdanh";
        conn = dbc.Connect();
        try {
           // ps = conn.prepareStatement(sql).executeQuery().getS;
            rs = ps.executeQuery();
            while (rs.next()) {
                String diaChi = rs.getString("Chucdanh");
                int soLuong = rs.getInt("SoLuong");
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }
    
    public ArrayList<Integer> MDAKy1(JComboBox jcb){
        ArrayList<Integer> ky1 = new ArrayList<Integer>();
        conn = dbc.Connect();
        try {
            ps = conn.prepareStatement("Select Count(*) SoLuong From MuonDoAn "
                    + "Where Year(Ngaymuon) = ? AND Month(Ngaymuon) Between '01' AND '06' Group By Tennguoimuon");
            ps.setString(1, jcb.getSelectedItem().toString());
            rs = ps.executeQuery();
            while(rs.next()){
                int soLuong = rs.getInt("SoLuong");
                ky1.add(soLuong);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ThongKe.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ky1;
    }
    public ArrayList<Integer> MDAKy2(JComboBox jcb){
        ArrayList<Integer> ky2 = new ArrayList<Integer>();
        conn = dbc.Connect();
        try {
            ps = conn.prepareStatement("Select Count(*) SoLuong From MuonDoAn "
                    + "Where Year(Ngaymuon) = ? AND Month(Ngaymuon) Between '01' AND '06' Group By Tennguoimuon");
            ps.setString(1, jcb.getSelectedItem().toString());
            rs = ps.executeQuery();
            while(rs.next()){
                int soLuong = rs.getInt("SoLuong");
                ky2.add(soLuong);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ThongKe.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ky2;
    }
    public ArrayList<String> MDA(JComboBox jcb){
        ArrayList<String> ten = new ArrayList<String>();
        conn = dbc.Connect();
        try {
            ps = conn.prepareStatement("Select Tennguoimuon From MuonDoAn "
                    + "Where Year(Ngaymuon) = ? Group By Tennguoimuon");
            ps.setString(1, jcb.getSelectedItem().toString());
            rs = ps.executeQuery();
            while(rs.next()){
                String tenn = rs.getString("Tennguoimuon");
                ten.add(tenn);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ThongKe.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ten;
    }

}
