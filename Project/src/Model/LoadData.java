package Model;

import Control.DBConnection;
import Control.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class LoadData {

    DBConnection dbc = new DBConnection();
    Connection conn = null;
    public PreparedStatement ps = null;
    ResultSet rs = null;
    ResultSetMetaData rsmd = null;

    // Hiển thị dữ liệu database lên ComboBox
    public void loadCombobox(JComboBox jcb, String sql, String s) {
        conn = dbc.Connect();
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                jcb.addItem(rs.getString(s));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Loi");
            System.out.println("Loi: " + ex.getMessage());
        }
    }

    // Lấy dữ liệu database gốc đưa hết lên bảng
    public void loadDataTable(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb1, JComboBox jcb2,
             JTextField jtf1, JTextField jtf2, String sql) {
        conn = dbc.Connect();

        try {
            ps = conn.prepareStatement(sql);
            if (jcb1 != null) {
                ps.setString(1, jcb1.getSelectedItem().toString());
                if (jcb2 != null) {
                    ps.setString(2, jcb1.getSelectedItem().toString());
                }
            }
            if (jtf1 != null) {
                ps.setString(1, jtf1.getText());
                if (jtf2 != null) {
                    ps.setString(2, jtf2.getText());
                }
            }
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();

            int n = rsmd.getColumnCount();
            String[] arr = new String[n];

            for (int i = 0; i < n; i++) {

                arr[i] = rsmd.getColumnName(i + 1);
            }

            dtm.setColumnIdentifiers(arr);

            while (rs.next()) {

                for (int i = 0; i < n; i++) {

                    arr[i] = rs.getString(i + 1);
                }
                dtm.addRow(arr);
            }
        } catch (SQLException ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        jt.setModel(dtm);
        jsp.setViewportView(jt);
        
    }
    public void loadDataTableTK(JTable jt, DefaultTableModel dtm, JScrollPane jsp,
             JTextField jtf1, String sql) {
        conn = dbc.Connect();

        try {
            ps = conn.prepareStatement(sql);
            
            if (jtf1 != null) {
                ps.setString(1, jtf1.getText());
                ps.setString(2, jtf1.getText());
                ps.setString(3, jtf1.getText());
                ps.setString(4, jtf1.getText());
                ps.setString(5, jtf1.getText());
            }
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();

            int n = rsmd.getColumnCount();
            String[] arr = new String[n];

            for (int i = 0; i < n; i++) {

                arr[i] = rsmd.getColumnName(i + 1);
            }

            dtm.setColumnIdentifiers(arr);

            while (rs.next()) {

                for (int i = 0; i < n; i++) {

                    arr[i] = rs.getString(i + 1);
                }
                dtm.addRow(arr);
            }
        } catch (SQLException ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        jt.setModel(dtm);
        jsp.setViewportView(jt);
        
    }
 
 // Nap du lieu cho bang 
    public void loadData(JTable jt, DefaultTableModel dtm, JScrollPane jsp,
            JComboBox jcb1, JComboBox jcb2, JTextField jtf1, JTextField jtf2, String sql, Object[] arr) {

        conn = dbc.Connect();

        try {
            ps = conn.prepareStatement(sql);
            if (jcb1 != null) {
                ps.setString(1, jcb1.getSelectedItem().toString());
                if (jcb2 != null) {
                    ps.setString(2, jcb1.getSelectedItem().toString());
                }
            }
            if (jtf1 != null) {
                ps.setString(1, jtf1.getText());
                if (jtf2 != null) {
                    ps.setString(2, jtf2.getText());
                }
            }

            rs = ps.executeQuery();
            int n = arr.length;
            dtm.setColumnIdentifiers(arr);
            //dtm.addColumn(0, arr);
            int k = 1;
            while (rs.next()) {
                arr[0] = k;
                k++;
                for (int i = 1; i < n; i++) {

                    arr[i] = rs.getString(i);
                }
                dtm.addRow(arr);
            }
        } catch (SQLException ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        
        jt.setModel(dtm);
        jsp.setViewportView(jt);
        
        
    }
    
    public void loadDataTK(JTable jt, DefaultTableModel dtm, JScrollPane jsp,
            JComboBox jcb1, JComboBox jcb2, JTextField jtf1, JTextField jtf2, String sql, Object[] arr, int m) {

        conn = dbc.Connect();

        try {
            ps = conn.prepareStatement(sql);
            if (jcb1 != null) {
                for(int i = 1; i <= m; i++){
                    ps.setString(i, jcb1.getSelectedItem().toString()); 
                }
                
            }

            rs = ps.executeQuery();
            int n = arr.length;
            dtm.setColumnIdentifiers(arr);
            
            int k = 1;
            while (rs.next()) {
                arr[0] = k;
                k++;
                for (int i = 1; i < n; i++) {

                    arr[i] = rs.getString(i);
                }
                dtm.addRow(arr);
            }
        } catch (SQLException ex) {
            System.out.println("Loi: " + ex.getMessage());
        } 
        jt.setModel(dtm);
        jsp.setViewportView(jt);
    }
    
//        ==> Tạo 1 hàm nạp dữ liệu từ Database ==> Sử dụng nhiều lần. 
//    public void loadData(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb, String sql, Object[] arr) {
//
//        conn = dbc.Connect();
//
//        try {
//            ps = conn.prepareStatement(sql);
//
//            rs = ps.executeQuery();
//            rsmd = rs.getMetaData();
//
//            int n = arr.length;
//            dtm.setColumnIdentifiers(arr);
//            int k = 1;
//            while (rs.next()) {
//                arr[0] = k;
//                k++;
//                for (int i = 1; i < n; i++) {
//
//                    arr[i] = rs.getString(i);
//                }
//
//                dtm.addRow(arr);
//            }
//        } catch (SQLException ex) {
//            System.out.println("Loi: " + ex.getMessage());
//        }
//        jt.setModel(dtm);
//        jsp.setViewportView(jt);
//    }
    
    
    //    // Nạp dữ liệu vào bảng DSCB
//    public void loadDSCB(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb, String sql) {
//
//        conn = dbc.Connect();
//
//        try {
//            ps = conn.prepareStatement(sql);
//            ps.setString(1, jcb.getSelectedItem().toString());
//            rs = ps.executeQuery();
//            Object[] arr = {"STT", "Họ Và tên", "Số Điện Thoại", "Email", "Số Tài Khoản", "Mã Số Thuế", "Số CMND", "Bộ môn"};
//            int n = arr.length;
//            dtm.setColumnIdentifiers(arr);
//
//            int k = 1;
//            while (rs.next()) {
//                arr[0] = k;
//                k++;
//                for (int i = 1; i < n; i++) {
//
//                    arr[i] = rs.getString(i);
//                }
//                dtm.addRow(arr);
//            }
//        } catch (SQLException ex) {
//            System.out.println("Loi: " + ex.getMessage());
//        }
//        jt.setModel(dtm);
//        jsp.setViewportView(jt);
//    }
//
//    // Nạp dữ liệu vào bảng DS Đồ Án
//    public void loadDSDoAn(JTable jt, DefaultTableModel dtm, JScrollPane jsp, JComboBox jcb) {
//
//        conn = dbc.Connect();
//        String sql = "Select Madoan, Tendetai, Hotensv, HotenGV, TenGVPB, Kyhoc"
//                + " From Doan DA, GiangVien GV, SinhVien SV"
//                + " Where DA.MaGVHD = GV.MaGV"
//                + " AND DA.Masv = SV.Masv AND Bomon = ?";
//        try {
//            ps = conn.prepareStatement(sql);
//            ps.setString(1, jcb.getSelectedItem().toString());
//            rs = ps.executeQuery();
//            rsmd = rs.getMetaData();
//
//            int n = rsmd.getColumnCount();
//            Object[] arr = {"STT", "Mã Đồ Án", "Tên Đề Tài", "Họ Tên SV", "GVHD", "GVPB", "Kỳ Học"};
//
//            dtm.setColumnIdentifiers(arr);
//
//            int k = 1;
//            while (rs.next()) {
//                arr[0] = k;
//                k++;
//                for (int i = 1; i < n; i++) {
//
//                    arr[i] = rs.getString(i);
//                }
//                dtm.addRow(arr);
//            }
//        } catch (SQLException ex) {
//            System.out.println("Loi: " + ex.getMessage());
//        }
//        jt.setModel(dtm);
//        jsp.setViewportView(jt);
//    }
//


    
//    public void loadDSMuonDA(JTable jt, DefaultTableModel dtm, JScrollPane jsp) {
//
//        conn = dbc.Connect();
//        String sql = "Select DA.Madoan, Tendetai, Hotensv, HotenGV, MaGVPB, Tennguoimuon, Ngaymuon"
//                + " From MuonDoAn MDA, GiangVien GV, SinhVien SV, DoAn DA"
//                + " Where MDA.MaDoAn = DA.Madoan AND DA.MaGVHD = GV.MaGV"
//                + " AND DA.Masv = SV.Masv";
//        try {
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsmd = rs.getMetaData();
//
//            int n = rsmd.getColumnCount();
//            String[] arr = {"Mã Đồ Án", "Tên Đề Tài", "Tên SV", "GVHD", "GVPB", "Tên Người Mượn", "Ngày Mượn"};
//
//            dtm.setColumnIdentifiers(arr);
//
//            while (rs.next()) {
//
//                for (int i = 0; i < n; i++) {
//
//                    arr[i] = rs.getString(i + 1);
//                }
//                dtm.addRow(arr);
//            }
//        } catch (SQLException ex) {
//            System.out.println("Loi: " + ex.getMessage());
//        }
//        jt.setModel(dtm);
//        jsp.setViewportView(jt);
//    }
//
//    public void loadDAThanhLy(JTable jt, DefaultTableModel dtm, JScrollPane jsp) {
//
//        conn = dbc.Connect();
//        String sql = "Select DA.Madoan, Tendetai, Hotensv, HotenGV, MaGVPB, Kyhoc"
//                + " From GiangVien GV, SinhVien SV, DoAn DA"
//                + " Where DA.MaGVHD = GV.MaGV"
//                + " AND DA.Masv = SV.Masv";
//       try {
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsmd = rs.getMetaData();
//
//            int n = rsmd.getColumnCount();
//            String[] arr = {"Mã Đồ Án", "Tên Đề Tài", "Tên SV", "GVHD", "GVPB", "Kỳ học"};
//
//            dtm.setColumnIdentifiers(arr);
//
//            while (rs.next()) {
//
//                for (int i = 0; i < n; i++) {
//
//                    arr[i] = rs.getString(i + 1);
//                }
//                dtm.addRow(arr);
//            }
//        } catch (SQLException ex) {
//            System.out.println("Loi: " + ex.getMessage());
//        }
//        jt.setModel(dtm);
//        jsp.setViewportView(jt);
//    }
//
//    public void loadMuonDAQuaHan(JTable jt, DefaultTableModel dtm, JScrollPane jsp) {
//
//        conn = dbc.Connect();
//        String sql = "Select MaGV, HotenGV, Hotensv, HotenGV, MaGVPB, Kyhoc"
//                + " From MuonDoAn MDA, GiangVien GV, SinhVien SV, DoAn DA"
//                + " Where MDA.MaDoAn = DA.Madoan AND DA.MaGVHD = GV.MaGV"
//                + " AND DA.Masv = SV.Masv";
//        try {
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsmd = rs.getMetaData();
//
//            int n = rsmd.getColumnCount();
//            String[] arr = {"Mã Giáo Viên", "Họ Tên GV", "Tên Sách", "Tên Tác Giả", "Ngày Mượn", "Ngày Trả", "Phạt"};
//
//            dtm.setColumnIdentifiers(arr);
//
//            while (rs.next()) {
//
//                for (int i = 0; i < n; i++) {
//
//                    arr[i] = rs.getString(i + 1);
//                }
//                dtm.addRow(arr);
//            }
//        } catch (SQLException ex) {
//            System.out.println("Loi: " + ex.getMessage());
//        }
//        jt.setModel(dtm);
//        jsp.setViewportView(jt);
//    }
//
//    public void loadMuonDADungHan(JTable jt, DefaultTableModel dtm, JScrollPane jsp) {
//
//        conn = dbc.Connect();
//        String sql = "Select MaGV, HotenGV, Hotensv, HotenGV, MaGVPB, Kyhoc"
//                + " From MuonDoAn MDA, GiangVien GV, SinhVien SV, DoAn DA"
//                + " Where MDA.MaDoAn = DA.Madoan AND DA.MaGVHD = GV.MaGV"
//                + " AND DA.Masv = SV.Masv";
//        try {
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsmd = rs.getMetaData();
//
//            int n = rsmd.getColumnCount();
//            String[] arr = {"Mã Giáo Viên", "Họ Tên GV", "Tên Sách", "Tên Tác Giả", "Ngày Mượn", "Ngày Trả", "Tình Trạng"};
//
//            dtm.setColumnIdentifiers(arr);
//
//            while (rs.next()) {
//
//                for (int i = 0; i < n; i++) {
//
//                    arr[i] = rs.getString(i + 1);
//                }
//                dtm.addRow(arr);
//            }
//        } catch (SQLException ex) {
//            System.out.println("Loi: " + ex.getMessage());
//        }
//        jt.setModel(dtm);
//        jsp.setViewportView(jt);
//    }
}
