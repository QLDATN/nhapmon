
package Model;

public class Info_MDA {
    
    private String maMuonDA;
    private String maDoAn;
    private String tenNguoiMuon;
    private String dienThoai;
    private String email;
    private String ngayMuon;
    private String ngayTra;
    private String ngayTraDuKien;
    private String tinhTrang;
    private String baoLanh;
    private String ghiChu;

    public Info_MDA() {
        
    }

    public Info_MDA(String maMuonDA, String maDoAn, String tenNguoiMuon,
            String dienThoai, String email, String ngayMuon, String ngayTra,
            String ngayTraDuKien, String tinhTrang, String baoLanh, String ghiChu) {
        this.maMuonDA = maMuonDA;
        this.maDoAn = maDoAn;
        this.tenNguoiMuon = tenNguoiMuon;
        this.dienThoai = dienThoai;
        this.email = email;
        this.ngayMuon = ngayMuon;
        this.ngayTra = ngayTra;
        this.ngayTraDuKien = ngayTraDuKien;
        this.tinhTrang = tinhTrang;
        this.baoLanh = baoLanh;
        this.ghiChu = ghiChu;
    }

    public String getMaMuonDA() {
        return maMuonDA;
    }

    public void setMaMuonDA(String maMuonDA) {
        this.maMuonDA = maMuonDA;
    }

    public String getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(String maDoAn) {
        this.maDoAn = maDoAn;
    }

    public String getTenNguoiMuon() {
        return tenNguoiMuon;
    }

    public void setTenNguoiMuon(String tenNguoiMuon) {
        this.tenNguoiMuon = tenNguoiMuon;
    }

    public String getDienThoai() {
        return dienThoai;
    }

    public void setDienThoai(String dienThoai) {
        this.dienThoai = dienThoai;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNgayMuon() {
        return ngayMuon;
    }

    public void setNgayMuon(String ngayMuon) {
        this.ngayMuon = ngayMuon;
    }

    public String getNgayTra() {
        return ngayTra;
    }

    public void setNgayTra(String ngayTra) {
        this.ngayTra = ngayTra;
    }

    public String getNgayTraDuKien() {
        return ngayTraDuKien;
    }

    public void setNgayTraDuKien(String ngayTraDuKien) {
        this.ngayTraDuKien = ngayTraDuKien;
    }

    public String getTinhTrang() {
        return tinhTrang;
    }

    public void setTinhTrang(String tinhTrang) {
        this.tinhTrang = tinhTrang;
    }

    public String getBaoLanh() {
        return baoLanh;
    }

    public void setBaoLanh(String baoLanh) {
        this.baoLanh = baoLanh;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

   
}
