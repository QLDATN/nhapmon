
package Model;

public class Info_DA {
    private String maDoAn;
    private String tenDeTai;
    private String maSV;
    private String tenSV;
    private String dienThoaiSV;
    private String emailSV;
    private String tenLop;
    private String khoa;
    private String kyHoc;
    private String namHoc;
    private String maGVHD;
    private String tenGVHD;
    private String maGVPB;
    private String tenGVPB;
    private String ngayBaoVe;
    private String ngayThanhLy;
    private String ghiChu;

    public Info_DA(){
        
    }
    
    public Info_DA(String maDoAn, String tenDeTai, String maSV, String tenSV,
            String dienThoaiSV, String emailSV, String tenLop, String khoa,
            String kyHoc, String namHoc, String maGVHD, String tenGVHD,
            String maGVPB, String tenGVPB, String ngayBaoVe, String ngayThanhLy, String ghiChu) {
        this.maDoAn = maDoAn;
        this.tenDeTai = tenDeTai;
        this.maSV = maSV;
        this.tenSV = tenSV;
        this.dienThoaiSV = dienThoaiSV;
        this.emailSV = emailSV;
        this.tenLop = tenLop;
        this.khoa = khoa;
        this.kyHoc = kyHoc;
        this.namHoc = namHoc;
        this.maGVHD = maGVHD;
        this.tenGVHD = tenGVHD;
        this.maGVPB = maGVPB;
        this.tenGVPB = tenGVPB;
        this.ngayBaoVe = ngayBaoVe;
        this.ngayThanhLy = ngayThanhLy;
        this.ghiChu = ghiChu;
    }

    public String getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(String maDoAn) {
        this.maDoAn = maDoAn;
    }

    public String getTenDeTai() {
        return tenDeTai;
    }

    public void setTenDeTai(String tenDeTai) {
        this.tenDeTai = tenDeTai;
    }

    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }

    public String getDienThoaiSV() {
        return dienThoaiSV;
    }

    public void setDienThoaiSV(String dienThoaiSV) {
        this.dienThoaiSV = dienThoaiSV;
    }

    public String getEmailSV() {
        return emailSV;
    }

    public void setEmailSV(String emailSV) {
        this.emailSV = emailSV;
    }

    public String getTenLop() {
        return tenLop;
    }

    public void setTenLop(String tenLop) {
        this.tenLop = tenLop;
    }

    public String getKhoa() {
        return khoa;
    }

    public void setKhoa(String khoa) {
        this.khoa = khoa;
    }

    public String getKyHoc() {
        return kyHoc;
    }

    public void setKyHoc(String kyHoc) {
        this.kyHoc = kyHoc;
    }

    public String getNamHoc() {
        return namHoc;
    }

    public void setNamHoc(String namHoc) {
        this.namHoc = namHoc;
    }

    public String getMaGVHD() {
        return maGVHD;
    }

    public void setMaGVHD(String maGVHD) {
        this.maGVHD = maGVHD;
    }

    public String getTenGVHD() {
        return tenGVHD;
    }

    public void setTenGVHD(String tenGVHD) {
        this.tenGVHD = tenGVHD;
    }

    public String getMaGVPB() {
        return maGVPB;
    }

    public void setMaGVPB(String maGVPB) {
        this.maGVPB = maGVPB;
    }

    public String getTenGVPB() {
        return tenGVPB;
    }

    public void setTenGVPB(String tenGVPB) {
        this.tenGVPB = tenGVPB;
    }

    public String getNgayBaoVe() {
        return ngayBaoVe;
    }

    public void setNgayBaoVe(String ngayBaoVe) {
        this.ngayBaoVe = ngayBaoVe;
    }

    public String getNgayThanhLy() {
        return ngayThanhLy;
    }

    public void setNgayThanhLy(String ngayThanhLy) {
        this.ngayThanhLy = ngayThanhLy;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    
    
}
