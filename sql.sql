USE [quanlydoan01]
GO
/****** Object:  User [dbQLDA]    Script Date: 16/12/2017 09:48:34 ******/
CREATE USER [dbQLDA] FOR LOGIN [dbQLDA] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [hiepvd_mssql]    Script Date: 16/12/2017 09:48:34 ******/
CREATE USER [hiepvd_mssql] FOR LOGIN [hiepvd_mssql] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [project1]    Script Date: 16/12/2017 09:48:34 ******/
CREATE USER [project1] FOR LOGIN [project1] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[GiangVien]    Script Date: 16/12/2017 09:48:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiangVien](
	[MaGV] [nvarchar](30) NOT NULL,
	[HotenGV] [nvarchar](30) NULL,
	[Gioitinh] [nvarchar](10) NULL,
	[Ngaysinh] [date] NULL,
	[Ngayvetruong] [nvarchar](20) NULL,
	[Ngayvaodang] [nvarchar](20) NULL,
	[Chucdanh] [nvarchar](10) NULL,
	[Chucvu] [nvarchar](20) NULL,
	[Dienthoai] [nvarchar](12) NULL,
	[Email] [nvarchar](30) NULL,
	[Diachi] [nvarchar](50) NULL,
	[Bomon] [nvarchar](20) NULL,
	[Chibo] [nvarchar](20) NULL,
	[Sotaikhoan] [nvarchar](20) NULL,
	[Masothue] [nvarchar](20) NULL,
	[SoCMND] [nvarchar](20) NULL,
	[Ghichu] [nvarchar](30) NULL,
 CONSTRAINT [PK_GiangVien] PRIMARY KEY CLUSTERED 
(
	[MaGV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[GVNH]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create View [dbo].[GVNH] AS Select Bomon, Ghichu From GiangVien Where Ghichu = 'NGHI HUU'
GO
/****** Object:  View [dbo].[GVONN]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create View [dbo].[GVONN] AS Select Bomon, Ghichu From GiangVien Where Ghichu = 'NCS NN' 
GO
/****** Object:  View [dbo].[GVDCT]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create View [dbo].[GVDCT] AS Select Bomon, Ghichu From GiangVien Where Ghichu = 'DANG CONG TAC'
GO
/****** Object:  Table [dbo].[MuonDoAn]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuonDoAn](
	[Muon-doan] [nvarchar](10) NOT NULL,
	[MaDoAn] [nvarchar](10) NOT NULL,
	[Tennguoimuon] [nvarchar](30) NULL,
	[Dienthoai] [nvarchar](12) NULL,
	[Email] [nvarchar](30) NULL,
	[Ngaymuon] [date] NULL,
	[Ngaytra] [date] NULL,
	[Ngaytradukien] [date] NULL,
	[Tinhtrang] [nvarchar](20) NULL,
	[Baolanh] [nvarchar](30) NULL,
	[Ghichu] [nvarchar](30) NULL,
 CONSTRAINT [PK__MuonDoAn] PRIMARY KEY CLUSTERED 
(
	[Muon-doan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[name]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create View [dbo].[name] AS Select Tennguoimuon From MuonDoAn Where Month(Ngaymuon) Between '01' AND '06' AND Year(Ngaymuon) = '2014' Group By Tennguoimuon
GO
/****** Object:  View [dbo].[name1]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create View [dbo].[name1] AS Select Tennguoimuon, Ngaymuon From MuonDoAn Where Month(Ngaymuon) Between '01' AND '06' AND Year(Ngaymuon) = '2014' Group By Tennguoimuon, Ngaymuon
GO
/****** Object:  View [dbo].[name2]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create View [dbo].[name2] AS Select Tennguoimuon, Ngaymuon From MuonDoAn Where Month(Ngaymuon) Between '07' AND '12' AND Year(Ngaymuon) = '2014' Group By Tennguoimuon, Ngaymuon
GO
/****** Object:  Table [dbo].[DoAn]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoAn](
	[Madoan] [nvarchar](10) NOT NULL,
	[Tendetai] [nvarchar](50) NULL,
	[Masv] [nvarchar](10) NOT NULL,
	[TenSV] [nvarchar](30) NULL,
	[DienThoai] [nvarchar](11) NULL,
	[Email] [nvarchar](30) NULL,
	[TenLop] [nvarchar](20) NULL,
	[Khoa] [nvarchar](30) NULL,
	[Kyhoc] [nvarchar](10) NULL,
	[Namhoc] [nvarchar](10) NULL,
	[MaGVHD] [nvarchar](30) NOT NULL,
	[TenGVHD] [nvarchar](30) NULL,
	[MaGVPB] [nvarchar](30) NULL,
	[TenGVPB] [nvarchar](30) NULL,
	[Ngaybaove] [date] NULL,
	[Ngaythanhly] [date] NULL,
	[Ghichu] [nvarchar](30) NULL,
 CONSTRAINT [PK__DoAn] PRIMARY KEY CLUSTERED 
(
	[Madoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Login]    Script Date: 16/12/2017 09:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Login](
	[TenDN] [nvarchar](50) NOT NULL,
	[MatKhau] [nvarchar](50) NOT NULL,
	[HoTen] [nvarchar](50) NULL,
	[GioiTinh] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[PhanQuyen] [nvarchar](50) NOT NULL,
	[TrangThai] [nvarchar](50) NULL,
 CONSTRAINT [PK_Login] PRIMARY KEY CLUSTERED 
(
	[TenDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA01', N'Game1', N'SV01', N'Nguyễn Văn A', N'01667470674', N'anv@gmail.com', N'CNTT2-1', N'Công Nghệ Thông Tin', N'2', N'2014', N'GV01', N'Lê Thị Lan', N'GV04', N'Phạm Văn Tuấn', CAST(N'2015-04-04' AS Date), CAST(N'2020-04-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA02', N'IOT2', N'SV02', N'Nguyễn Văn B', N'01667470674', N'bnv@gmail.com', N'CNTT2-2', N'Công Nghệ Thông Tin', N'3', N'2011', N'GV02', N'Nguyễn Văn Nam', N'GV16', N'Văn Nam', CAST(N'2012-07-04' AS Date), CAST(N'2017-07-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA03', N'Game2', N'SV03', N'Nguyễn Văn C', N'01667470674', N'cnv@gmail.com', N'CNTT2-3', N'Công Nghệ Thông Tin', N'3', N'2011', N'GV10', N'Vũ Thị Thu Trang', N'GV09', N'Trịnh Tuấn Đạt', CAST(N'2012-08-04' AS Date), CAST(N'2017-08-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA04', N'Game1', N'SV04', N'Nguyễn Văn A', N'01667470674', N'anv@gmail.com', N'CNTT2-1', N'Công Nghệ Thông Tin', N'1', N'2013', N'GV02', N'Nguyễn Văn Nam', N'GV04', N'Phạm Văn Tuấn', CAST(N'2013-12-04' AS Date), CAST(N'2018-12-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA05', N'Game1', N'SV05', N'Nguyễn Văn A', N'01667470674', N'anv@gmail.com', N'CNTT2-1', N'Công Nghệ Thông Tin', N'2', N'2013', N'GV01', N'Lê Thị Lan', N'GV11', N'Nguyễn Thị Hương Giang', CAST(N'2014-04-04' AS Date), CAST(N'2019-04-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA06', N'AI2', N'SV06', N'Nguyễn Văn F', N'0987654321', N'fnv@gmail.com', N'CNTT2-4', N'Công Nghệ Thông Tin', N'2', N'2010', N'GV06', N'Đặng Văn Cường', N'GV04', N'Phạm Văn Tuấn', CAST(N'2010-03-15' AS Date), CAST(N'2015-03-30' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA07', N'Game2', N'SV07', N'Nguyễn Văn C', N'01667470674', N'cnv@gmail.com', N'CNTT2-3', N'Công Nghệ Thông Tin', N'3', N'2013', N'GV10', N'Vũ Thị Thu Trang', N'GV09', N'Trịnh Tuấn Đạt', CAST(N'2013-08-04' AS Date), CAST(N'2018-08-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA08', N'MT2', N'SV08', N'Nguyễn Văn G', N'01667470674', N'bnv@gmail.com', N'CNTT2-3', N'Công Nghệ Thông Tin', N'1', N'2009', N'GV20', N'Nguyễn Kim Khánh', N'GV16', N'Văn Nam', CAST(N'2009-12-04' AS Date), CAST(N'2014-12-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA09', N'AI1', N'SV09', N'Nguyễn Văn H', N'01667470674', N'bnv@gmail.com', N'CNTT2-2', N'Công Nghệ Thông Tin', N'1', N'2008', N'GV01', N'Lê Thị Lan', N'GV03', N'Lê Thị Hoa', CAST(N'2008-12-04' AS Date), CAST(N'2013-12-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA10', N'IOT1', N'SV10', N'Nguyễn Văn I', N'01667470674', N'bnv@gmail.com', N'CNTT1-2', N'Công Nghệ Thông Tin', N'2', N'2014', N'GV07', N'Vũ Đức Hiệp', N'GV16', N'Văn Nam', CAST(N'2015-03-22' AS Date), CAST(N'2020-03-22' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA11', N'AI1', N'SV11', N'Nguyễn Văn K', N'01667470674', N'bnv@gmail.com', N'CNTT2-2', N'Công Nghệ Thông Tin', N'1', N'2016', N'GV14', N'Trịnh Thành Trung', N'GV19', N'Mai Văn Mạnh', CAST(N'2016-12-04' AS Date), CAST(N'2021-12-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA12', N'AI1', N'SV12', N'Nguyễn Văn M', N'01667470674', N'bnv@gmail.com', N'CNTT2-2', N'Công Nghệ Thông Tin', N'1', N'2015', N'GV12', N'Trần Thị Hương Hoa', N'GV16', N'Văn Nam', CAST(N'2015-12-04' AS Date), CAST(N'2020-12-14' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA13', N'MT1', N'SV13', N'Nguyễn Văn N', N'01667470674', N'bnv@gmail.com', N'CNTT2-2', N'Công Nghệ Thông Tin', N'1', N'2011', N'GV22', N'Nguyễn Văn Tiến', N'GV06', N'Đặng Văn Cường', CAST(N'2011-11-15' AS Date), CAST(N'2016-11-15' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA14', N'MT2', N'SV114', N'Nguyễn Văn O', N'01667470674', N'bnv@gmail.com', N'CNTT1-3', N'Công Nghệ Thông Tin', N'1', N'2012', N'GV20', N'Nguyễn Kim Khánh', N'GV06', N'Đặng Văn Cường', CAST(N'2012-11-15' AS Date), CAST(N'2017-11-15' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA15', N'Game3', N'SV15', N'Nguyễn Văn N', N'01667470674', N'bnv@gmail.com', N'CNTT1-3', N'Công Nghệ Thông Tin', N'1', N'2009', N'GV14', N'Trịnh Thành Trung', N'GV01', N'Lê Thị Lan', CAST(N'2009-11-15' AS Date), CAST(N'2014-11-15' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA16', N'IOT1', N'SV16', N'Nguyễn Văn S', N'01667470674', N'bnv@gmail.com', N'CNTT1-1', N'Công Nghệ Thông Tin', N'2', N'2007', N'GV14', N'Trịnh Thành Trung', N'GV01', N'Lê Thị Lan', CAST(N'2007-12-23' AS Date), CAST(N'2012-12-23' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA17', N'AI2', N'SV17', N'Nguyễn Văn P', N'01667470674', N'bnv@gmail.com', N'CNTT1-1', N'Công Nghệ Thông Tin', N'2', N'2010', N'GV30', N'Nguyễn Kim', N'GV02', N'Nguyễn Văn Nam', CAST(N'2011-04-23' AS Date), CAST(N'2016-12-23' AS Date), N'')
INSERT [dbo].[DoAn] ([Madoan], [Tendetai], [Masv], [TenSV], [DienThoai], [Email], [TenLop], [Khoa], [Kyhoc], [Namhoc], [MaGVHD], [TenGVHD], [MaGVPB], [TenGVPB], [Ngaybaove], [Ngaythanhly], [Ghichu]) VALUES (N'DA18', N'Game1', N'SV18', N'Nguyễn Văn Q', N'01667470674', N'anv@gmail.com', N'CNTT2-1', N'Công Nghệ Thông Tin', N'1', N'2012', N'GV02', N'Nguyễn Văn Nam', N'GV04', N'Phạm Văn Tuấn', CAST(N'2012-12-04' AS Date), CAST(N'2017-12-14' AS Date), N'')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV01', N'Lê Thị Lan', N'Nữ', CAST(N'1970-12-25' AS Date), N'2010-01-11', N'2011-03-04', N'PGS.TS', N'GV', N'01661234567', N'lanlt@gmial.com', N'Hai Bà Trưng - Hà Nội', N'CNPM', N'CNPM1', N'12355', N'55321', N'123466333', N'NCS NN')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV02', N'Nguyễn Văn Nam', N'Nam', CAST(N'1965-12-01' AS Date), N'2005-01-01', N'2004-01-02', N'GS', N'TBM', N'01661234568', N'namnv@gmial.com', N'Hoàng Mai - Hà Nội', N'ATTT', N'ATTT2', N'12356', N'65321', N'123467789', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV03', N'Lê Thị Hoa', N'Nữ', CAST(N'1975-08-20' AS Date), N'2000-10-09', N'2002-12-12', N'TS', N'PBM', N'1661234569', N'hoalt@gmial.com', N'Đống Đa - Hà Nội', N'CNPM', N'CNPM2', N'12357', N'75321', N'123468', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV04', N'Phạm Văn Tuấn', N'Nam', CAST(N'1990-10-30' AS Date), N'2015-10-30', N'2017-04-15', N'KS', N'GV', N'01661234570', N'tuanpv@gmial.com', N'Thanh Xuân - Hà Nội', N'HTTT', N'HTTT2', N'12358', N'85321', N'123469222', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV05', N'Nguyễn Hữu Vinh', N'Nam', CAST(N'1990-12-02' AS Date), N'2015-10-30', N'2017-02-09', N'KS', N'GV', N'01661234571', N'vinhnh@gmial.com', N'Quốc Oai - Hà Nội', N'KHMT', N'KHMT2', N'12359', N'95321', N'123470111', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV06', N'Đặng Văn Cường', N'Nam', CAST(N'1985-01-01' AS Date), N'2010-10-10', N'', N'TS', N'PBM', N'01661234572', N'cuongdv@gmial.com', N'Cầu Giấy - Hà Nội', N'KHMT', N'KHMT1', N'12360', N'60321', N'123471666', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV07', N'Vũ Đức Hiệp', N'Nam', CAST(N'1986-02-02' AS Date), N'2013-12-16', N'', N'TS', N'PBM', N'01661234573', N'hiepvd@gmial.com', N'Từ Liêm - Hà Nội', N'ATTT', N'ATTT1', N'12361', N'61321', N'123472999', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV08', N'Nguyễn Hồng Phương', N'Nam', CAST(N'1985-03-03' AS Date), N'2010-10-18', N'2012-07-02', N'TS', N'GV', N'1661234574', N'phuongnh@gmial.com', N'Hoàn Kiếm - Hà Nội', N'HTTT', N'HTTT1', N'12362', N'62321', N'123473', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV09', N'Trịnh Tuấn Đạt', N'Nam', CAST(N'1980-04-04' AS Date), N'2010-04-16', N'2012-11-12', N'TS', N'GV', N'01661234575', N'dattt@gmial.com', N'Ba Đình - Hà Nội', N'CNPM', N'CNPM1', N'12363', N'63321', N'123474333', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV10', N'Vũ Thị Thu Trang', N'Nữ', CAST(N'1983-05-05' AS Date), N'2011-05-05', N'2013-05-05', N'TS', N'GV', N'964268151', N'trangvtt@gmail.com', N'Hà Nội', N'CNPM', N'CNPM2', N'12349', N'94321', N'123460', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV11', N'Nguyễn Thị Hương Giang', N'Nữ', CAST(N'1981-06-06' AS Date), N'2012-06-06', N'2014-06-06', N'TS', N'PBM', N'964268152', N'giangnth@gmail.com', N'Hà Nội', N'CNPM', N'CNPM1', N'12350', N'50321', N'123461', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV12', N'Trần Thị Hương Hoa', N'Nữ', CAST(N'1945-07-07' AS Date), N'1960-07-07', N'1965-07-07', N'GS', N'TBM', N'964268153', N'hoatth@gmail.com', N'Hà Nội', N'KHMT', N'KHMT1', N'12351', N'51321', N'123462', N'NGHI HUU')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV13', N'Quỳnh Mai', N'Nữ', CAST(N'1975-08-08' AS Date), N'2000-08-08', N'2005-08-08', N'PGS.TS', N'PBM', N'0964268154', N'maibtt@gmail.com', N'Hà Nội', N'KHMT', N'KHMT2', N'12352', N'52321', N'123463111', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV14', N'Trịnh Thành Trung', N'Nam', CAST(N'1977-09-09' AS Date), N'2004-09-09', N'2006-09-09', N'TS', N'GV', N'964268155', N'trungtt@gmail.com', N'Hà Nội', N'CNPM', N'CNPM2', N'12353', N'53321', N'123464', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV15', N'Đinh Viết Sang', N'Nam', CAST(N'1981-09-10' AS Date), N'2006-09-10', N'2008-09-10', N'ThS', N'GV', N'964268156', N'sangdv@gmail.com', N'Hà Nội', N'KHMT', N'KHMT1', N'12354', N'54321', N'123465', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV16', N'Văn Nam', N'Nam', CAST(N'1985-12-01' AS Date), N'2012-01-01', N'2014-01-02', N'ThS', N'GV', N'1661234568', N'namv@gmial.com', N'Hoàng Mai - Hà Nội', N'KHMT', N'KHMT2', N'12356', N'65321', N'123467', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV17', N'Vũ Thị Hương', N'Nữ', CAST(N'1974-08-08' AS Date), N'2000-11-02', N'2003-03-26', N'GS', N'VT', N'0123456789', N'huongvt@gmail.com', N'Thái Bình', N'CNPM', N'CNPM1', N'3523', N'4534', N'533541122', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV19', N'Mai Văn Mạnh', N'Nam', CAST(N'1985-12-01' AS Date), N'2012-01-01', N'2014-01-02', N'TS', N'GV', N'01661234568', N'namv@gmial.com', N'Hoàng Mai - Hà Nội', N'CNPM', N'CNPM2', N'12356', N'65321', N'123467890', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV20', N'Nguyễn Kim Khánh', N'Nam', CAST(N'1978-08-08' AS Date), N'2014-11-02', N'2015-03-09', N'TS', N'TBM', N'09876543231', N'khanknk@gmail.com', N'Hà Nội', N'KTMT', N'KTMT1', N'3523', N'4534', N'533545334', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV21', N' Quỳnh Mai', N'Nữ', CAST(N'1975-08-08' AS Date), N'2000-08-08', N'2008-01-01', N'GS', N'TBM', N'964268154', N'maibtt@gmail.com', N'Hà Nội', N'KHMT', N'KHMT2', N'12352', N'52321', N'123463', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV22', N'Nguyễn Văn Tiến', N'Nam', CAST(N'1988-08-08' AS Date), N'2014-11-02', N'2015-03-09', N'ThS', N'GV', N'0123456789', N'tiennv@gmail.com', N'Thái Bình', N'KTMT', N'KTMT2', N'3523', N'4534', N'533545234', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV25', N'Văn Nam', N'Nam', CAST(N'1985-12-01' AS Date), N'2012-01-01', N'2014-01-02', N'ThS', N'GV', N'1661234568', N'namv@gmial.com', N'Hoàng Mai - Hà Nội', N'KHMT', N'KHMT2', N'12356', N'65321', N'123467', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV29', N'Nguyễn Như Kim', N'Nữ', CAST(N'1988-08-08' AS Date), N'2014-11-02', N'', N'KS', N'GV', N'0123456789', N'kimnn@gmail.com', N'HY', N'ATTT', N'ATTT2', N'3523', N'4534', N'533541122', N'NCS NN')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV30', N'Nguyễn Kim', N'Nam', CAST(N'1977-08-08' AS Date), N'2003-11-02', N'2005-03-26', N'PGS.TS', N'TBM', N'0123456789', N'kimn@gmail.com', N'HY', N'ATTT', N'ATTT1', N'3523', N'4534', N'533541122', N'DANG CONG TAC')
INSERT [dbo].[GiangVien] ([MaGV], [HotenGV], [Gioitinh], [Ngaysinh], [Ngayvetruong], [Ngayvaodang], [Chucdanh], [Chucvu], [Dienthoai], [Email], [Diachi], [Bomon], [Chibo], [Sotaikhoan], [Masothue], [SoCMND], [Ghichu]) VALUES (N'GV31', N'Nguyễn Như Khánh', N'Nữ', CAST(N'1991-08-08' AS Date), N'2016-11-02', N'', N'KS', N'GV', N'09876543231', N'khanknk@gmail.com', N'Hà Nội', N'KTMT', N'KTMT1', N'3523', N'4534', N'533545334', N'NCS NN')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'a', N'a', N'a', N'Null', N'a', N'Member', N'Hoạt Động')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'admin', N'admin', N'Admin', N'Nam', N'hellobaby@gmail.com', N'Admin', N'Hoạt Động')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'admin2', N'admin', N'', N'Nam', N'hellobaby@gmail.com', N'Admin', N'Hoạt Động')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'justin', N'timberlake', N'Justin Timberlake', N'Nam', NULL, N'Member', N'Tạm Ngưng')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'lelan', N'123456', N'Lê Thị Lan', N'Nữ', NULL, N'Member', N'Hoạt Động')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'member01', N'1', N'Trần Văn Công', N'Nam', N'sfgg@sdaf.com', N'Member', N'Hoạt Động')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'phamvantuan', N'123', N'Phạm Văn Tuấn', N'Nam', NULL, N'Admin', N'Hoạt Động')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'sanchez', N'arsenal', N'Hưng Chez', N'Nam', NULL, N'Member', N'Tạm Ngưng')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'tes', N'admin', N'Test', N'Nam', N'', N'Member', N'Hoạt Động')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'vinh12', N'vinh', N'Nguyễn Hữu Vinh', N'Null', N'test', N'Member', N'Hoạt Động')
INSERT [dbo].[Login] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Email], [PhanQuyen], [TrangThai]) VALUES (N'vuduchiep', N'a', N'Vũ Đức Hiệp', N'Nam', N'hiepvd@gmail.com', N'Admin', N'Hoạt Động')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA01', N'DA06', N'Lê Thị Lan', N'01661234567', N'lanlt@gmial.com', CAST(N'2013-04-10' AS Date), CAST(N'2013-07-10' AS Date), CAST(N'2013-08-10' AS Date), N'Bình thường', N'200000', N'Dung Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA02', N'DA12', N'Trần Thị Hương Hoa', N'01661234567', N'vinhnh@gmail.com', CAST(N'2015-04-10' AS Date), CAST(N'2015-08-20' AS Date), CAST(N'2015-08-10' AS Date), N'Bình thường', N'100000', N'Qua Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA03', N'DA04', N'Trịnh Thành Trung', N'01661234567', N'namnv@gmail.com', CAST(N'2016-04-10' AS Date), CAST(N'2016-08-10' AS Date), CAST(N'2016-08-10' AS Date), N'Bình thường', N'150000', N'Dung Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA04', N'DA03', N'Vũ Thị Thu Trang', N'01661234567', N'trangvtt@gmail.com', CAST(N'2013-08-10' AS Date), CAST(N'2013-12-10' AS Date), CAST(N'2013-11-10' AS Date), N'Bị hư', N'200000', N'Qua Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA05', N'DA10', N'Vũ Đức Hiệp', N'0987654321', N'hiepvd@gmail.com', CAST(N'2014-02-10' AS Date), CAST(N'2014-05-15' AS Date), CAST(N'2014-05-10' AS Date), N'Bình thường', N'200000', N'Qua Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA06', N'DA02', N'Nguyễn Văn Nam', N'01661234568', N'namnv@gmial.com', CAST(N'2013-06-20' AS Date), CAST(N'2013-11-20' AS Date), CAST(N'2013-11-30' AS Date), N'Bình thường', N'300000', N'Dung Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA07', N'DA09', N'Nguyễn Văn Tiến', N'01661234568', N'khanhnk@gmial.com', CAST(N'2011-07-20' AS Date), CAST(N'2011-11-20' AS Date), CAST(N'2011-11-30' AS Date), N'Bị hư', N'250000', N'Dung Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA08', N'DA11', N'Nguyễn Kim Khánh', N'01661234568', N'khanhnk@gmial.com', CAST(N'2012-07-20' AS Date), CAST(N'2012-11-20' AS Date), CAST(N'2012-11-30' AS Date), N'Bình thường', N'250000', N'Dung Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA09', N'DA13', N'Lê Thị Lan', N'01661234568', N'khanhnk@gmial.com', CAST(N'2013-12-20' AS Date), CAST(N'2014-05-20' AS Date), CAST(N'2014-05-30' AS Date), N'Bình thường', N'250000', N'Dung Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA10', N'DA05', N'Lê Thị Lan', N'01661234568', N'khanhnk@gmial.com', CAST(N'2014-07-20' AS Date), CAST(N'2014-10-20' AS Date), CAST(N'2014-09-30' AS Date), N'Bình thường', N'100000', N'Qua Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA11', N'DA15', N'Vũ Đức Hiệp', N'01661234568', N'hiepvd@gmial.com', CAST(N'2014-01-20' AS Date), CAST(N'2014-04-20' AS Date), CAST(N'2014-09-30' AS Date), N'Bình thường', N'150000', N'Dung Han')
INSERT [dbo].[MuonDoAn] ([Muon-doan], [MaDoAn], [Tennguoimuon], [Dienthoai], [Email], [Ngaymuon], [Ngaytra], [Ngaytradukien], [Tinhtrang], [Baolanh], [Ghichu]) VALUES (N'MDA12', N'DA01', N'Phạm Văn Tuấn', N'01661234568', N'tuanvp@gmial.com', CAST(N'2016-01-20' AS Date), CAST(N'2016-04-20' AS Date), CAST(N'2016-03-20' AS Date), N'Bình thường', N'150000', N'Qua Han')
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_GiangVienMaGV]    Script Date: 16/12/2017 09:48:36 ******/
ALTER TABLE [dbo].[GiangVien] ADD  CONSTRAINT [UC_GiangVienMaGV] UNIQUE NONCLUSTERED 
(
	[MaGV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DoAn]  WITH CHECK ADD  CONSTRAINT [FK_DoAn_GiangVien] FOREIGN KEY([MaGVHD])
REFERENCES [dbo].[GiangVien] ([MaGV])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DoAn] CHECK CONSTRAINT [FK_DoAn_GiangVien]
GO
ALTER TABLE [dbo].[DoAn]  WITH CHECK ADD  CONSTRAINT [FK_DoAn_GiangVien1] FOREIGN KEY([MaGVPB])
REFERENCES [dbo].[GiangVien] ([MaGV])
GO
ALTER TABLE [dbo].[DoAn] CHECK CONSTRAINT [FK_DoAn_GiangVien1]
GO
ALTER TABLE [dbo].[MuonDoAn]  WITH CHECK ADD  CONSTRAINT [FK__MuonDoAn__DoAn] FOREIGN KEY([MaDoAn])
REFERENCES [dbo].[DoAn] ([Madoan])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[MuonDoAn] CHECK CONSTRAINT [FK__MuonDoAn__DoAn]
GO
ALTER TABLE [dbo].[GiangVien]  WITH CHECK ADD  CONSTRAINT [CK__GiangVien__ChucDanh] CHECK  (([ChucDanh]='GS' OR [ChucDanh]='PGS.TS' OR [ChucDanh]='TS' OR [ChucDanh]='ThS' OR [ChucDanh]='KS'))
GO
ALTER TABLE [dbo].[GiangVien] CHECK CONSTRAINT [CK__GiangVien__ChucDanh]
GO
ALTER TABLE [dbo].[GiangVien]  WITH CHECK ADD  CONSTRAINT [CK__GiangVien__ChucVu] CHECK  (([ChucVu]='VT' OR [ChucVu]='VP' OR [ChucVu]='TBM' OR [ChucVu]='PBM' OR [ChucVu]='GV' OR [ChucVu]='GVC'))
GO
ALTER TABLE [dbo].[GiangVien] CHECK CONSTRAINT [CK__GiangVien__ChucVu]
GO
ALTER TABLE [dbo].[GiangVien]  WITH CHECK ADD  CONSTRAINT [CK__GiangVien__GhiChu] CHECK  (([GhiChu]='NCS NN' OR [GhiChu]='NGHI HUU' OR [GhiChu]='DANG CONG TAC'))
GO
ALTER TABLE [dbo].[GiangVien] CHECK CONSTRAINT [CK__GiangVien__GhiChu]
GO
